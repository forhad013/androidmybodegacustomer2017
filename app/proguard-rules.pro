# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/Mac/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}


-dontwarn com.google.gson.**
-dontwarn java.nio.file.**
-dontwarn org.codehaus.mojo.animal_sniffer.**
-dontwarn com.squareup.okhttp.internal.huc.**
-dontwarn com.android.volley.error.**
-keep class com.android.volley.error.** { *; }
-keep class com.squareup.okhttp.internal.huc.** { *; }
-keep class okio.** { *; }
-keep class android.support.design.widget.** { *; }
-keep interface android.support.design.widget.** { *; }
-dontwarn android.support.design.**
# Twilio Client
-keep class com.twilio.** { *; }

# Apache HttpClient
-dontwarn org.apache.http.**
