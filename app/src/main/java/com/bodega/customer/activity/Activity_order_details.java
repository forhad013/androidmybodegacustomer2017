package com.bodega.customer.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.adapter.OrderProductAdapter;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.gcm.RegistrationIntentService;
import com.bodega.customer.model.OrderProductModel;
import com.bodega.customer.twillio.ClientActivity;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Activity_order_details extends AppCompatActivity {
    TextView storeNameTxt,amount,statusTxt,date,time,type,payment;
    RelativeLayout callBtn,msgBtn;

    ListView productList;
    String id,  customerid,  storeid,  firstname,  lastname,  telephone,  email,  address,  city,  postalcode,  notes,  deliverycost,  deliverytype,  specialprice,  status,  createdAt,  updatedAt,  storename;

    String storeContact,gcmCode,loginStatus;
    boolean isInternetOn;

    ConnectionDetector cd;

    MyDatabase db;
    
    ArrayList<OrderProductModel> orderProductModelArrayList;

    String userID,message,storeOwnerName,profilePicString;

    int error;
    ;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    SharePref sharePref;
    JSONParser jsonParser;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_order_details);

         storeNameTxt = (TextView)  findViewById(R.id.storeName);
         amount = (TextView)  findViewById(R.id.amount);
         statusTxt = (TextView)  findViewById(R.id.status);
         date = (TextView)  findViewById(R.id.date);
         time = (TextView)  findViewById(R.id.time);
         type = (TextView)  findViewById(R.id.type);
         payment = (TextView)  findViewById(R.id.payment);
        callBtn = (RelativeLayout) findViewById(R.id.call);

        msgBtn= (RelativeLayout) findViewById(R.id.sms);
        productList = (ListView) findViewById(R.id.list);

        cd = new ConnectionDetector(getApplicationContext());

        sharePref = new SharePref(getApplicationContext());

        jsonParser = new JSONParser();

        orderProductModelArrayList = new ArrayList<>();
        
        db = new MyDatabase(getApplicationContext());

        isInternetOn = cd.isConnectingToInternet();

        userID = sharePref.getshareprefdatastring(SharePref.USERID);

        id= getIntent().getStringExtra("id");

        customerid= getIntent().getStringExtra("customerid");
        storeid = getIntent().getStringExtra("storeid");
        firstname= getIntent().getStringExtra("firstname");
        lastname= getIntent().getStringExtra("lastname");
        telephone= getIntent().getStringExtra("telephone");
        email= getIntent().getStringExtra("email");
        address= getIntent().getStringExtra("address");
        city= getIntent().getStringExtra("city");
        postalcode= getIntent().getStringExtra("postalcode");
                notes= getIntent().getStringExtra("notes");
                deliverycost= getIntent().getStringExtra("deliverycost");
                deliverytype= getIntent().getStringExtra("deliverytype");
                specialprice= getIntent().getStringExtra("specialprice");
                status= getIntent().getStringExtra("status");
                createdAt= getIntent().getStringExtra("createdAt");
                updatedAt= getIntent().getStringExtra("updatedAt");
                storename= getIntent().getStringExtra("storename");

        progressSweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        String[] orderTime = updatedAt.split("\\ ");

        String dateS = orderTime[0];
        String timeS = orderTime[1];


        storeNameTxt.setText(storename);

        String x = status;

        if(x.equals("0")){
            x = "Ordered";
        }else if(x.equals("0")){
            x.equals("Delivered");
        }else{
            x.equals("Canceled");
        }

        statusTxt.setText("Status : "+x);
        date.setText("Date : "+dateS);
        time.setText("Time : "+timeS);
        type.setText("Order Type : "+deliverytype);


        if(isInternetOn){

            new AsyncOrderDetails().execute();
        }


        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                if (loginStatus.equals("yes")) {
//                    CallToOwner call = new CallToOwner(getApplicationContext(), storeContact);
//                    call.call();

                    Intent in = new Intent(getApplicationContext(), ClientActivity.class);

                    in.putExtra("image", profilePicString);
                    in.putExtra("ownerName", storeOwnerName);



                        in.putExtra("storeContact", storeContact);



                    startActivity(in);
                } else {

                    if(!gcmCode.isEmpty()) {

                        Intent in = new Intent(getApplicationContext(), LoginActivity.class);

                        in.putExtra("token", storeOwnerName);

                        startActivity(in);
                    }else {

                        if(isInternetOn) {
                            if (checkPlayServices()) {
                                Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
                                getApplicationContext().startService(intent);

                            }
                            doneDialog = new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.NORMAL_TYPE);
                            doneDialog.setTitleText("Wait for few second");
                            // doneDialog.setContentText("Login unsuccessful");
                            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    doneDialog.dismiss();
                                }
                            });
                            doneDialog.show();

                        }

                    }
                }
            }
        });

        msgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                if(loginStatus.equals("yes")) {



                    db.saveStore(storeid,storename,storeOwnerName,"",storeContact);


                    Intent in = new Intent(getApplicationContext(), SoccetIOChat.class);

                    in.putExtra("shopId", storeid);
                  
                    in.putExtra("shopName", storeOwnerName);
                
                    startActivity(in);
                }else{

                    if(!gcmCode.isEmpty()) {

                        Intent in = new Intent(getApplicationContext(), LoginActivity.class);
                        in.putExtra("token", storeOwnerName);
                        startActivity(in);
                    }else{
                        if(isInternetOn) {
                            if (checkPlayServices()) {
                                Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
                                getApplicationContext().startService(intent);

                            }
                            doneDialog = new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.NORMAL_TYPE);
                            doneDialog.setTitleText("Wait for few second");
                            // doneDialog.setContentText("Login unsuccessful");
                            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    doneDialog.dismiss();
                                }
                            });
                            doneDialog.show();

                        }
                    }
                }
            }
        });


        ImageButton bckBtn = (ImageButton) findViewById(R.id.backBtn);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                 finish();
            }
            return false;
        }
        return true;
    }


    class AsyncOrderDetails extends AsyncTask<String, String, String> {



        @Override
        protected void onPreExecute() {

            try {

                progressSweetAlertDialog.show();

                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {

            }

        }

        @Override
        protected String doInBackground(String... params) {

            orderProductModelArrayList.clear();
            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("orderid", id));



            JSONObject json =null;
            json = jsonParser.makeHttpRequest(Url.ODRERDETAILS, "GET", pair);

            Log.e("jsonTset", json + "");

            try {
                error = json.getInt("error") ;

                if(error==0){


                    message =json.getString("message");

                    storeContact =json.getString("activecontactno");
                    profilePicString =json.getString("profilepic");
                    String n = json.getString("firstname");
                    String m = json.getString("lastname");

                    storeOwnerName = n+" "+m;



                    JSONArray conversationsArray = json.getJSONArray("records");




                    for(int i=0;i<conversationsArray.length();i++) {



                        JSONObject jsonObject = conversationsArray.getJSONObject(i);



                        String productID= jsonObject.getString("productid");

                        String productName= jsonObject.getString("productname");

                        String quantity= jsonObject.getString("quantity");

                        String totalPrice= jsonObject.getString("totalprice");




                        OrderProductModel orderModel = new OrderProductModel( productID,  productName,  quantity,  totalPrice );
                        orderProductModelArrayList.add(orderModel);



                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



            return  null;
        }

        @Override
        protected void onPostExecute(String result) {


            OrderProductAdapter orderAdapter = new OrderProductAdapter(getApplicationContext(),orderProductModelArrayList);



            productList.setAdapter(orderAdapter);

            progressSweetAlertDialog.dismiss();
        }

    }
}
