package com.bodega.customer.activity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.gcm.RegistrationIntentService;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegistrationActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    EditText firstName, lastName, password, email, contact;

    Button registerBtn, facebookBtn;

    SharePref sharePref;
    JSONObject json;
    int success = 0;

    String msg;
    String[] results;

    LinearLayout terms;
    Url url;

    String id;
    String phone;
    ConnectionDetector cd;

    String fbSocialID, fbEmail, fbFullname, fbFirstName, fbLastname;

    LoginButton loginButton;
    private static final int MY_PERMISSIONS_REQUEST_GCM = 700;

    private CallbackManager callbackManager;

    boolean isInternetOn = false;

    ImageButton backBtn;

    Typeface mytTypeface;

    String userFirstName, userLastName, userDeviceType;

    String userID, gcmCode, firstNameString, lastNameString, passwordString, emailString, message, userrlMethod, userSocialId, contactString;

    JSONParser jsonParser;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    int permissionCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_registration);


        LoginManager.getInstance().logOut();

        loginButton = (LoginButton) findViewById(R.id.login_button);


        backBtn = (ImageButton) findViewById(R.id.backBtn);

        loginButton.setReadPermissions("email", "public_profile");
        //   backBtn = (ImageButton) findViewById(R.id.backBtn);

        jsonParser = new JSONParser();
        url = new Url();

        mytTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        terms = (LinearLayout) findViewById(R.id.termsHyperLink);

        cd = new ConnectionDetector(getApplicationContext());

        isInternetOn = cd.isConnectingToInternet();

        sharePref = new SharePref(getApplicationContext());

        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        email = (EditText) findViewById(R.id.emailSignUp);
        password = (EditText) findViewById(R.id.passwordSignUp);
        contact = (EditText) findViewById(R.id.contactNo);

        registerBtn = (Button) findViewById(R.id.register);


        password.setTypeface(mytTypeface);

        // gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);

        if (checkPlayServices()) {
            Log.e("GCM", "asdasd");
            if (isInternetOn) {
//             permissionCheck  = ContextCompat.checkSelfPermission(getApplicationContext(),
//                     Manifest.permission.C2D_MESSAGE);
//
//                if (ContextCompat.checkSelfPermission(getApplicationContext(),
//                        Manifest.permission.C2D_MESSAGE)
//                        != PackageManager.PERMISSION_GRANTED) {
//
//                    // Should we show an explanation?
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                            Manifest.permission.C2D_MESSAGE)) {
//
//                        // Show an expanation to the user *asynchronously* -- don't block
//                        // this thread waiting for the user's response! After the user
//                        // sees the explanation, try again to request the permission.
//
//                    } else {
//
//                        // No explanation needed, we can request the permission.
//
//                        ActivityCompat.requestPermissions(this,
//                                new String[]{Manifest.permission.C2D_MESSAGE},
//                                MY_PERMISSIONS_REQUEST_GCM);
//
//                        // MY_PERMISSIONS_REQUEST_GCM is an
//                        // app-defined int constant. The callback method gets the
//                        // result of the request.
//                    }
//                }
//
//
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);

            }


        }

        //  init();


        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),
                        LoginActivity.class);

                startActivity(intent);
                finish();

            }
        });

        getData();
        // Log.e("GCM", gcmRegCode);

        gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);

//        while(1 > 0) {
//            loginButton.setEnabled(false);
//            if(!gcmCode.isEmpty()){
//
//                loginButton.setEnabled(true);
//                break;
//            }
//
//        }

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.mybodega.online/terms&conditions"));
                startActivity(in);
            }
        });


        Log.e("gcm,", gcmCode);

        progressSweetAlertDialog = new SweetAlertDialog(RegistrationActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {

                                    Log.e("fbInfo", object + "");
                                    fbSocialID = object.getString("id");
                                    fbEmail = object.getString("email");
                                    fbFullname = object.getString("name");

                                    if (fbFullname.split("\\w+").length > 1) {

                                        fbLastname = fbFullname.substring(fbFullname.lastIndexOf(" ") + 1);
                                        fbFirstName = fbFullname.substring(0, fbFullname.lastIndexOf(' '));
                                    } else {
                                        fbFirstName = fbFullname;
                                    }

                                    new AsyncTaskFBlogin().execute();

                                    Log.e("fbInfo", "Hi, " + fbFirstName + " " + fbLastname);
                                    Log.e("fbInfo", "Hi, " + object.getString("name") + " " + object.getString("email"));
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }


            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });



       /* backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });*/

        registerBtn.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {

                                               if (nullCheck()) {

                                                   firstNameString = firstName.getText().toString().trim();
                                                   lastNameString = lastName.getText().toString().trim();
                                                   emailString = email.getText().toString().trim();
                                                   passwordString = password.getText().toString().trim();
                                                   contactString = contact.getText().toString().trim();
                                                   userrlMethod = "0";
                                                   userSocialId = "0";
                                                   cd = new ConnectionDetector(getApplicationContext());

                                                   isInternetOn = cd.isConnectingToInternet();

                                                   if (isInternetOn) {

                                                       new AsyncTaskRegValid().execute();


                                                   } else {
                                                       Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();


                                                   }

                                               }
                                           }

                                       }
        );


    }


    //  @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_GCM: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(this, RegistrationIntentService.class);
                    startService(intent);

                } else {

                    doneDialog = new SweetAlertDialog(RegistrationActivity.this, SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setTitleText("Permission Denied");
                    doneDialog.setContentText("Can Not Complete Sign Up");
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    });

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /*
        public static boolean isEmailValid(String email) {
            boolean isValid = false;
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            CharSequence inputStr = email;
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (matcher.matches()) {
                isValid = true;
            }
            return isValid;
        }*/
    public boolean nullCheck() {
        boolean flag = false;

        if (!firstName.getText().toString().trim().equalsIgnoreCase("")) {
            if (!lastName.getText().toString().trim().equalsIgnoreCase("")) {
                if (!contact.getText().toString().trim().equalsIgnoreCase("")) {
                    if (!email.getText().toString().trim().equalsIgnoreCase("")) {
                        if (!password.getText().toString().trim().equalsIgnoreCase("")) {
                            return true;
                        } else {
                            msg = "Please Enter Password!";
                        }
                    } else {
                        msg = "Please Enter valid Email!";

                    }
                } else {
                    msg = "Please give Contact Number";
                }

            } else {
                msg = "Please Enter Last Name!";
            }

        } else {
            msg = "Please Enter First Name!";
        }


        if (!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }
        return flag;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        AppEventsLogger.deactivateApp(this);
        super.onPause();

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);

        finish();
        super.onBackPressed();
    }

    class AsyncTaskRegValid extends AsyncTask<String, String, String> {
        String responseBody;

        JSONObject response1;

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd", "asdasd");
                progressSweetAlertDialog.show();
                registerBtn.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {


            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Url.REGISTRATION);

            httpPost.setHeader("content-type", "application/json");
            httpPost.setHeader("Accept", "application/json");
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.accumulate("userFirstName", firstNameString);
                jsonObject.accumulate("userLastName", lastNameString);
                jsonObject.accumulate("userEmail", emailString);
                jsonObject.accumulate("userContact", contactString);
                jsonObject.accumulate("userPassword", passwordString);
                jsonObject.accumulate("userType", "0");
                jsonObject.accumulate("userrlMethod", userrlMethod);
                jsonObject.accumulate("userDeviceType", "0");
                jsonObject.accumulate("userSocialId", userSocialId);
                jsonObject.accumulate("userGcmCode", gcmCode);
            } catch (JSONException e) {
                e.printStackTrace();

            }

            // Log.e("reg", pair + "");
            Log.e("reg", json + "");
            StringEntity entity = null;

            try {
                entity = new StringEntity(jsonObject.toString());
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                // Log.d("ch",entity+"");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            httpPost.setEntity(entity);

            try {
                //  response = httpClient.execute(httpPost);
//                    resC++;
//                    Log.e("Value of response count: ", resC + "");
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                responseBody = httpClient.execute(httpPost, responseHandler);
                response1 = new JSONObject(responseBody);
                Log.e("res", responseBody + "");

                // invoiceNumber= response1.getString("orderno");


            } catch (IOException e) {
                e.printStackTrace();
                //  } catch (JSONException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                success = response1.getInt("success");
                message = response1.getString("message");

                Log.e("success", String.valueOf(success));
                Log.e("msh", message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if (success == 1) {
                progressSweetAlertDialog.dismiss();

                registerBtn.setEnabled(true);
                sharePref.setshareprefdatastring(SharePref.LOGEDIN, "no");
                try {

                    JSONObject results = response1.getJSONObject("result");
                    sharePref.setshareprefdatastring(SharePref.USERID, results.getString("userId"));
                    sharePref.setshareprefdatastring(SharePref.FIRSTNAME, firstNameString);
                    sharePref.setshareprefdatastring(SharePref.USERPASSWWORD, passwordString);
                    sharePref.setshareprefdatastring(SharePref.USEREMAIL, emailString);
                    sharePref.setshareprefdatastring(SharePref.USERPHONE, contactString);
                    sharePref.setshareprefdatastring(SharePref.LOGEDIN, "yes");
                    sharePref.setshareprefdatastring(SharePref.LOGEDIN_METHOD, "email");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                doneDialog = new SweetAlertDialog(RegistrationActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                doneDialog.setTitleText("Success");
                doneDialog.setContentText("Registration Successful");
                doneDialog.show();
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        finish();
                    }
                });

//                Intent intent = new Intent(RegistrationActivity.this, DrawerActivity.class);
//                startActivity(intent);
                //finish();


            } else {

                progressSweetAlertDialog.dismiss();
                registerBtn.setEnabled(true);
                final SweetAlertDialog errorDialog = new SweetAlertDialog(RegistrationActivity.this, SweetAlertDialog.ERROR_TYPE);
                errorDialog.setTitleText("ERROR");
                errorDialog.setContentText(message);
                errorDialog.show();
                errorDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        errorDialog.dismiss();
                    }
                });
            }

        }


    }


    class AsyncTaskFBlogin extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd", "asdasd");
                progressSweetAlertDialog.show();
                registerBtn.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("userEmail", fbEmail));
            pair.add(new BasicNameValuePair("userPassword", ""));
            pair.add(new BasicNameValuePair("newGcmCode", gcmCode));
            pair.add(new BasicNameValuePair("userType", "0"));
            pair.add(new BasicNameValuePair("userrlMethod", "1"));
            pair.add(new BasicNameValuePair("userSocialId", fbSocialID));
            pair.add(new BasicNameValuePair("userDeviceType", "0"));
            pair.add(new BasicNameValuePair("userFirstName", fbFirstName));
            pair.add(new BasicNameValuePair("userLastName", fbLastname));
            json = jsonParser.makeHttpRequest(Url.LOGIN, "POST", pair);

            Log.e("reg", pair + "");


            try {


                success = json.getInt("success");
                message = json.getString("message");


                if (success == 1) {
                    json = json.getJSONObject("results");

                    JSONObject jsonObject = json.getJSONObject("userInformation");

                    userID = jsonObject.getString("userId");
                    userFirstName = jsonObject.getString("userFirstName");
                    userLastName = jsonObject.getString("userLastName");
                    userDeviceType = jsonObject.getString("userDeviceType");


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (success == 1) {
                progressSweetAlertDialog.dismiss();

                registerBtn.setEnabled(true);

                sharePref.setshareprefdatastring(SharePref.USEREMAIL, emailString);
                sharePref.setIsLoggedIn(true);


                try {
                    sharePref.setshareprefdatastring(SharePref.USERID, userID);
                    sharePref.setshareprefdatastring(SharePref.FIRSTNAME, userFirstName);
                    sharePref.setshareprefdatastring(SharePref.LASTNAME, userLastName);
                    sharePref.setshareprefdatastring(SharePref.USEREMAIL, emailString);
                    sharePref.setshareprefdatastring(SharePref.GCM_REGCODE, gcmCode);
                    sharePref.setshareprefdatastring(SharePref.USERPASSWWORD, "");
                    sharePref.setshareprefdatastring(SharePref.LOGEDIN, "yes");

                    sharePref.setshareprefdatastring(SharePref.LOGEDIN_METHOD, "facebook");


                    finish();

                } catch (Exception e) {

                }

                //   new AsyncTaskSocketReg().execute();
                //   Toast.makeText(getApplicationContext(),f+"p"+product+" "+userId+" "+ userLastName+" "+userLastName, Toast.LENGTH_LONG).show();


            } else {
                progressSweetAlertDialog.dismiss();
                doneDialog = new SweetAlertDialog(RegistrationActivity.this, SweetAlertDialog.ERROR_TYPE);
                doneDialog.setTitleText("Login unsuccessful");
                // doneDialog.setContentText("Login unsuccessful");
                doneDialog.show();
            }


        }


    }


    public void getData() {

        String possibleEmail = "";


//


        try {


            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            contact.setText(tManager.getLine1Number());


            // Show on screen

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");

            for (Account account : accounts) {

                possibleEmail = account.name;
                email.setText(possibleEmail);
            }
        } catch (Exception e) {
            Log.i("Exception", "Exception:" + e);
        }


    Log.i("Exception", "mails:"+possibleEmail) ;
}
    public class EmailFetcher{

        String getName(Context context) {
            Cursor CR = null;
            CR = getOwner(context);
            String id = "", name = "";
            while (CR.moveToNext())
            {
                name = CR.getString(CR.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            }
            return name;
        }





        Cursor getOwner(Context context) {
            String accountName = null;
            Cursor emailCur = null;
            AccountManager accountManager = AccountManager.get(context);
            Account[] accounts = accountManager.getAccountsByType("com.google");
            if (accounts[0].name != null) {
                accountName = accounts[0].name;
                String where = ContactsContract.CommonDataKinds.Email.DATA + " = ?";
                ArrayList<String> what = new ArrayList<String>();
                what.add(accountName);
                Log.v("Got account", "Account " + accountName);
                for (int i = 1; i < accounts.length; i++) {
                    where += " or " + ContactsContract.CommonDataKinds.Email.DATA + " = ?";
                    what.add(accounts[i].name);
                    Log.v("Got account", "Account " + accounts[i].name);
                }
                String[] whatarr = (String[])what.toArray(new String[what.size()]);
                ContentResolver cr = context.getContentResolver();
                emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, where, whatarr, null);
                if (id != null) {
                    // get the phone number
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext())
                    {
                        phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Log.v("Got contacts", "phone" + phone);
                    }
                    pCur.close();
                }
            }
            return emailCur;
        }
    }

    public void init(){

        TelephonyManager phoneManager = (TelephonyManager)
                getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        String phoneNumber = phoneManager.getLine1Number();

        Log.e("asd","asd");
        contact.setText(phoneNumber);



    }


}
