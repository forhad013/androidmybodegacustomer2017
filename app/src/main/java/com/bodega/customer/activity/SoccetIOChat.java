package com.bodega.customer.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.adapter.NewMessageListAdapter;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.gcm.RegistrationIntentService;
import com.bodega.customer.model.NewMessageItem;
import com.bodega.customer.twillio.ClientActivity;
import com.bodega.customer.utils.ChatApplication;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.bodega.customer.R.id.rel;


public class SoccetIOChat extends AppCompatActivity {

    String shopID,shopName;

    TextView shopNameView;

    EditText messageEditText;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    int success;

    int error=0;

    ImageButton backBtn;

    String message;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    String conversationID;

    LinearLayout callBtn;

    boolean isInternetOn;

    String roomID="";

    String storeContact,storeImage,gcmCode,loginStatus,manager_name;
    MyDatabase db;

    ImageButton sendBtn;

    ListView messageList;

    String sendingMsg,userID;

    ConnectionDetector cd;
    String userName;

    boolean intStatus;

    SharePref sharePref;

    JSONParser jsonParser;
    String senderID,senderName,shopEmail;

    NewMessageListAdapter singleMessageListAdapter;

    HashMap<String,String> roomMap;

    public ArrayList<NewMessageItem> newMessageItemArrayList,MessageItemList;

    RelativeLayout relativeLayout;

   // LinearLayout linearLayout;

    SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat newtargetFormat = new SimpleDateFormat("MM-dd-yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm-a");

    SimpleDateFormat databaseDateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    SimpleDateFormat chatDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm-a");

  //  2016-05-17 21:29:36
    Socket mSocket;
//    {
//        try {
//            mSocket = IO.socket(Url.SOCKETCHATIP);
//        } catch (URISyntaxException e) {}
//    }


    private Emitter.Listener handleIncomingMessages = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
              runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("data","run");
                     JSONObject data = (JSONObject) args[0];
//
                   Log.e("data",data+"");


                    try{
     //   {"sender":{"image_path":null,"userLastName":"Hossen","socket_id":"ILFV88IJJq2QzDwIAAEf","userFirstName":"Forhad","userId":57},"id":123,"text":"asd","updated_at":"2017-01-25T10:03:56.000Z","created_at":"2017-01-25T10:03:56.000Z","room_id":8,"user_id":"57","type":"message"}


                                JSONObject deatils = data.getJSONObject("sender");

                                String message = data.getString("text");
                        String dateNdTime  = data.getString("created_at").replace("T"," ").replace("."," ");

                        dateNdTime = dateNdTime.substring(0,dateNdTime.lastIndexOf(" "));

                        Log.e("d",dateNdTime);
                        String senderId = data.getString("user_id");

                        String senderName = shopName;
                        String room_id = data.getString("room_id");

                        String receiver_id = userID;

                        String receiver_name = userName;

                        Date dateFromDb = databaseDateformat.parse(dateNdTime);

                        // dateFromDb.setT

                        Calendar c = Calendar.getInstance();

                        c.setTime(dateFromDb);
                        c.setTimeZone(TimeZone.getDefault());

                        String s=  getCurrentTimezoneOffset();
                        s= s.replace("G","");
                        s= s.replace("M","");
                        s= s.replace("T","");


                        boolean hasPlusSign = s.contains("+");

                        // Log.e("asd",hasPlusSign+"");
                        if(hasPlusSign){
                            s= s.replace("+","");
                            String[] parts = s.split(":");
                            c.add(Calendar.HOUR,Integer.parseInt(parts[0]));
                            c.add(Calendar.MINUTE,Integer.parseInt(parts[1]));
                        }else{
                            s=   s.replace("-","");
                            String[] parts = s.split(":");
                            int k = - Integer.parseInt(parts[0]);
                            int j = - Integer.parseInt(parts[1]);

                            c.add(Calendar.HOUR,k);
                            c.add(Calendar.MINUTE,j);
                        }


                        Date dbTime=c.getTime();

                        // Log.e("parse",timeFormat.format(dbTime));

                        dateNdTime = chatDateFormat.format(dbTime);


                        int id = 0;

                        String[] msgTime = dateNdTime.split("\\ ");

                        String date = msgTime[0];
                        String time = msgTime[1];
                       // sendNotification(message,senderId,senderName,receiver_id,date,time);

                        Log.e("rm",room_id);
                        Log.e("rm1",roomID);
                        if(roomID.equals(room_id)&&!senderId.equals(userID)) {
                            NewMessageItem newMessageItem = new NewMessageItem(id + "", senderId + "", senderName, message, time, date, 1, receiver_id + "", receiver_name);

                            //    newMessageItemArrayList.add(newMessageItem);

                            MessageItemList.add(newMessageItem);

                            singleMessageListAdapter.notifyDataSetChanged();
                            messageList.setSelection(singleMessageListAdapter.getCount());

                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);

                            try {
                                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                r.play();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
 

                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_message_acitivity);


        //SocketLogin socketLogin =  new SocketLogin(getApplicationContext());



      //  mSocket.on("message", handleIncomingMessages);



        shopID = getIntent().getStringExtra("shopId");

//        Log.e("shop",shopID);

        shopName = getIntent().getStringExtra("shopName");

        shopEmail = getIntent().getStringExtra("shopEmail");



        roomMap = new HashMap<>();

        sharePref = new SharePref(getApplicationContext());

        backBtn = (ImageButton) findViewById(R.id.backBtn);

        jsonParser = new JSONParser();
        shopNameView = (TextView) findViewById(R.id.storeName);

        messageEditText = (EditText) findViewById(R.id.message_text);

        messageList = (ListView) findViewById(R.id.msgList);

        relativeLayout = (RelativeLayout) findViewById(rel);

        sendBtn = (ImageButton) findViewById(R.id.send);

       // linearLayout = (LinearLayout) findViewById(R.id.messageBar);
        db = new MyDatabase(getApplicationContext());

        MessageItemList = new ArrayList<>();
        userID = sharePref.getshareprefdatastring(SharePref.USERID);
        userName = sharePref.getshareprefdatastring(SharePref.USERNAME);

        String s = "";
        if(shopName.length()>=20){
            s = shopName.substring(0,20);
            s= s+"..";
        }else{
            s = shopName;
        }
                shopNameView.setText(s);

        newMessageItemArrayList = new ArrayList<>();
        senderID = sharePref.getshareprefdatastring(SharePref.USERID);

        senderName = sharePref.getshareprefdatastring(SharePref.USERNAME);

        MessageItemList = new ArrayList<>();

        cd = new ConnectionDetector(getApplicationContext());
        callBtn = (LinearLayout) findViewById(R.id.call);

        intStatus = cd.isConnectingToInternet();
        TimeZone tz = TimeZone.getDefault();


       // Log.e("tz",getCurrentTimezoneOffset()+"");

      //  new AsyncGetAllMessage().execute();


        progressSweetAlertDialog = new SweetAlertDialog(SoccetIOChat.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

  //      db.updateRead(shopID);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //connectSocket();

        if(intStatus){
          //  new AsyncTaskLogin().execute();
            connectSocket();
        }else{

            Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();


        }


        singleMessageListAdapter = new NewMessageListAdapter(getApplicationContext(), MessageItemList);



        messageList.setAdapter(singleMessageListAdapter);
        messageList.setSelection(singleMessageListAdapter.getCount()-1);

        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                if (loginStatus.equals("yes")) {
//                    CallToOwner call = new CallToOwner(getApplicationContext(), storeContact);
//                    call.call();

                    Intent in = new Intent(getApplicationContext(), ClientActivity.class);

                    in.putExtra("image", storeImage);
                    in.putExtra("ownerName",shopName );



                    in.putExtra("storeContact", storeContact);



                    startActivity(in);
                } else {

                    if(!gcmCode.isEmpty()) {

                        Intent in = new Intent(getApplicationContext(), LoginActivity.class);

                        in.putExtra("token", "");

                        startActivity(in);
                    }else {

                        if(intStatus) {
                            if (checkPlayServices()) {
                                Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
                                getApplicationContext().startService(intent);

                            }
                            doneDialog = new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.NORMAL_TYPE);
                            doneDialog.setTitleText("Wait for few second");
                            // doneDialog.setContentText("Login unsuccessful");
                            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    doneDialog.dismiss();
                                }
                            });
                            doneDialog.show();

                        }

                    }
                }
            }
        });



        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intStatus = cd.isConnectingToInternet();

                if(intStatus) {

                    sendingMsg = messageEditText.getText().toString();



                    JSONObject jsonObject = new JSONObject();

                try {
                        jsonObject.accumulate("user_id",userID);
                        jsonObject.accumulate("receiver_id",shopID);
                        jsonObject.accumulate("text",sendingMsg);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if(!sendingMsg.isEmpty()) {

                        mSocket.emit("message", jsonObject);



                        Calendar c =Calendar.getInstance();
                        Date current_time = c.getTime();

                        Log.e("current",current_time+"");

                       String dateString = targetFormat.format(current_time);

                        String timeString = timeFormat.format(current_time);


                        NewMessageItem newMessageItem = new NewMessageItem(0+"",  userID+"",  userName,  sendingMsg,    timeString, dateString, 1,  shopID+"",  shopName);

                        MessageItemList.add(newMessageItem);

                        singleMessageListAdapter.notifyDataSetChanged();
                        messageEditText.setText("");
                        messageList.setSelection(MessageItemList.size()-1);
                        messageList.setSelection(singleMessageListAdapter.getCount()-1);
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);

                        if(roomID.equals("")){
                            new AsyncGetRoomDetails().execute();
                        }
                    }


                }else{
                    Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);

    }


    public String getCurrentTimezoneOffset() {

        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        int offsetInMillis = tz.getOffset(cal.getTimeInMillis());

        String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
        offset = "GMT"+(offsetInMillis >= 0 ? "+" : "-") + offset;

        return offset;
    }

    public void connectSocket(){
       // ChatApplication app = (ChatApplication) getApplication();
        ChatApplication app = new ChatApplication();
        mSocket = app.getSocket();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("user_id",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        mSocket.emit("register", jsonObject);//
     //   mSocket.on("joined", onNewMessage);
        mSocket.on("message", handleIncomingMessages);

        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);



        mSocket.connect();

        Log.e("con","connected");

        // checkNewMsg();

    }
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    @Override
    public void onBackPressed() {

        finish();

        super.onBackPressed();
    }


    @Override
    protected void onStop() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("user_id",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }




        mSocket.emit("unregister", jsonObject);

        super.onStop();
    }

    @Override
    protected void onDestroy() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("user_id",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Stop","Stop");
        mSocket.emit("unregister", jsonObject);
        super.onDestroy();
    }

    public void getAllMessage(){

        messageList.setAdapter(singleMessageListAdapter);
        messageList.setSelection(singleMessageListAdapter.getCount());
    }



    @Override
    protected void onNewIntent(Intent intent) {

       // getAllMessage();
        super.onNewIntent(intent);
    }
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
             runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for(int i=0;i<args.length;i++) {
                        Log.e("soccet", args[i] + "");
                    }
                }
            });
        }
    };

    @Override
    public void onResume() {
        mSocket.connect();
        if(!roomID.equals("")) {
            new AsyncGetAllMessage().execute();
        }else{
            new AsyncGetRoomDetails().execute();
        }
        super.onResume();
     //   getApplicationContext().registerReceiver(mMessageReceiver, new IntentFilter("unique_name"));
    }

    @Override
    protected void onPause() {
        super.onPause();
       // getApplicationContext().unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Extract data included in the Intent
        //    getAllMessage();

            //do other stuff here
        }
    };

//    @Override
//    protected void onDestroy() {
//        mSocket.off("chat.messages", handleIncomingMessages);
//        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
//        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
//        mSocket.disconnect();
//        super.onDestroy();
//    }




    class AsyncGetAllMessage extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            try {

                newMessageItemArrayList.clear();

                progressSweetAlertDialog.show();

                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {

            }

        }

        @Override
        protected String doInBackground(String... params) {


            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("roomid", roomID));


            String url ="http://mybodega.online/apis/getroommessages.php";

            JSONObject json = jsonParser.makeHttpRequest(url, "GET", pair);

            Log.e("url", url+ "");
            Log.e("json", json+ "");

            try {



                error = json.getInt("success");

                if(error==1){



                    JSONArray contactArray = json.getJSONObject("results").getJSONArray("contactno");

                    {
                        for (int i = 0; i < contactArray.length(); i++) {



                            JSONObject data = contactArray.getJSONObject(i);

                            String id = data.getString("userid");
                            if(!id.equals(userID)){
                                storeContact =    data.getString("contactno")+"";
                            }

                            Log.e("St",storeContact);

                        }
                    }

                   JSONArray jsonArray = json.getJSONObject("results").getJSONArray("info");

                    int l = jsonArray.length();

                    for(int i=0;i<jsonArray.length();i++){

                        try{


                            JSONObject data = jsonArray.getJSONObject(i);

                            String message = data.getString("text");
                            String dateNdTime  = data.getString("created_at");

                            Date dateFromDb = databaseDateformat.parse(dateNdTime);

                           // dateFromDb.setT

                            Calendar c = Calendar.getInstance();

                            c.setTime(dateFromDb);
                            c.setTimeZone(TimeZone.getDefault());

                          String s=  getCurrentTimezoneOffset();
                            s= s.replace("G","");
                            s= s.replace("M","");
                            s= s.replace("T","");


                            boolean hasPlusSign = s.contains("+");

                           // Log.e("asd",hasPlusSign+"");
                            if(hasPlusSign){
                                s= s.replace("+","");
                                String[] parts = s.split(":");
                                c.add(Calendar.HOUR,Integer.parseInt(parts[0]));
                                c.add(Calendar.MINUTE,Integer.parseInt(parts[1]));
                            }else{
                                s=   s.replace("-","");
                                String[] parts = s.split(":");
                                int k = - Integer.parseInt(parts[0]);
                                int j = - Integer.parseInt(parts[1]);

                                c.add(Calendar.HOUR,k);
                                c.add(Calendar.MINUTE,j);
                            }


                            Date dbTime=c.getTime();

                           // Log.e("parse",timeFormat.format(dbTime));

                            dateNdTime = chatDateFormat.format(dbTime);

                        //    JSONObject sender = data.getJSONObject("sender_id");



                            int senderID= data.getInt("senderid");
                            String senderName= "";


                            //JSONObject receiver = data.getJSONObject("receiver");

                            int receiverID= data.getInt("receiverid");
                            String receiverName= "";

                            int id = 0;

                            String[] msgTime = dateNdTime.split("\\ ");

                            String date = msgTime[0];
                            String time = msgTime[1];

                            NewMessageItem newMessageItem = new NewMessageItem(id+"",  senderID+"",  senderName,  message,  time,  date,  1,  receiverID+"",  receiverName);

                            newMessageItemArrayList.add(newMessageItem);

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

//           Log.e("asd", newMessageItemArrayList.size()+"");

            MessageItemList.clear();

            if(storeContact.equals("")||storeContact.equals("null")){
                callBtn.setVisibility(View.GONE);
            }

            int l=newMessageItemArrayList.size();
            for(int i=0;i<newMessageItemArrayList.size();i++){

                MessageItemList.add(newMessageItemArrayList.get(l-i-1));

            }

           MessageItemList = newMessageItemArrayList ;


            singleMessageListAdapter = new NewMessageListAdapter(getApplicationContext(), MessageItemList);



            messageList.setAdapter(singleMessageListAdapter);
            messageList.setSelection(singleMessageListAdapter.getCount()-1);


            progressSweetAlertDialog.dismiss();

        }

    }


    private void sendNotification(String message,String senderID,String senderName,String receiverID,String smsDate,String smsTime) {
        Intent intent = new Intent(this, SoccetIOChat.class);
        intent.putExtra("shopId",senderID);
        intent.putExtra("shopName",senderName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_gcm,BIND_IMPORTANT)


                .setContentTitle("New Message from "+ senderName)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    class AsyncGetRoomDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            try {

                progressSweetAlertDialog.show();


                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {

            }

        }

        @Override
        protected String doInBackground(String... params) {


            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            //    pair.add(new BasicNameValuePair("conversation", conversationID));


            String url ="http://mybodega.online/apis/getroomid.php";

            pair.add(new BasicNameValuePair("senderid", senderID));

            pair.add(new BasicNameValuePair("receiverid", shopID));

            JSONObject json = jsonParser.makeHttpRequest(url, "GET", pair);

            Log.e("url", url+ "");
            Log.e("json", json+ "");


            try {



                error = json.getInt("success");

                if(error==1){


                    storeContact = json.getJSONObject("results").getString("contactno");

                    roomID= json.getJSONObject("results").getString("roomid");

                    userName= json.getJSONObject("results").getString("firstname") + " " + json.getJSONObject("results").getString("lastname");




                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            progressSweetAlertDialog.dismiss();
//           Log.e("asd", newMessageItemArrayList.size()+"");

            new AsyncGetAllMessage().execute();

        }

    }
}


