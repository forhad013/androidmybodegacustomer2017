package com.bodega.customer.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ForgetPassword extends AppCompatActivity {

    TextView topTxt,submit;

    EditText email;

    ImageButton backBtn;

    int success = 0 ;
    String message;
    JSONParser jsonParser;

    JSONObject json;

    String emailString;





    boolean isInternetAvailable;
    ConnectionDetector cd;

    Typeface myTypeface, custom;

    SharePref sharePref;

    TextView web,contact;


    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);


        backBtn = (ImageButton) findViewById(R.id.backBtn);
        email = (EditText) findViewById(R.id.email);

        submit = (TextView) findViewById(R.id.submit);

        jsonParser = new JSONParser();

        topTxt = (TextView) findViewById(R.id.barText);

        myTypeface = Typeface.createFromAsset(this.getAssets(), "fonts/GothamRnd-Book.otf");

        custom = Typeface.createFromAsset(this.getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        contact = (TextView)findViewById(R.id.contact);
        web = (TextView)findViewById(R.id.web);



        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {


                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("message/rfc822");
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@mybodega.online"});

                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(ForgetPassword.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.mybodega.online/"));
                    startActivity(in);
                }
            }
        });
      //  email.setTypeface(myTypeface);
//        submit.setTypeface(custom);
//        topTxt.setTypeface(custom);
//


        cd = new ConnectionDetector(getApplicationContext());

        isInternetAvailable = cd.isConnectingToInternet();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);


                startActivity(intent);

                finish();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isInternetAvailable = cd.isConnectingToInternet();
                emailString =  email.getText().toString();
                progressSweetAlertDialog = new SweetAlertDialog(ForgetPassword.this, SweetAlertDialog.PROGRESS_TYPE);
                progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                progressSweetAlertDialog.setTitleText("Loading");
                progressSweetAlertDialog.setCancelable(false);

                submit.setEnabled(true);

                if(!emailString.isEmpty()) {
                    if(isInternetAvailable) {
                        new AsyncTaskForget().execute();
                    }else{

                        Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                }else{

                    doneDialog = new SweetAlertDialog(ForgetPassword.this, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setContentText("Give your email address");
                    doneDialog.show();
                }
            }
        });



    }

//    @Override
//    public void setContentView(View view)
//    {
//        super.setContentView(view);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
//    }

    class AsyncTaskForget extends AsyncTask<String, String, String> {


        @Override
        protected void onPostExecute(String result) {
            if (success == 1) {


                try {


                   /// sharePref.setshareprefdatastring(SharePref.LOGEDIN, "yes");

                    progressSweetAlertDialog.dismiss();


                    doneDialog = new SweetAlertDialog(ForgetPassword.this, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setContentText(message);
                    doneDialog.show();
                    doneDialog.setConfirmText("OK");
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);


                            startActivity(intent);

                            finish();
                        }
                    });

                } catch (Exception e) {

                }

            } else {
                progressSweetAlertDialog.dismiss();
                doneDialog = new SweetAlertDialog(ForgetPassword.this, SweetAlertDialog.SUCCESS_TYPE);
                doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                doneDialog.setContentText("We do not have this email address in our system.  Please, verify your correct email or write us to password@mybodega.online");
                doneDialog.show();
            }


          
            submit.setEnabled(true);
        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("userEmail",emailString));
            pair.add(new BasicNameValuePair("userType","0"));



            json = jsonParser.makeHttpRequest(Url.FORGET, "POST", pair);

            Log.e("reg", json + "");


            try {

                success = json.getInt("success");
                message = json.getString("message");





            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd", "asdasd");
                progressSweetAlertDialog.show();
                submit.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

    }
}
