package com.bodega.customer.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.FontChangeCrawler;
import com.bodega.customer.utils.SharePref;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class SplashActivity extends AppCompatActivity {

    TextView contact, web;
    TextView explore;
    protected boolean _active = true;
    protected int _splashTime = 3000;
    RelativeLayout relative;


    boolean firstTime;



    boolean isInternetOn;

    ConnectionDetector cd;

    JSONParser jsonParser;
    SharePref sharePref;

    boolean status;
    String tokenStirng;

    SweetAlertDialog doneDialog;


    MyDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        contact = (TextView)findViewById(R.id.contact);
        web = (TextView)findViewById(R.id.web);
        explore = (TextView)findViewById(R.id.explore);



        db = new MyDatabase(getApplicationContext());

     //   db.createCartTable();

        jsonParser = new JSONParser();

        cd = new ConnectionDetector(getApplicationContext());

        sharePref = new SharePref(getApplicationContext());

        isInternetOn = cd.isConnectingToInternet();

        sharePref = new SharePref(getApplicationContext());


//        new AsyncTaskEdit().execute();

        firstTime = true;


        if(!sharePref.getshareprefdataBoolean(SharePref.OLDUSER)){

            sharePref.setshareprefdataBoolean(SharePref.OLDUSER,false);
        }


        relative = (RelativeLayout) findViewById(R.id.relative);
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {



                }  Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@mybodega.online"});

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(SplashActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    Intent in=new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.mybodega.online/"));
                    startActivity(in);
                }
            }
        });


        relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    Intent in = new Intent(SplashActivity.this, DrawerActivity.class);
                    startActivity(in);
                    finish();
                }
            }
        });




//        if(isInternetOn){
//
//            new AsyncTaskEdit().execute();
//
//        }else{
//
//            doneDialog=  new SweetAlertDialog(SplashActivity.this,SweetAlertDialog.ERROR_TYPE);
//            doneDialog.setTitleText("Error");
//            doneDialog.setContentText("Please turn on the internet connection and Restart the application");
//            doneDialog.show();
//            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                @Override
//                public void onClick(SweetAlertDialog sweetAlertDialog) {
//
//
//                    finish();
//
//
//                }
//            });
//
//
//        }


        if(!isInternetOn){
            Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }



    }

    @Override
    public void setContentView(View view)
    {
        super.setContentView(view);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
    }





    }

