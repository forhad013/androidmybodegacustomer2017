package com.bodega.customer.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.adapter.ConversationItem;
import com.bodega.customer.adapter.NavDrawerListAdapter;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.fragment.ChangePassword;
import com.bodega.customer.fragment.Fragment_Cart;
import com.bodega.customer.fragment.Fragment_ShopDetails;
import com.bodega.customer.fragment.Fragment_last_order;
import com.bodega.customer.fragment.Map_fragment_main_permission;
import com.bodega.customer.fragment.MessageList;
import com.bodega.customer.fragment.ProfileFragment;
import com.bodega.customer.gcm.RegistrationIntentService;
import com.bodega.customer.utils.ChatApplication;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class DrawerActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ImageView mDrawerfooter;
    private ImageButton mDrawerToggle;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private NavDrawerListAdapter adapter;
    private ArrayList<String> navDrawerItems;

    private ArrayList<Integer> navDrawerItemsImages;

    JSONObject json;
    String gcmCode;
    TextView newTextCounter;
    Fragment mFragment;
    public Stack<Fragment> fragmentStack;
    // nav drawer title
    private CharSequence mDrawerTitle;
    ConnectionDetector cd;
    boolean isInternetPresent;
    // SimpleFacebook mSimpleFacebook;
    ImageButton text;

    ArrayList<ConversationItem> conversationItemArrayList;

    public FragmentManager mFragmentManager;
    SharePref sharePref;
    String type="normal";
    ImageView toolbar_title;
    String userID;
    ImageButton submit;

    String token ="";

    TextView titleText;

    MyDatabase db;
    String loginStatus;

    ImageButton cart;


    TextView cartCounter;

    SweetAlertDialog doneDialog;

    public static List<String> categoryname;
    public static List<String> categoryid;

    int success;
    public JSONParser jsonParser = new JSONParser();

    String loginMethod;

    Socket mSocket;

    public int newMessageCounter=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        //  toolbar_title = (ImageView) findViewById(R.id.toolbar_title);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerToggle = (ImageButton) findViewById(R.id.ham);
        navDrawerItems = new ArrayList<>();
        navDrawerItemsImages = new ArrayList<>();

        fragmentStack = new Stack<>();

        db = new MyDatabase(getApplicationContext());
        // submit = (ImageButton) findViewById(R.id.submit);

        sharePref = new SharePref(this);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();


        userID = sharePref.getshareprefdatastring(SharePref.USERID);
        text = (ImageButton) findViewById(R.id.text);
       // Log.e("logi", loginStatus);

        newTextCounter = (TextView) findViewById(R.id.textCounter);

        mDrawerList.setDividerHeight(2);
        cart = (ImageButton) findViewById(R.id.cart);
        // Log.e("logi", loginStatus);

        cartCounter = (TextView) findViewById(R.id.cartCounter);

       // SocketLogin socketLogin =  new SocketLogin(getApplicationContext());

        titleText = (TextView) findViewById(R.id.titleText);


        conversationItemArrayList = new ArrayList<>();

        if(isInternetPresent){


          //  new AsyncTaskLogin().execute();

        }
        loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
        loginMethod = sharePref.getshareprefdatastring(SharePref.LOGEDIN_METHOD);
        if(loginStatus.equals("yes")){

            initDrawerLogedIn();
        }else{

            initDrawerNotLogedIn();
        }


              displayView(0);

     //   setCounter();



        if(isInternetPresent) {

            connectSocket();
        }



        mDrawerToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
            }
        });


        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                loginMethod = sharePref.getshareprefdatastring(SharePref.LOGEDIN_METHOD);


                if (loginStatus .equals("yes")){
                    displayView(4);
                }
                else
                {
                    Intent in = new Intent(DrawerActivity.this, LoginActivity.class);
                    startActivity(in);

                }


            }
        });

        cart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                displayView(5);
            }
        });


        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                displayView(i);


            }
        });


     //   categoryname = new ArrayList<String>();
     //   categoryid = new ArrayList<String>();

        if(isInternetPresent) {
            //   new AsyncTaskGetCategories().execute();
        }else{
            Toast.makeText(getApplicationContext(), "Check your internet connection!", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    protected void onDestroy() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("user_id",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        mSocket.emit("unregister", jsonObject);

        super.onDestroy();
    }

    public void displayView(int position) {
        // update the main content by replacing fragments

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new Map_fragment_main_permission();
             //   fragment = new Fragment_ShopDetails();
                titleText.setText("Select your store");
                token = "hide";

                hideTextButton(true);
                hideCartButton(true,0);

                break;
            case 1:
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                loginMethod = sharePref.getshareprefdatastring(SharePref.LOGEDIN_METHOD);
                gcmCode  = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
                if (loginStatus .equals("yes")){
                    fragment = new ProfileFragment();
                    hideCartButton(true,0);
                    hideTextButton(true);
                }
                else
                {
                        if(!gcmCode.isEmpty()) {
                            Intent in = new Intent(DrawerActivity.this, LoginActivity.class);


                            startActivity(in);
                        }else{

                            if (checkPlayServices()) {
                                // Start IntentService to register this application with GCM.
                                Intent intent = new Intent(this, RegistrationIntentService.class);
                                startService(intent);

                            }
                            doneDialog = new SweetAlertDialog(DrawerActivity.this, SweetAlertDialog.NORMAL_TYPE);
                            doneDialog.setTitleText("Wait for few second");
                            // doneDialog.setContentText("Login unsuccessful");
                            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    mDrawerLayout.closeDrawer(mDrawerList);
                                    doneDialog.dismiss();
                                }
                            });
                            doneDialog.show();


                        }

                    }

                titleText.setText("");
                token = "show";

                break;
            case 9:
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                loginMethod = sharePref.getshareprefdatastring(SharePref.LOGEDIN_METHOD);

               gcmCode  = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);


                if (loginStatus .equals("yes")){
                    if(!loginMethod.equals("facebook")) {
                        fragment = new ChangePassword();
                    }else{

                      SweetAlertDialog  doneDialog = new SweetAlertDialog(DrawerActivity.this, SweetAlertDialog.ERROR_TYPE);
                        doneDialog.setTitleText("You are loged in via facebook. This page is not valid for you");
                        // doneDialog.setContentText("Login unsuccessful");
                        doneDialog.show();
                    }
                }

                mDrawerLayout.closeDrawer(mDrawerList);
                token = "show";
                titleText.setText("");
                break;
            case 3:

            {
                sharePref.setshareprefdatastring(SharePref.LOGEDIN, "no");
                sharePref.setshareprefdataBoolean(SharePref.FIRSTTIME, false);
                loginStatus = "no";
                mDrawerList.setItemChecked(position, true);
                mDrawerList.setSelection(position);
                db.clearAllCart();

                //mDrawerLayout.closeDrawer(mDrawerList);
                Intent in = new Intent(DrawerActivity.this, DrawerActivity.class);
                startActivity(in);
                finish();
            }
            titleText.setText("");
            token = "show";
            mDrawerLayout.closeDrawer(mDrawerList);
                break;

            case 4:

                fragment = new MessageList();
                titleText.setText("");
                token = "show";
                hideCartButton(true,0);
                break;

            case 5:

                fragment = new Fragment_Cart();
                titleText.setText("");
                token = "show";
                break;

            case 6:

                fragment = new Fragment_ShopDetails();
                titleText.setText("");
                token = "show";
                break;

            case 2:

                fragment = new Fragment_last_order();



                titleText.setText("");
                token = "show";
                break;
            default:
                break;
        }

        if (fragment != null) {

            mFragmentManager = getSupportFragmentManager();

            FragmentTransaction ft = mFragmentManager.beginTransaction();



            if (fragmentStack.size() > 0) {
                ft.hide(fragmentStack.lastElement());
            }
            ft.add(R.id.frame_container, fragment);
            fragmentStack.push(fragment);
            ft.commit();
            // fragmentManager.beginTransaction()
            // .replace(R.id.frame_container, fragment).commit();
            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);

            mDrawerLayout.closeDrawer(mDrawerList);

        }

//
//        if(token.equals("hide")){
//
//            newTextCounter.setVisibility(View.INVISIBLE);
//            text.setVisibility(View.INVISIBLE);
//        }else{
//            newTextCounter.setVisibility(View.VISIBLE);
//            text.setVisibility(View.VISIBLE);
//
//        }
    }



    @Override
    protected void onNewIntent(Intent intent) {

      //  setCounter();
        super.onNewIntent(intent);
    }


    @Override
    public void onResume() {
       // setCounter();

        loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);

        if(loginStatus.equals("yes")){

            initDrawerLogedIn();
        }else{

            initDrawerNotLogedIn();
        }

        if(isInternetPresent)
        mSocket.connect();

        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();
//        getApplicationContext().unregisterReceiver(mMessageReceiver);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Extract data included in the Intent
            setCounter();

            //do other stuff here
        }
    };

    public void setmDrawerTitle(String title){


        /*if(title.equals("submit")){
            toolbar_title.
        }else if(title.equals("message")){
            toolbar_title.setImageDrawable(getResources().getDrawable(R.drawable.message_title));
        }else if(title.equals("home")){
            toolbar_title.setImageDrawable(getResources().getDrawable(R.drawable.yappy));
        }else if(title.equals("settings")){
            toolbar_title.setImageDrawable(getResources().getDrawable(R.drawable.settings_title));
        }else if(title.equals("profile")){
            toolbar_title.setImageDrawable(getResources().getDrawable(R.drawable.profile_title));
        }*/



    }

    public void hideTextButton(boolean status){
        if(status){
            newTextCounter.setVisibility(View.GONE);
            text.setVisibility(View.GONE);
           titleText.setText("");

        }else{
            newTextCounter.setVisibility(View.VISIBLE);
            text.setVisibility(View.VISIBLE);
            titleText.setText("");

        }

    }

    public void showTitle(){
        titleText.setText("Select your store");
    }
    public void hideTitle(){
        titleText.setText("");
    }


    public void hideCartButton(boolean status,int count){
        if(status){
            cart.setVisibility(View.INVISIBLE);
            cartCounter.setVisibility(View.INVISIBLE);


        }else{
            cart.setVisibility(View.VISIBLE);
            cartCounter.setVisibility(View.VISIBLE);
            cartCounter.setText(count+"");
        }

    }



    @Override
    public void onBackPressed() {

        Log.i("aa", fragmentStack.size() + "");


        if(mDrawerLayout.isDrawerOpen(mDrawerList)){

            mDrawerLayout.closeDrawer(mDrawerList);
        }else {

            if (fragmentStack.size() > 1) {

                Log.i("si:", fragmentStack.size() + "");
                FragmentTransaction ft = mFragmentManager.beginTransaction();

                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());

                fragmentStack.lastElement().onResume();
                ft.show(fragmentStack.lastElement());
                Log.i("bbb", fragmentStack.lastElement() + "");
                ft.commit();

            } else {


                finish();


            }
        }
    }


    public void setCounter(){

        newTextCounter.setText(newMessageCounter+"");
    }

    public void setCounter(int i){

        newTextCounter.setText(i+"");
    }

    class AsyncTaskPending extends AsyncTask<String, String, String> {


        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();




            pair.add(new BasicNameValuePair("userId", userID));


            json = jsonParser.makeHttpRequest(Url.PENDINGMESSAGE, "POST", pair);

            Log.e("reg", json + "");



            return null;
        }

        @Override
        protected void onPreExecute() {

            try {

                Log.e("apiCall", "done");

                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                //  Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }



    }
    public void initDrawerLogedIn(){


        navDrawerItems.clear();
        navDrawerItemsImages.clear();

        navDrawerItems.add("Home");
        navDrawerItems.add("Profile");
        navDrawerItems.add("My Orders");
        navDrawerItems.add("Log Out");


        navDrawerItemsImages.add(R.drawable.home);
        navDrawerItemsImages.add(R.drawable.ic_profile1);
        navDrawerItemsImages.add(R.drawable.ic_inventory);
        navDrawerItemsImages.add(R.drawable.ic_logout);

        adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems,navDrawerItemsImages);

        mDrawerList.setAdapter(adapter);
    }


    public void initDrawerNotLogedIn(){

        navDrawerItems.clear();
        navDrawerItemsImages.clear();

        navDrawerItems.add("Home");
        navDrawerItems.add("Log In");


        navDrawerItemsImages.add(R.drawable.home);
        navDrawerItemsImages.add(R.drawable.ic_login);

        adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems,navDrawerItemsImages);

        mDrawerList.setAdapter(adapter);
    }


//    class AsyncTaskLogin extends AsyncTask<String, String, String> {
//
//
//        @Override
//        protected void onPreExecute() {
//
//            try {
//
//
//
//                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
//            } catch (IndexOutOfBoundsException e) {
//
//            }
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//
//            List<NameValuePair> pair = new ArrayList<NameValuePair>();
//
//
//            pair.add(new BasicNameValuePair("email", sharePref.getshareprefdatastring(SharePref.USEREMAIL)));
//            pair.add(new BasicNameValuePair("password", "12345678"));
//
//
//            JSONObject json = jsonParser.makeHttpRequest(Url.SOCKET_LOGIN, "POST", pair);
//
//            Log.e("json",json+"");
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            new AsyncTaskMSGLIST().execute();
//        }
//
//    }
//
//    class AsyncTaskMSGLIST extends AsyncTask<String, String, String> {
//
//
//        @Override
//        protected void onPreExecute() {
//
//            try {
//
//
//            } catch (IndexOutOfBoundsException e) {
//
//            }
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//
//            List<NameValuePair> pair = new ArrayList<NameValuePair>();
//
//
//
//            userID = sharePref.getshareprefdatastring(SharePref.USERID);
//
//
//
//            JSONObject json = jsonParser.makeHttpRequest("http://220.158.205.11/servicechat/public/users/"+userID+"/conversations", "GET", pair);
//
//
//            Log.e("json",json+"");
//
//            try {
//                boolean success = json.getBoolean("success");
//
//                if(success){
//                    conversationItemArrayList.clear();
//                    JSONArray jsonArray = json.getJSONArray("result");
//
//                    for(int i=0;i<jsonArray.length();i++){
//
//                        JSONObject item = jsonArray.getJSONObject(i);
//                        String  senderID = item.getString("author_id");
//                        String  conID = item.getString("name");
//
//                        ConversationItem conversationItem = new ConversationItem(senderID,conID);
//                        conversationItemArrayList.add(conversationItem);
//
//                    }
//
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//
//
//            connectSocket();
//            checkNewMsg();
//            // setCounter();
//        }
//
//    }

    public void checkNewMsg(){

        for(int i=0;i<conversationItemArrayList.size();i++) {
            if(!db.CheckConversation(conversationItemArrayList.get(i).getSenderID())){

                newMessageCounter++;
            }

            setCounter();
        }
    }
    public void connectSocket(){
     //   ChatApplication app = (ChatApplication) getApplication();

        ChatApplication app = new ChatApplication();
        mSocket = app.getSocket();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("user_id",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        mSocket.emit("register", jsonObject);//
        mSocket.on("joined", onNewMessage);
        mSocket.on("message", handleIncomingMessages);

        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        //  roomMap.put("room",conversationID);
        // mSocket.emit("join", roomMap);


        mSocket.connect();

        // checkNewMsg();

    }

    @Override
    protected void onRestart() {
        mSocket.connect();
        super.onRestart();
    }

    @Override
    protected void onStop() {

//        JSONObject jsonObject = new JSONObject();
//
//        try {
//            jsonObject.accumulate("userid",userID);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        Log.e("discon","disconnected");
//
//        mSocket.emit("register_offline", jsonObject);
        super.onStop();
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("test", args[0] + "");
                }
            });
        }
    };

    private Emitter.Listener handleIncomingMessages = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("data1","run");
                    JSONObject data = (JSONObject) args[0];

                    Log.e("data",data+"");

                    try{


                        JSONObject details = data.getJSONObject("sender");

                        String message = data.getString("text");
                        String dateNdTime  = data.getString("created_at").replace("T"," ").replace("."," ");

                        dateNdTime = dateNdTime.substring(0,dateNdTime.lastIndexOf(" "));

                        Log.e("d",dateNdTime);
                        String senderId = data.getString("user_id");

                      //  String senderName = details.getString("userStoreName") +details.getString("userLastName");
                        String senderName = details.getString("userStoreName") +"";
                        String room_id = data.getString("room_id");

                        String receiver_id = userID;

                        String receiver_name = "";






                        int id = 0;

                        String[] msgTime = dateNdTime.split("\\ ");

                        String date = msgTime[0];
                        String time = msgTime[1];


                        sendNotification(message,senderId,senderName,receiver_id,date,time);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    newMessageCounter++;

                    setCounter();


                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < args.length; i++) {
                        Log.e("soccet", args[i] + "");
                    }
                }
            });
        }
    };

    private void sendNotification(String message,String senderID,String senderName,String receiverID,String smsDate,String smsTime) {
        Intent intent = new Intent(this, SoccetIOChat.class);
        intent.putExtra("shopId",senderID);
        intent.putExtra("shopName",senderName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_gcm,BIND_IMPORTANT)


                .setContentTitle("New Message from "+ senderName)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}

