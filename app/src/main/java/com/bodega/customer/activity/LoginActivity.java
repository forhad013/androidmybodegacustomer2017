package com.bodega.customer.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.gcm.RegistrationIntentService;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;

    String msg;
    LoginButton loginButton;
    String userId, userFirstName, userLastName,userDeviceType, userContact;

    boolean isInternetAvailable;
    ConnectionDetector cd;

    Typeface myTypeface, custom;

    SharePref sharePref;
    String gcmRegCode;

    String token;

    EditText email, password;

    Button login, signUp, facebook;

    TextView forget_password;

    String message, emailString, passwordString, userrlMethod, userType, results;
    String loginStatus;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    int success = 0;

    ImageButton backBtn;

    JSONParser jsonParser;
    JSONArray product;

    JSONObject json, f;

    TextView ownerName;
    String fbSocialID,fbEmail,fbFullname,fbFirstName,fbLastname;

    List permission;

    private CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login);


        backBtn = (ImageButton) findViewById(R.id.backBtn);
        jsonParser = new JSONParser();
        sharePref = new SharePref(getApplicationContext());

        gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
        emailString = sharePref.getshareprefdatastring(SharePref.USEREMAIL);
        loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);

        ownerName = (TextView) findViewById(R.id.ownerName);
        Log.e("gcm", gcmRegCode);

        token = getIntent().getStringExtra("token");

        //Log.e("gcm", token);

        LoginManager.getInstance().logOut();
        cd = new ConnectionDetector(getApplicationContext());
        isInternetAvailable = cd.isConnectingToInternet();

        passwordString = sharePref.getshareprefdatastring(SharePref.USERPASSWWORD);

        email = (EditText) findViewById(R.id.emailSignUp);
        loginButton = (LoginButton)findViewById(R.id.login_button);



        loginButton.setReadPermissions("email","public_profile");
        password = (EditText) findViewById(R.id.passwordSignUp);
        login = (Button) findViewById(R.id.signIn);
        signUp = (Button) findViewById(R.id.signUp);
        forget_password = (TextView) findViewById(R.id.forgetPassword);






        custom = Typeface.createFromAsset(this.getAssets(), "fonts/RobotoCondensed-Regular.ttf");

        myTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");

        email.setText(emailString);

      //  password.setText(passwordString);


        password.setTypeface(myTypeface);


        try {
            if(!token.isEmpty())
            ownerName.setText(token+ " needs you to");

        }catch (Exception e){

        }


        callbackManager = CallbackManager.Factory.create();


        progressSweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        normalDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE);


        gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);

        if (gcmRegCode.equals("")) {
            if(isInternetAvailable) {
                if (checkPlayServices()) {


                    Intent intent = new Intent(this, RegistrationIntentService.class);
                    startService(intent);

                }
            }
        }


        gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);

        signUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),
                        RegistrationActivity.class);

                startActivity(intent);
                finish();

            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                finish();

            }
        });




        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {



                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object,GraphResponse response) {
                                try {

                                    Log.e("fbInfo",object+"" );
                                    fbSocialID =object.getString("id");
                                    fbEmail = object.getString("email");
                                    fbFullname = object.getString("name");

                                    if(fbFullname.split("\\w+").length>1){

                                        fbLastname = fbFullname.substring(fbFullname.lastIndexOf(" ")+1);
                                        fbFirstName = fbFullname.substring(0, fbFullname.lastIndexOf(' '));
                                    }
                                    else{
                                        fbFirstName = fbFullname;
                                    }



//                                    while(true){
//                                        if(!gcmRegCode.isEmpty()) {
//                                            break;
//                                        }
//
//                                    }
                                    if(isInternetAvailable) {
                                        new AsyncTaskFBlogin().execute();
                                    }else{
                                        Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                                    }
//                                    Log.e("fbInfo","Hi, " + fbFirstName +" "+ fbLastname);
//                                    Log.e("fbInfo","Hi, " + object.getString("name") +" "+ object.getString("email"));
//

                                     } catch(JSONException ex) {

                                    ex.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }


            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });


        forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),
                        ForgetPassword.class);

                startActivity(intent);
                finish();
            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                emailString = email.getText().toString().trim();
                passwordString = password.getText().toString().trim();
                userrlMethod = "0";
                userType = "0";

                //if (nullCheck()) {

                    isInternetAvailable = cd.isConnectingToInternet();
                    if (isInternetAvailable) {
                        new AsyncTaskRegValid().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                }

           // }
        });


//        if(loginStatus.equals("yes")){
//            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
//
//
//            startActivity(intent);
//
//            finish();
//        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

//    @Override
//    public void setContentView(View view)
//    {
//        super.setContentView(view);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
//    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(SharePref.GCM_REGCODE));
     //   AppEventsLogger.activateApp(this);
    }


    public void setLoginButtonEnable(boolean status){


        signUp.setEnabled(status);
         loginButton.setEnabled(status);
    };
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
       // AppEventsLogger.deactivateApp(this);


        super.onPause();

    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    class AsyncTaskFBlogin extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd", "asdasd");
                progressSweetAlertDialog.show();
                login.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("userEmail", fbEmail));
            pair.add(new BasicNameValuePair("userPassword", ""));
            pair.add(new BasicNameValuePair("newGcmCode", gcmRegCode));
            pair.add(new BasicNameValuePair("userType", "0"));
            pair.add(new BasicNameValuePair("userrlMethod", "1"));
            pair.add(new BasicNameValuePair("userSocialId", fbSocialID));
            pair.add(new BasicNameValuePair("userDeviceType", "0"));
            pair.add(new BasicNameValuePair("userFirstName", fbFirstName));
            pair.add(new BasicNameValuePair("userLastName", fbLastname));
            json = jsonParser.makeHttpRequest(Url.LOGIN, "POST", pair);

            Log.e("reg", pair + "");


            try {



                success = json.getInt("success");
                message = json.getString("message");




                if (success == 1) {
                    f = json.getJSONObject("results");

                    JSONObject jsonObject = f.getJSONObject("userInformation");

                    userId =jsonObject.getString("userId");
                    userFirstName = jsonObject.getString("userFirstName");
                    userLastName = jsonObject.getString("userLastName");
                    userDeviceType = jsonObject.getString("userDeviceType");
                    try {
                        userContact = jsonObject.getString("userContact");
                    }catch(Exception e){

                        userContact = "";
                    }
                    Log.e("asd",userFirstName);


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (success == 1) {

                progressSweetAlertDialog.dismiss();
                loginButton.setEnabled(true);
                sharePref.setshareprefdatastring(SharePref.USEREMAIL, emailString);
                sharePref.setIsLoggedIn(true);


                try {
                    sharePref.setshareprefdatastring(SharePref.USERID, userId);
                    sharePref.setshareprefdatastring(SharePref.FIRSTNAME, userFirstName);
                    sharePref.setshareprefdatastring(SharePref.LASTNAME, userLastName);
                    sharePref.setshareprefdatastring(SharePref.USEREMAIL, fbEmail);
                    sharePref.setshareprefdatastring(SharePref.GCM_REGCODE, gcmRegCode);
                    sharePref.setshareprefdatastring(SharePref.USERPHONE, userContact);
                    sharePref.setshareprefdatastring(SharePref.USERPASSWWORD, "");
                    sharePref.setshareprefdatastring(SharePref.LOGEDIN, "yes");
                    sharePref.setshareprefdatastring(SharePref.LOGEDIN_METHOD, "facebook");
                    //   sharePref.setshareprefdatastring(SharePref.USERID,userID);


                    finish();


                } catch (Exception e) {

                }
                //   Toast.makeText(getApplicationContext(),f+"p"+product+" "+userId+" "+ userLastName+" "+userLastName, Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(getApplicationContext(), DrawerActivity.class);
//                startActivity(intent);

              // new AsyncTaskSocketReg().execute();

            } else {
                progressSweetAlertDialog.dismiss();
                loginButton.setEnabled(true);
                doneDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE);
                doneDialog.setTitleText("Login unsuccessful");
                // doneDialog.setContentText("Login unsuccessful");
                doneDialog.show();
            }


           // progressSweetAlertDialog.dismiss();

        }


    }

    class AsyncTaskRegValid extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd", "asdasd");
                progressSweetAlertDialog.show();
                login.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("userEmail", emailString));
            pair.add(new BasicNameValuePair("userPassword", passwordString));
            pair.add(new BasicNameValuePair("newGcmCode", gcmRegCode));
            pair.add(new BasicNameValuePair("userType", userType));
            pair.add(new BasicNameValuePair("userDeviceType", "0"));
            pair.add(new BasicNameValuePair("userrlMethod", userrlMethod));

            json = jsonParser.makeHttpRequest(Url.LOGIN, "POST", pair);

            Log.e("reg", json + "");


            try {



                success = json.getInt("success");
                message = json.getString("message");




                if (success == 1) {
                    f = json.getJSONObject("results");

                    JSONObject jsonObject = f.getJSONObject("userInformation");

                    userId =jsonObject.getString("userId");
                    userFirstName = jsonObject.getString("userFirstName");
                    userLastName = jsonObject.getString("userLastName");
                    userDeviceType = jsonObject.getString("userDeviceType");
                    userContact = jsonObject.getString("userContact");


                    Log.e("asd",userFirstName);

                   /* if (userType.equals("1")) {

                        success = 0;
                    } else {

                    }*/
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (success == 1) {
                progressSweetAlertDialog.dismiss();

                loginButton.setEnabled(true);

                sharePref.setshareprefdatastring(SharePref.USEREMAIL, emailString);
                sharePref.setIsLoggedIn(true);


                try {
                    sharePref.setshareprefdatastring(SharePref.USERID, userId);
                    sharePref.setshareprefdatastring(SharePref.FIRSTNAME, userFirstName);
                    sharePref.setshareprefdatastring(SharePref.LASTNAME, userLastName);
                    sharePref.setshareprefdatastring(SharePref.USEREMAIL, emailString);
                    sharePref.setshareprefdatastring(SharePref.USERPHONE, userContact);
                    sharePref.setshareprefdatastring(SharePref.GCM_REGCODE, gcmRegCode);
                    sharePref.setshareprefdatastring(SharePref.USERPASSWWORD, passwordString);
                    sharePref.setshareprefdatastring(SharePref.LOGEDIN, "yes");

                    sharePref.setshareprefdatastring(SharePref.LOGEDIN_METHOD, "email");

//                    doneDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE);
//                    doneDialog.setTitleText("Success");
//                    doneDialog.setContentText("Login Successful");
//                    doneDialog.show();
//                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                        @Override
//                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
//                        }
//                    });


                } catch (Exception e) {

                }

          //     new AsyncTaskSocketLogin().execute();


            } else {
                progressSweetAlertDialog.dismiss();
                login.setEnabled(true);
                doneDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE);
                doneDialog.setTitleText("ERROR");
                doneDialog.setContentText("The email address or password you entered is incorrect");
                // doneDialog.setContentText("Login unsuccessful");
                doneDialog.show();
            }



        }


    }

    public boolean nullCheck() {
        boolean flag = false;

        if (!email.getText().toString().trim().equalsIgnoreCase("")) {
            if (!password.getText().toString().trim().equalsIgnoreCase("")) {

                return true;

            } else {
                msg = "Please Enter Password!";
            }

        } else {
            msg = "Please Enter Your name!";
        }


        if (!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }


        return flag;


    }




}
