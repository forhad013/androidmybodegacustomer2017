package com.bodega.customer.model;

/**
 * Created by HP on 02/08/2015.
 */
public final class ProductsModel {


  public  String proPrice;

  public ProductsModel(String proPrice, String proImage, String status, String proCateoryName, String proCateoryId, String proID, String proName) {
    this.proPrice = proPrice;
    this.proImage = proImage;
    this.status = status;
    this.proCateoryName = proCateoryName;
    this.proCateoryId = proCateoryId;
    this.proID = proID;
    this.proName = proName;
  }

  public  String proImage;

  public  String status;
  public  String proCateoryName;
  public  String proCateoryId;
  public  String proID;
  public  String proName;


  public String getProPrice() {
    return proPrice;
  }

  public void setProPrice(String proPrice) {
    this.proPrice = proPrice;
  }

  public String getProImage() {
    return proImage;
  }

  public void setProImage(String proImage) {
    this.proImage = proImage;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getProCateoryName() {
    return proCateoryName;
  }

  public void setProCateoryName(String proCateoryName) {
    this.proCateoryName = proCateoryName;
  }

  public String getProCateoryId() {
    return proCateoryId;
  }

  public void setProCateoryId(String proCateoryId) {
    this.proCateoryId = proCateoryId;
  }

  public String getProID() {
    return proID;
  }

  public void setProID(String proID) {
    this.proID = proID;
  }

  public String getProName() {
    return proName;
  }

  public void setProName(String proName) {
    this.proName = proName;
  }





}




