package com.bodega.customer.model;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class OrderProductModel {





    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
    public String productName;

    public OrderProductModel(String productId, String  productName, String quantity, String totalPrice) {
        this.productName = productName;
        this.productId = productId;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }

    public String productId;
    public String quantity;
    public String totalPrice;



}



