package com.bodega.customer.model;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class OrderModel {




    public OrderModel(String id, String customerid, String storeid, String firstname, String lastname, String telephone, String email, String address, String city, String postalcode, String notes, String deliverycost, String deliverytype, String specialprice, String status, String createdAt, String updatedAt, String storename) {
        this.id = id;
        this.customerid = customerid;
        this.storeid = storeid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.telephone = telephone;
        this.email = email;
        this.address = address;
        this.city = city;
        this.postalcode = postalcode;
        this.notes = notes;
        this.deliverycost = deliverycost;
        this.deliverytype = deliverytype;
        this.specialprice = specialprice;
        this.status = status;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.storename = storename;
    }

    private String id;

    private String customerid;

    private String storeid;

    private String firstname;

    private String lastname;

    private String telephone;

    private String email;

    private String address;

    private String city;

    private String postalcode;

    private String notes;

    private String deliverycost;

    private String deliverytype;

    private String specialprice;

    private String status;

    private String createdAt;

    private String updatedAt;

    private String storename;



    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getCustomerid() {
        return customerid;
    }



    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }



    public String getStoreid() {
        return storeid;
    }



    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }



    public String getFirstname() {
        return firstname;
    }



    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }



    public String getLastname() {
        return lastname;
    }


    public void setLastname(String lastname) {
        this.lastname = lastname;
    }



    public String getTelephone() {
        return telephone;
    }



    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getAddress() {
        return address;
    }



    public void setAddress(String address) {
        this.address = address;
    }



    public String getCity() {
        return city;
    }


    public void setCity(String city) {
        this.city = city;
    }



    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }



    public String getNotes() {
        return notes;
    }



    public void setNotes(String notes) {
        this.notes = notes;
    }


    public String getDeliverycost() {
        return deliverycost;
    }



    public void setDeliverycost(String deliverycost) {
        this.deliverycost = deliverycost;
    }


    public String getDeliverytype() {
        return deliverytype;
    }



    public void setDeliverytype(String deliverytype) {
        this.deliverytype = deliverytype;
    }


    public String getSpecialprice() {
        return specialprice;
    }


    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }



    public String getStatus() {
        return status;
    }



    public void setStatus(String status) {
        this.status = status;
    }



    public String getCreatedAt() {
        return createdAt;
    }



    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    public String getUpdatedAt() {
        return updatedAt;
    }



    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }



    public String getStorename() {
        return storename;
    }


    public void setStorename(String storename) {
        this.storename = storename;
    }




}



