package com.bodega.customer.model;

import java.util.ArrayList;

/**
 * Created by HP on 02/08/2015.
 */
public final class ShopModel {


  public  String proID;
    public String proName;
    public  String proBrand;
    public  String proCategory;

  public String getProID() {
    return proID;
  }

  public void setProID(String proID) {
    this.proID = proID;
  }

  public String getProName() {
    return proName;
  }

  public void setProName(String proName) {
    this.proName = proName;
  }

  public String getProBrand() {
    return proBrand;
  }

  public void setProBrand(String proBrand) {
    this.proBrand = proBrand;
  }

  public String getProCategory() {
    return proCategory;
  }

  public void setProCategory(String proCategory) {
    this.proCategory = proCategory;
  }

  public String getProPrice() {
    return proPrice;
  }

  public void setProPrice(String proPrice) {
    this.proPrice = proPrice;
  }

  public String getProCode() {
    return proCode;
  }

  public void setProCode(String proCode) {
    this.proCode = proCode;
  }

  public String getProInfo() {
    return proInfo;
  }

  public void setProInfo(String proInfo) {
    this.proInfo = proInfo;
  }

  public ArrayList<String> getProImage() {
    return proImage;
  }

  public void setProImage(ArrayList<String> proImage) {
    this.proImage = proImage;
  }

  public ArrayList<ShopSubModel> getSubProducts() {
    return subProducts;
  }

  public void setSubProducts(ArrayList<ShopSubModel> subProducts) {
    this.subProducts = subProducts;
  }

  public  String proPrice;
    public  String proInfo;
    public  String proCode;
    public  ArrayList<String> proImage;



    public ArrayList<ShopSubModel> subProducts;


    public ShopModel(){


        proImage= new ArrayList<String>();

        subProducts=new ArrayList<ShopSubModel>();
    }

}




