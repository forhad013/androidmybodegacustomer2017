package com.bodega.customer.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by HP on 02/08/2015.
 */
public final class MarkerModel implements ClusterItem{

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getShopInfo() {
    return shopInfo;
  }

  public void setShopInfo(String shopInfo) {
    this.shopInfo = shopInfo;
  }

  public void setPosition(LatLng position) {
    this.position = position;
  }

  public MarkerModel(String shopName, String shopInfo, LatLng position) {
    this.shopName = shopName;
    this.shopInfo = shopInfo;
    this.position = position;
  }

  public  String shopName;
    public  String shopInfo;
    public LatLng position;


  @Override
  public LatLng getPosition() {
    return null;
  }
}




