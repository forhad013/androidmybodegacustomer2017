package com.bodega.customer.model;

/**
 * Created by HP on 02/08/2015.
 */
public final class PopularProductModel {




  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String price;

  public PopularProductModel(String productId, String productName,String price, String status ) {
    this.price = price;
    this.status = status;
    this.productId = productId;
    this.productName = productName;
  }

  public String status;
  public String productId;
  public String productName;


}




