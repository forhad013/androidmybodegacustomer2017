package com.bodega.customer.twillio;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.utils.SharePref;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.twilio.client.Connection;
import com.twilio.client.ConnectionListener;
import com.twilio.client.Device;
import com.twilio.client.DeviceListener;
import com.twilio.client.PresenceEvent;
import com.twilio.client.Twilio;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

public class ClientActivity extends AppCompatActivity implements DeviceListener, ConnectionListener,View.OnClickListener {

    private static final String TAG = ClientActivity.class.getName();

    private static final int MIC_PERMISSION_REQUEST_CODE = 1;

    /*
     * You must provide a publicly accessible server to generate a Capability Token to connect to the Client service
     * Refer to website documentation for additional details: https://www.twilio.com/docs/quickstart/php/android-client
     */
    private static final String TOKEN_SERVICE_URL = "https://lit-bayou-97655.herokuapp.com/token";

    /*
     * A Device is the primary entry point to Twilio Services
     */
    private Device clientDevice;

    /*
     * A Connection represents a connection between a Device and Twilio Services.
     * Connections are either outgoing or incoming, and not created directly.
     * An outgoing connection is created by Device.connect()
     * An incoming connection are created internally by a Device and hanged to the registered PendingIntent
     */
    private Connection activeConnection;
    private Connection pendingConnection;

    private AudioManager audioManager;
    private int savedAudioMode = AudioManager.MODE_INVALID;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.lin2) {

            //    dialButton.setEnabled(true);
            title.setText("Call Finished");
            disconnect();
        }
    }

    /*
     * A representation of the current properties of a client token
     */
    protected class ClientProfile {
        private String name;
        private boolean allowOutgoing = true;
        private boolean allowIncoming = true;


        public ClientProfile(String name, boolean allowOutgoing, boolean allowIncoming) {
            this.name = name;
            this.allowOutgoing = allowOutgoing;
            this.allowIncoming = allowIncoming;
        }

        public String getName() {
            return name;
        }

        public boolean isAllowOutgoing() {
            return allowOutgoing;
        }

        public boolean isAllowIncoming() {
            return allowIncoming;
        }
    }

    /*
     * Android application UI elements
     */
    protected boolean _active = true;
    protected int _splashTime = 5000;

    private EditText numberField;
    public TextView title;
    String ownerName,ownerPic,contact;
    CircleImageView image;
    public ImageButton dialButton,hangupButton;
    LinearLayout lin1,lin2;

    Thread thread;

    ImageButton backBtn;

    SweetAlertDialog progressSweetAlertDialog;
    private ClientProfile clientProfile;
    private AlertDialog alertDialog;
    private Chronometer chronometer;
    private View callView;
    private View capabilityPropertiesView;

    private boolean muteMicrophone;
    private boolean speakerPhone;

    SharePref sharePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twillio_call);

        image  = (CircleImageView)findViewById(R.id.image);
        title =  (TextView)findViewById(R.id.title);

        backBtn = (ImageButton) findViewById(R.id.backBtn);
        ownerName = getIntent().getStringExtra("ownerName");
        ownerPic = getIntent().getStringExtra("image");
        contact = getIntent().getStringExtra("storeContact");


        sharePref = new SharePref(getApplicationContext());


//        clientDevice.disconnectAll();

                contact = contact.replace("(","");
        contact = contact.replace(")","");
        contact = contact.replace("-","");
        contact = contact.replace("+","");
        contact = contact.replaceAll(" ","");
        //contact = "+"+contact;
        // contact = "9177839161";
        lin2 = (LinearLayout) findViewById(R.id.lin2);

        Log.e("contact",contact);

        hangupButton = (ImageButton)findViewById(R.id.hangupButton);
        lin2.setOnClickListener(this);

        setData();
        /*
         * Create a default profile (name=jenny, allowOutgoing=true, allowIncoming=true)
         */
        clientProfile = new ClientProfile(sharePref.getshareprefdatastring(SharePref.USERNAME), true, true);
        progressSweetAlertDialog = new SweetAlertDialog(ClientActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        progressSweetAlertDialog.show();
        /*
         * Needed for setting/abandoning audio focus during call
         */
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        /*
         * Check microphone permissions. Needed in Android M.
         */
        if (!checkPermissionForMicrophone()) {
            requestPermissionForMicrophone();
        } else {
            /*
             * Initialize the Twilio Client SDK
             */
            initializeTwilioClientSDK();
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnect();

//                clientDevice.disconnectAll();
   //             clientDevice.release();
                finish();
            }
        });

        /*
         * Set the initial state of the UI
         */



        thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(5000);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressSweetAlertDialog.dismiss();
                                makeCall();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            };
        };
        thread.start();
    }

    /*
     * Initialize the Twilio Client SDK
     */
    private void initializeTwilioClientSDK() {

        if (!Twilio.isInitialized()) {
            Twilio.initialize(getApplicationContext(), new Twilio.InitListener() {

                /*
                 * Now that the SDK is initialized we can register using a Capability Token.
                 * A Capability Token is a JSON Web Token (JWT) that specifies how an associated Device
                 * can interact with Twilio services.
                 */
                @Override
                public void onInitialized() {
                    Twilio.setLogLevel(Log.DEBUG);
                    /*
                     * Retrieve the Capability Token from your own web server
                     */
                    retrieveCapabilityToken(clientProfile);
                }

                @Override
                public void onError(Exception e) {
                    Log.e(TAG, e.toString());
                    Toast.makeText(ClientActivity.this, "Failed to initialize the Twilio Client SDK", Toast.LENGTH_LONG).show();
                }
            });
        }
    }


    public void disconnectTwilio(){
        Twilio.shutdown();

    }

    /*
     * Create a Device or update the capabilities of the current Device
     */
    private void createDevice(String capabilityToken) {

       // clientDevice.release();

        try {
            if (clientDevice == null) {

                Log.e("null","null");
                clientDevice = Twilio.createDevice(capabilityToken, this);
                Log.e("check",clientDevice.getState().name()+"");
                /*
                 * Providing a PendingIntent to the newly created Device, allowing you to receive incoming calls
                 *
                 *  What you do when you receive the intent depends on the component you set in the Intent.
                 *
                 *  If you're using an Activity, you'll want to override Activity.onNewIntent()
                 *  If you're using a Service, you'll want to override Service.onStartCommand().
                 *  If you're using a BroadcastReceiver, override BroadcastReceiver.onReceive().
                 */

                Intent intent = new Intent(getApplicationContext(), ClientActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                clientDevice.setIncomingIntent(pendingIntent);
            } else {

                Log.e("update","update");
                clientDevice.updateCapabilityToken(capabilityToken);
            }



        } catch (Exception e) {
            Log.e(TAG, "An error has occured updating or creating a Device: \n" + e.toString());
           // Toast.makeText(ClientActivity.this, "Device error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    /*
     * Receive intent for incoming call from Twilio Client Service
     * Android will only call Activity.onNewIntent() if `android:launchMode` is set to `singleTop`.
     */
    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    /*
     * Request a Capability Token from your public accessible server
     */
    private void retrieveCapabilityToken(final ClientProfile newClientProfile) {

        // Correlate desired properties of the Device (from ClientProfile) to properties of the Capability Token
        Uri.Builder b = Uri.parse(TOKEN_SERVICE_URL).buildUpon();
        if (newClientProfile.isAllowOutgoing()) {
            b.appendQueryParameter("allowOutgoing", newClientProfile.allowOutgoing ? "true" : "false");
        }
        if (newClientProfile.isAllowIncoming() && newClientProfile.getName() != null) {
         //   b.appendQueryParameter("client", newClientProfile.getName());
        }

        Ion.with(getApplicationContext())
                .load(b.toString())
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String capabilityToken) {
                        if (e == null) {
                            Log.d("capabilityToken", capabilityToken);

                            // Update the current Client Profile to represent current properties
                            ClientActivity.this.clientProfile = newClientProfile;

                            // Create a Device with the Capability Token
                            createDevice(capabilityToken);
                        } else {
                            Log.e(TAG, "Error retrieving token: " + e.toString());
                        //    Toast.makeText(ClientActivity.this, "Error retrieving token", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void makeCall(){

        try {

            connect(contact,true);


            //   lin1.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_background));

            //   dialButton.setEnabled(false);

            title.setText("Call In Progress.....");
        }catch (Exception e){
            e.printStackTrace();
            title.setText("Call Failed");
            // Toast.makeText(getApplicationContext(),"Can't call on this number",Toast.LENGTH_SHORT);
        }

    }

    /*
     * Create an outgoing connection
     */
    private void connect(String contact, boolean isPhoneNumber) {
        // Determine if you're calling another client or a phone number
        if (!isPhoneNumber) {
            contact = "client:" + contact.trim();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("To", contact);

        //Log.e("clientDevice",clientDevice.getCapabilityToken()+"");

        if (clientDevice != null) {
            // Create an outgoing connection
            activeConnection = clientDevice.connect(params, this);

        } else {
           // Toast.makeText(ClientActivity.this, "No existing device", Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * Disconnect an active connection
     */
    private void disconnect() {
        if (activeConnection != null) {
            activeConnection.disconnect();
            activeConnection = null;
            disconnectTwilio();

            endThis();
        }
    }

    @Override
    public void onBackPressed() {
        disconnect();

//                clientDevice.disconnectAll();
        //             clientDevice.release();
        finish();
        super.onBackPressed();
    }

    /*
     * The initial state when there is no active connection
     */


    /*
     * Reset UI elements
     */
    private void resetUI() {

     //   callView.setVisibility(View.INVISIBLE);
      //  chronometer.setVisibility(View.INVISIBLE);

        muteMicrophone = false;
        speakerPhone = false;

        setAudioFocus(false);
        audioManager.setSpeakerphoneOn(speakerPhone);


    }

    /*
     * The UI state when there is an active connection
     */


    /*
     * Creates an update token UI dialog
     */

    /*
     * Create an outgoing call UI dialog
     */


    /*
     * Creates an incoming call UI dialog
     */




    /* Device Listener */
    @Override
    public void onStartListening(Device device) {
        Log.d(TAG, "Device has started listening for incoming connections");
    }

    /* Device Listener */
    @Override
    public void onStopListening(Device device) {
        Log.d(TAG, "Device has stopped listening for incoming connections");
    }

    /* Device Listener */
    @Override
    public void onStopListening(Device device, int errorCode, String error) {
        Log.e(TAG, String.format("Device has encountered an error and has stopped" +
                " listening for incoming connections: %s", error));
    }

    /* Device Listener */
    @Override
    public boolean receivePresenceEvents(Device device) {
        return false;
    }

    /* Device Listener */
    @Override
    public void onPresenceChanged(Device device, PresenceEvent presenceEvent) {
    }

    /* Connection Listener */
    @Override
    public void onConnecting(Connection connection) {
        Log.d(TAG, "Attempting to connect");
    }

    /* Connection Listener */
    @Override
    public void onConnected(Connection connection) {
        Log.d(TAG, "Connected");
    }

    /* Connection Listener */
    @Override
    public void onDisconnected(Connection connection) {
        // Remote participant may have disconnected an incoming call before the local participant was able to respond, rejecting any existing pendingConnections
        if( connection == pendingConnection ) {
            pendingConnection = null;
            alertDialog.dismiss();
        } else if (activeConnection != null && connection != null) {
            if (activeConnection == connection) {
                activeConnection = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        resetUI();
                    }
                });
            }
            Log.d(TAG, "Disconnect");
        }
    }

    /* Connection Listener */
    @Override
    public void onDisconnected(Connection connection, int errorCode, String error) {
        // A connection other than active connection could have errored out.
        if (activeConnection != null && connection != null) {
            if (activeConnection == connection) {
                activeConnection = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        resetUI();
                    }
                });
            }
            Log.e(TAG, String.format("Connection error: %s", error));
        }
    }



    private boolean checkPermissionForMicrophone() {
        int resultMic = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        if (resultMic == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissionForMicrophone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(this,
                    "Microphone permissions needed. Please allow in App Settings for additional functionality.",
                    Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    MIC_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        /*
         * Check if microphone permissions is granted
         */
        if (requestCode == MIC_PERMISSION_REQUEST_CODE && permissions.length > 0) {
            boolean granted = true;
            if (granted) {
                /*
                * Initialize the Twilio Client SDK
                */
                initializeTwilioClientSDK();
            } else {
                Toast.makeText(this,
                        "Microphone permissions needed. Please allow in App Settings for additional functionality.",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setAudioFocus(boolean setFocus) {
        if (audioManager != null) {
            if (setFocus) {
                savedAudioMode = audioManager.getMode();
                // Request audio focus before making any device switch.
                audioManager.requestAudioFocus(null, AudioManager.STREAM_VOICE_CALL,
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                /*
                 * Start by setting MODE_IN_COMMUNICATION as default audio mode. It is
                 * required to be in this mode when playout and/or recording starts for
                 * best possible VoIP performance. Some devices have difficulties with speaker mode
                 * if this is not set.
                 */
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            } else {
                audioManager.setMode(savedAudioMode);
                audioManager.abandonAudioFocus(null);
            }
        }
    }
    public void setData(){


        try {

            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                    .defaultDisplayImageOptions(defaultOptions)
                    .build();
            ImageLoader.getInstance().init(config);


            ImageLoader.getInstance().displayImage(ownerPic, image, defaultOptions);
        }catch (Exception e){

            e.printStackTrace();


        }




    }




    public void endThis(){

        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (_active && (waited < _splashTime)) {
                        sleep(100);
                        if (_active) {
                            waited += 100;
                        }
                    }
                } catch (Exception e) {

                } finally {


                    finish();

                }
            };
        };
        splashTread.start();
    }

}
