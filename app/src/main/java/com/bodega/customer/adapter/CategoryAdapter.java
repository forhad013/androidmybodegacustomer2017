package com.bodega.customer.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.fragment.Fragment_product;
import com.bodega.customer.model.CateogryModel;
import com.bodega.customer.utils.FontChangeCrawler;

import java.util.ArrayList;


public class CategoryAdapter extends BaseAdapter {




	int tag;
    String token;
	ArrayList<CateogryModel> cateogryModelArrayList;

	private Context context;

	FontChangeCrawler fontChanger;

	public CategoryAdapter(Context context,
						   ArrayList<CateogryModel> cateogryModelArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.cateogryModelArrayList = cateogryModelArrayList;



	//	fontChanger = new FontChangeCrawler(context.getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");



	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return cateogryModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.category_item, parent, false);
	//	}

		TextView nameTxt = (TextView) view.findViewById(R.id.categoryName);
		ImageButton edit = (ImageButton) view.findViewById(R.id.edit);
		ImageButton forward = (ImageButton) view.findViewById(R.id.forward);
	 		nameTxt.setText(cateogryModelArrayList.get(position).catName);




		forward.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Fragment fragment = new Fragment_product();

				((DrawerActivity)context).mFragmentManager = ((DrawerActivity)context).getSupportFragmentManager();

				FragmentTransaction ft = ((DrawerActivity)context).mFragmentManager.beginTransaction();

				Bundle args = new Bundle();
				args.putString("catID", cateogryModelArrayList.get(position).getCatID());
				args.putString("catTitle", cateogryModelArrayList.get(position).getCatName());

				fragment.setArguments(args);


				ft.add(R.id.frame_container, fragment);
				((DrawerActivity)context).fragmentStack.push(fragment);
				ft.commit();
			}
		});


		return view;
	}



}