package com.bodega.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.model.PopularProductModel;

import java.util.ArrayList;



public class GridProductAdapter extends BaseAdapter {



    ArrayList<PopularProductModel> popularProductModelArrayList;
    int tag;
    String token;

    private Context context;

    public GridProductAdapter(Context context,
                             ArrayList<PopularProductModel> popularProductModelArrayListtPrice) {

        // TODO Auto-generated constructor stub
        this.context = context;
        this.popularProductModelArrayList = popularProductModelArrayListtPrice;


    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return popularProductModelArrayList.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;


        if (view == null) {

            view = LayoutInflater.from(context).inflate(
                    R.layout.grid_item, parent, false);
        }

        TextView title1 = (TextView) view.findViewById(R.id.item);







        String text = popularProductModelArrayList.get(position).getProductName();

        text = text  + " " + popularProductModelArrayList.get(position).getPrice();



        title1.setText(text);



        return view;
    }

}