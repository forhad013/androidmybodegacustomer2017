package com.bodega.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.model.OrderProductModel;
import com.bodega.customer.utils.FontChangeCrawler;
import com.bodega.customer.utils.SharePref;

import java.text.DecimalFormat;
import java.util.ArrayList;


public class OrderProductAdapter extends BaseAdapter {

	View.OnClickListener mOnClickListener;


	int tag;
    String token;
	ArrayList<OrderProductModel> orderProductModelArrayList;

	private Context context;

	FontChangeCrawler fontChanger;

	MyDatabase db;

	SharePref sharePref;

	public OrderProductAdapter(Context context,
							   ArrayList<OrderProductModel> orderProductModelArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.orderProductModelArrayList = orderProductModelArrayList;


		db = new MyDatabase(context);

		sharePref = new SharePref(context);

	//	fontChanger = new FontChangeCrawler(context.getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");



	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return orderProductModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.order_product_item, parent, false);
	//	}

		TextView nameTxt = (TextView) view.findViewById(R.id.name);
		TextView price = (TextView) view.findViewById(R.id.price);


		DecimalFormat df = new DecimalFormat("#.00");
		df.format(Float.parseFloat(orderProductModelArrayList.get(position).getTotalPrice()));

	 		nameTxt.setText(orderProductModelArrayList.get(position).getQuantity()+" * "+orderProductModelArrayList.get(position).getProductName());
		price.setText("$"+ df.format(Float.parseFloat(orderProductModelArrayList.get(position).getTotalPrice())));

//		edit.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent i = new Intent(context,AddProduct.class);
//
//				i.putExtra("token","edit");
//				i.putExtra("product_id",orderProductModelArrayList.get(position).getId());
//				i.putExtra("product_price",orderProductModelArrayList.get(position).getPrice());
//				i.putExtra("name",orderProductModelArrayList.get(position).getTitle());
//				i.putExtra("status",orderProductModelArrayList.get(position).getStatus());
//				i.putExtra("categoryID",orderProductModelArrayList.get(position).getCategoryId());
//				i.putExtra("categoryTitle",orderProductModelArrayList.get(position).getCategoryName());
//				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				context.startActivity(i);
//			}
//		});







		return view;
	}



}