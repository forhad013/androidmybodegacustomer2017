package com.bodega.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.model.NewMessageItem;
import com.bodega.customer.utils.FontChangeCrawler;
import com.bodega.customer.utils.SharePref;

import java.util.ArrayList;


public class NewMessageListAdapter extends BaseAdapter {



	FontChangeCrawler fontChanger ;

	int tag;
    String token;
	ArrayList<NewMessageItem> messageItemArrayList;

	private Context context;

	SharePref sharePref;

	String currentUserID;

	public NewMessageListAdapter(Context context,
								 ArrayList<NewMessageItem> messageItemArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.messageItemArrayList = messageItemArrayList;


		sharePref = new SharePref(context);


		currentUserID = sharePref.getshareprefdatastring(SharePref.USERID);



	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return messageItemArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;



			view = LayoutInflater.from(context).inflate(
					R.layout.message_item, parent, false);


		TextView messageTextOther = (TextView) view.findViewById(R.id.messageText);
		TextView messageTextMe = (TextView) view.findViewById(R.id.messageTextMe);
		TextView timeOther = (TextView) view.findViewById(R.id.time);
		TextView timetMe = (TextView) view.findViewById(R.id.timeMe);
		TextView dateTxt = (TextView) view.findViewById(R.id.date);
		 fontChanger = new FontChangeCrawler(context .getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");


	//	fontChanger.replaceFonts((ViewGroup)view);

//		CircleImageView person2 = (CircleImageView) view.findViewById(R.id.secondPerson);
//		CircleImageView person1 = (CircleImageView) view.findViewById(R.id.firstPerson);





		String senderID = messageItemArrayList.get(position).getSenderID();

		String messageID = messageItemArrayList.get(position).getID();

		String message = messageItemArrayList.get(position).getMessage();


//		Log.e("md",message);

		String time = messageItemArrayList.get(position).getTime();

		String date = messageItemArrayList.get(position).getDate();
		String receiverID = messageItemArrayList.get(position).getReceiverID();

		if(position==0){
			dateTxt.setText(date);
			dateTxt.setVisibility(View.VISIBLE);
		}else if(date.equals(messageItemArrayList.get(position-1).getDate())){

			dateTxt.setVisibility(View.GONE);
		}else {
			dateTxt.setText(date);
			dateTxt.setVisibility(View.VISIBLE);
		}

		if(senderID.equals(currentUserID)){
			timetMe.setText(time);
			messageTextMe.setText(message);

			messageTextOther.setVisibility(View.GONE);
			messageTextMe.setVisibility(View.VISIBLE);

		}else{
			timeOther.setText(time);
			messageTextOther.setText(message);
			messageTextMe.setVisibility(View.GONE);
			messageTextOther.setVisibility(View.VISIBLE);
		}



		return view;
	}



}