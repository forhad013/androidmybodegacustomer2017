package com.bodega.customer.adapter;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class ConversationItem {


    public ConversationItem(String senderID, String conversionID) {
        this.senderID = senderID;
        this.conversionID = conversionID;
    }

    public String senderID;
    public String conversionID;

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getConversionID() {
        return conversionID;
    }

    public void setConversionID(String conversionID) {
        this.conversionID = conversionID;
    }
}
