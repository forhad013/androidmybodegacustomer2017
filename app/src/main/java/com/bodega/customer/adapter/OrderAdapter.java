package com.bodega.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.model.OrderModel;
import com.bodega.customer.utils.FontChangeCrawler;
import com.bodega.customer.utils.SharePref;

import java.util.ArrayList;


public class OrderAdapter extends BaseAdapter {

	View.OnClickListener mOnClickListener;


	int tag;
    String token;
	ArrayList<OrderModel> orderModelArrayList;

	private Context context;

	FontChangeCrawler fontChanger;

	MyDatabase db;

	SharePref sharePref;

	public OrderAdapter(Context context,
						ArrayList<OrderModel> orderModelArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.orderModelArrayList = orderModelArrayList;


		db = new MyDatabase(context);

		sharePref = new SharePref(context);

	//	fontChanger = new FontChangeCrawler(context.getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");



	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return orderModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.order_item, parent, false);
	//	}

		TextView storeName = (TextView) view.findViewById(R.id.storeName);
		TextView amount = (TextView) view.findViewById(R.id.amount);
		TextView status = (TextView) view.findViewById(R.id.status);
		TextView date = (TextView) view.findViewById(R.id.date);
		TextView time = (TextView) view.findViewById(R.id.time);
		TextView type = (TextView) view.findViewById(R.id.type);
		TextView payment = (TextView) view.findViewById(R.id.payment);

		String[] orderTime = orderModelArrayList.get(position).getUpdatedAt().split("\\ ");

		String dateS = orderTime[0];
		String timeS = orderTime[1];


		storeName.setText(orderModelArrayList.get(position).getStorename());

		String x = orderModelArrayList.get(position).getStatus();

		if(x.equals("0")){
			x = "Ordered";
		}else if(x.equals("1")){
			x.equals("Preparing");
		}else if(x.equals("2")) {
			x.equals("Delivered");
		}else if(x.equals("3"))
		{
			x.equals("Canceled");
		}

		status.setText("Status : "+x);
		date.setText("Date : "+dateS);
		time.setText("Time : "+timeS);
		type.setText("Order Type : "+orderModelArrayList.get(position).getDeliverytype());








		return view;
	}



}