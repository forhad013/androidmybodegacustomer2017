package com.bodega.customer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.model.ProductModel;
import com.bodega.customer.utils.FontChangeCrawler;
import com.bodega.customer.utils.SharePref;

import java.util.ArrayList;


public class ProductAdapter extends BaseAdapter {

	View.OnClickListener mOnClickListener;


	int tag;
    String token;
	ArrayList<ProductModel> productModelArrayList;

	private Context context;

	FontChangeCrawler fontChanger;

	MyDatabase db;

	SharePref sharePref;

	public ProductAdapter(Context context,
						  ArrayList<ProductModel> productModelArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.productModelArrayList = productModelArrayList;


		db = new MyDatabase(context);

		sharePref = new SharePref(context);

	//	fontChanger = new FontChangeCrawler(context.getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");



	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return productModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.product_item, parent, false);
	//	}

		TextView nameTxt = (TextView) view.findViewById(R.id.name);
		TextView price = (TextView) view.findViewById(R.id.price);
		ImageButton addToCart = (ImageButton) view.findViewById(R.id.addToCart);

	 		nameTxt.setText(productModelArrayList.get(position).getTitle());
		price.setText("$"+productModelArrayList.get(position).getPrice());


//		edit.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent i = new Intent(context,AddProduct.class);
//
//				i.putExtra("token","edit");
//				i.putExtra("product_id",productModelArrayList.get(position).getId());
//				i.putExtra("product_price",productModelArrayList.get(position).getPrice());
//				i.putExtra("name",productModelArrayList.get(position).getTitle());
//				i.putExtra("status",productModelArrayList.get(position).getStatus());
//				i.putExtra("categoryID",productModelArrayList.get(position).getCategoryId());
//				i.putExtra("categoryTitle",productModelArrayList.get(position).getCategoryName());
//				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				context.startActivity(i);
//			}
//		});

		addToCart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				db.checkCart(productModelArrayList.get(position).getStoreID(),sharePref.getshareprefdatastring(SharePref.CURRENTSHOPNAME),
						productModelArrayList.get(position).getId(),productModelArrayList.get(position).getTitle()
				,"1",productModelArrayList.get(position).getPrice());

				setCounter(position);

			}
		});






		return view;
	}

	public void setCounter(int position){

		int count = db.getCartCounter(productModelArrayList.get(position).getStoreID());

		((DrawerActivity)context).hideCartButton(false,count);

	}


}