package com.bodega.customer.gcm;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.bodega.customer.utils.SharePref;


public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    SharePref sharePref;

    String logedID;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("Broadcast", "----------------------- GCM Broad cast");

        sharePref = new SharePref(context);

       logedID = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
         if (logedID.equals("yes")) {
            ComponentName comp = new ComponentName(context.getPackageName(),
                    MyGcmListenerService.class.getName());
            startWakefulService(context, (intent.setComponent(comp)));
            setResultCode(Activity.RESULT_OK);
        }
    }
}
