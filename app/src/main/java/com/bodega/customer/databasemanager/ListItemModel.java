package com.bodega.customer.databasemanager;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class ListItemModel {

    public String id;
    public String type;
    public String date;
    public String item;
    public String note;
    public String price;
    public String subitem;




    public String getSubitem() {
        return subitem;
    }

    public void setSubitem(String subitem) {
        this.subitem = subitem;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }




    public ListItemModel(String id, String date, String price, String note, String item, String type,String subitem) {


        this.id = id;
        this.date = date;
        this.price = price;
        this.note = note;
        this.item = item;
        this.type = type;
        this.subitem = subitem;


    }



}
