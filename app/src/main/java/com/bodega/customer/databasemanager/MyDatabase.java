package com.bodega.customer.databasemanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bodega.customer.adapter.MessageListItem;
import com.bodega.customer.adapter.NewMessageListItem;
import com.bodega.customer.model.CartListModel;
import com.bodega.customer.utils.SharePref;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mac on 11/12/15.
 */
public class MyDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "bodega.sqlite";
    private static final int DATABASE_VERSION = 1;

    SharePref sharePref;

    static public String USERID = "userID";
    static public String STOREID = "storeID";

    static public String PRODUCTID = "productID";
    static public String PRODUCTNAME = "productName";
    static public String PRODUCTCATEGORY = "productCategory";
    static public String PRODUCTPRICE = "productPrice";

    static public String PRODUCTQUANTITY = "quantity";
    static public String PRODUCTIMAGE = "image";

    SimpleDateFormat showDateFormate, fromCalender;

    String myID;
    Calendar calendar;

    MessageItem messageItem;

    MessageListItem messageListItem;

    ArrayList<MessageListItem> messageListItemArrayList;

    ArrayList<MessageItem> messageItemArrayList;
    SimpleDateFormat formatter, timeFormat;


    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        formatter = new SimpleDateFormat("yyyy-MM-dd");
        timeFormat = new SimpleDateFormat("hh:mm a");




        sharePref = new SharePref(context);
        myID = sharePref.getshareprefdatastring(SharePref.USERID);
        messageItemArrayList = new ArrayList<>();

        messageListItemArrayList = new ArrayList<>();


    }

    public void delete(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String ID = id;
        //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
        // Cursor cursor = db.rawQuery(retrieve, null);

        db.delete("accounts", "ID" + "=" + ID, null);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();
    }

    public void saveMessage(String fromID, String shopID, String shopName, String type, String message, String status) {
        SQLiteDatabase db = this.getReadableDatabase();
        calendar = Calendar.getInstance();

        Date date = calendar.getTime();

        String dateForDb = formatter.format(date);

        String timeForDb = timeFormat.format(date);


        Log.e("asd",date+"");

        ContentValues cv = new ContentValues();
        cv.put("myID", fromID);
        cv.put("shopID", shopID);
        cv.put("shopName", shopName);

        cv.put("type", type);
        cv.put("message", message);

        cv.put("date", dateForDb);
        cv.put("time", timeForDb);
        cv.put("status", status);


        String ID = "ID";
        //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
        // Cursor cursor = db.rawQuery(retrieve, null);

        db.insert("inbox", null, cv);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();

    }


    public void clear() {

        SQLiteDatabase db = this.getReadableDatabase();


        db.execSQL("delete from  accounts");

        db.close();
    }



    public boolean CheckConversation(String userID) {
        Cursor c;
        String searchDate = "";


        boolean status = false;

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from conversation where userID= "+userID;




        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {


            status = true;

        }

        db.close();


        return status;

    }


    public String getConversation(String userID) {
        Cursor c;
        String searchDate = "";


        String conID = "";

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from conversation where userID= "+userID;


        c = db.rawQuery(retrieve, null);

        Log.e("c", c.getCount() + "");

        if (c.getCount() != 0) {

            if (c != null) {

              if (c.moveToFirst()) {
                    do {


                        conID = c.getString(c.getColumnIndex("conID"));


                    } while (c.moveToNext());


                }
            }
        }

        db.close();


        return conID;

    }



    public void saveConversationID(String userID,String conversationID,String userName){


        SQLiteDatabase db = this.getReadableDatabase();


        ContentValues cv = new ContentValues();
        cv.put(USERID, userID);
        cv.put("conID", conversationID);
        cv.put("userName", userName);
        cv.put("status", "1");

        String ID = "ID";



        db.insert("conversation", null, cv);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();


    }
    public void updateUnRead(String userID) {
        SQLiteDatabase db = this.getReadableDatabase();

        String ID = userID;

        // db.update("cart","ID" + "=" + ID, null);
        ContentValues cv = new ContentValues();

        cv.put("status", "0");



        Log.e("update","updated");


        db.update("conversation", cv, "userID  =?", new String[]{userID});

        db.close();
    }


    public int checkUnread() {

        int count=0;
        Cursor c;
        String searchDate = "";


        ArrayList storeIdList = new ArrayList();

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from conversation where status= 0";

        Log.e("check", retrieve);


        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

//            if (c != null) {
//                if (c.moveToFirst()) {
//                    do {
//
//
//
//
//                    } while (c.moveToNext());
//
//                }
//
//            }

        }

        db.close();

        count = c.getCount();

        return count;

    }

    public void createCartTable(){


        String CREATE_ONE_TABLE = "CREATE TABLE " + "IF NOT EXISTS cart " + "("
                + "ID" + " INTEGER PRIMARY KEY,"
                + USERID + " TEXT," + STOREID + " TEXT,"
                + PRODUCTID + " TEXT,"+ PRODUCTNAME + " TEXT," + PRODUCTCATEGORY + " TEXT,"
                + PRODUCTIMAGE + " TEXT,"+ PRODUCTPRICE + " TEXT," + PRODUCTQUANTITY + " TEXT" + ")";

        SQLiteDatabase db = this.getReadableDatabase();

        db.rawQuery(CREATE_ONE_TABLE, null);

        Log.e("sql", CREATE_ONE_TABLE);

    }


    public void deleteCartItem(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String ID = id;
        //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
        // Cursor cursor = db.rawQuery(retrieve, null);

        db.delete("cart", "productID" + "=" + ID, null);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();
    }

    public void clearAllCart() {

        SQLiteDatabase db = this.getReadableDatabase();


        db.execSQL("delete from  cart");

        db.close();
    }

    public void clearCartItem(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String ID = id;
        //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
        // Cursor cursor = db.rawQuery(retrieve, null);

        db.delete("cart", "storeID" + "=" + ID, null);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();
    }

    public void AddToCart(String shopID,String shopName,String productID,String productName,String quaity,String productPrice){


        SQLiteDatabase db = this.getReadableDatabase();

        Log.e("shopID",myID);

        ContentValues cv = new ContentValues();
        cv.put(USERID, myID);
        cv.put(STOREID, shopID);
        cv.put("storeName", shopName);
        cv.put(PRODUCTID, productID);
        cv.put(PRODUCTNAME, productName);

        cv.put(PRODUCTPRICE, productPrice);

        cv.put(PRODUCTQUANTITY, quaity);


        String ID = "ID";

        Log.e("doneAdd","asd");

        db.insert("cart", null, cv);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();


    }


    public void   checkCart(String shopID,String shopName,String productID,String productName,String quaity,String productPrice){


        {

        ContentValues cv = new ContentValues();

            Cursor c;

            SQLiteDatabase db = this.getReadableDatabase();
           // String retrieve = "select * from cart where productID ="+productID+ " and userID=" +myID+"" ;

            String retrieve = "select * from cart where productID ="+productID ;
            c = db.rawQuery(retrieve, null);

            if (c.getCount() != 0) {

                if (c != null) {


                    String previousItemNumber = "";

                    if (c.moveToFirst()) {
                        do {


                            previousItemNumber = (c.getString(c.getColumnIndex(PRODUCTQUANTITY)));


                        } while (c.moveToNext());


                    }



                    int x = Integer.parseInt(previousItemNumber) + Integer.parseInt(quaity);


                    // db.update("cart","ID" + "=" + ID, null);
                    ContentValues cv1 = new ContentValues();
                    cv1.put(PRODUCTQUANTITY, x);

                    //Log.e("update","updated");


                    db.update("cart", cv1, "productID =?", new String[]{productID});

                    db.close();
                    //  Toast.makeText(getActivity(), "Added to cart  ", Toast.LENGTH_LONG).show();



                }



            } else{
                AddToCart(shopID,shopName,productID,productName,quaity,productPrice);

            }

        }


    }

    public ArrayList getList(String token) {
        Cursor c;
        String searchDate = "";



        SQLiteDatabase db = this.getReadableDatabase();
//        String retrieve = "select * from inbox where shopID= "+token+" and myID='" +myID+"'";

        String retrieve = "select * from inbox where shopID= "+token;

        Log.e("asd",retrieve);



        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            if (c != null) {
                if (c.moveToFirst()) {
                    do {


                        String ID = c.getString(c.getColumnIndex("ID"));
                        String myID = c.getString(c.getColumnIndex("myID"));
                        String shopID = c.getString(c.getColumnIndex("shopID"));
                        String shopName = c.getString(c.getColumnIndex("shopName"));
                        String type = c.getString(c.getColumnIndex("type"));
                        String message = c.getString(c.getColumnIndex("message"));

                        String date = c.getString(c.getColumnIndex("date"));

                        String time = c.getString(c.getColumnIndex("time"));

                        String status = c.getString(c.getColumnIndex("status"));

                        messageItem = new MessageItem(ID, myID, shopID, shopName, type, message, date, time, Integer.parseInt(status));


                        messageItemArrayList.add(messageItem);

                    } while (c.moveToNext());

                }

            }

        }

        db.close();


        return messageItemArrayList;

    }

    public ArrayList getCartList(String shopID) {
        Cursor c;



        ArrayList<CartListModel> cartListModelArrayList = new ArrayList<>();



        SQLiteDatabase db = this.getReadableDatabase();
//        String retrieve = "select * from cart where storeID= "+shopID+" and userID=" +myID+"";

        String retrieve = "select * from cart where storeID = "+shopID;

        Log.e("asd",retrieve);

        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            if (c != null) {
                if (c.moveToFirst()) {
                    do {




                        String shopName = c.getString(c.getColumnIndex("storeName"));
                        String productID = c.getString(c.getColumnIndex(PRODUCTID));
                        String productName   = c.getString(c.getColumnIndex(PRODUCTNAME));
                        String productImage   = c.getString(c.getColumnIndex(PRODUCTIMAGE));
                        String productPrice = c.getString(c.getColumnIndex(PRODUCTPRICE));

                        String quaity = c.getString(c.getColumnIndex(PRODUCTQUANTITY));



                        CartListModel cartListModel = new CartListModel(shopID,shopName,productID,productName,productPrice,productImage,quaity);

                        cartListModelArrayList.add(cartListModel);

                    } while (c.moveToNext());

                }

            }

        }

        db.close();




        return cartListModelArrayList;

    }


    public float getCartTotal(String shopID) {
        Cursor c;



        float total=0;



        SQLiteDatabase db = this.getReadableDatabase();
//        String retrieve = "select * from cart where storeID= "+shopID+" and userID=" +myID+"";

        String retrieve = "select * from cart where storeID = "+shopID;

        Log.e("asd",retrieve);

        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            if (c != null) {
                if (c.moveToFirst()) {
                    do {


                        String productPrice = c.getString(c.getColumnIndex(PRODUCTPRICE));

                        String quaity = c.getString(c.getColumnIndex(PRODUCTQUANTITY));

                        total = total + (Float.parseFloat(productPrice)*Float.parseFloat(quaity));



                    } while (c.moveToNext());

                }

            }

        }

        db.close();




        return total;

    }


    public int getCartCounter(String shopID) {
        Cursor c;
        String searchDate = "";



        ArrayList<CartListModel> cartListModelArrayList = new ArrayList<>();



        SQLiteDatabase db = this.getReadableDatabase();
//        String retrieve = "select * from cart where storeID= "+shopID+" and userID='" +myID+"'";

        String retrieve = "select * from cart where storeID= "+shopID;



        c = db.rawQuery(retrieve, null);

        int count = c.getCount();

        db.close();

        Log.e("c", retrieve + "");
        Log.e("c",count+"");


        return count;

    }

    public void saveMessageIncoming(String fromID, String shopID, String shopName, String type, String message, String status,String smsDate, String smsTime) {
        SQLiteDatabase db = this.getReadableDatabase();
        calendar = Calendar.getInstance();

        Date date = calendar.getTime();

        String dateForDb = formatter.format(date);

        String timeForDb = timeFormat.format(date);


        ContentValues cv = new ContentValues();
        cv.put("myID", fromID);
        cv.put("shopID", shopID);
        cv.put("shopName", shopName);

        cv.put("type", type);
        cv.put("message", message);

        cv.put("date", dateForDb);
        cv.put("time", timeForDb);
        cv.put("status", status);


        String ID = "ID";
        //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
        // Cursor cursor = db.rawQuery(retrieve, null);

        db.insert("inbox", null, cv);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();

    }

//    public ArrayList getMessageList() {
//        Cursor c;
//        String searchDate = "";
//
//
//        ArrayList checkList = new ArrayList();
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        String retrieve = "select * from inbox where myID='" +myID+"'";
//
//        Log.e("check", retrieve);
//
//
//        c = db.rawQuery(retrieve, null);
//        if (c.getCount() != 0) {
//
//            if (c != null) {
//                if (c.moveToFirst()) {
//                    do {
//
//
//                        String ID = c.getString(c.getColumnIndex("ID"));
//
//                        String shopID = c.getString(c.getColumnIndex("shopID"));
//                        String shopName = c.getString(c.getColumnIndex("shopName"));
//
//
//
//                      if(!checkList.contains(shopID)){
//                          messageListItem = new MessageListItem(ID, shopID, shopName);
//
//                          messageListItemArrayList.add(messageListItem);
//
//                          checkList.add(shopID);
//                      }
//
//
//                    } while (c.moveToNext());
//
//                }
//
//            }
//
//        }
//
//        db.close();
//
//
//        return messageListItemArrayList;
//
//    }

    public ArrayList getMessageList() {
        Cursor c;
        String searchDate = "";


        ArrayList checkList = new ArrayList();

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from conversation";

        Log.e("check", retrieve);

        ArrayList<NewMessageListItem> newMessageListItems = new ArrayList<>();

        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            if (c != null) {
                if (c.moveToFirst()) {
                    do {


                        String userID = c.getString(c.getColumnIndex("userID"));

                        String conID = c.getString(c.getColumnIndex("conID"));
                        String userName = c.getString(c.getColumnIndex("userName"));


                          NewMessageListItem newMessageListItem = new NewMessageListItem(userID, userName,conID);

                        newMessageListItems.add(newMessageListItem);


                    } while (c.moveToNext());

                }

            }

        }

        db.close();


        return newMessageListItems;

    }

    public void saveStore(String storeID, String storeName, String ownerName, String storeAddress, String storePhone) {
        SQLiteDatabase db = this.getReadableDatabase();


        String retrieve = "select * from stores where storeID= "+storeID;

        Log.e("check", retrieve);

        Cursor c;
        c = db.rawQuery(retrieve, null);
        if (c.getCount()== 0) {

            ContentValues cv = new ContentValues();
            cv.put("storeID", storeID);
            cv.put("storeName", storeName);
            cv.put("owerName", ownerName);

            cv.put("storeAddress", storeAddress);

            cv.put("storePhone", storePhone);

            String ID = "ID";
            //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
            // Cursor cursor = db.rawQuery(retrieve, null);

            db.insert("stores", null, cv);

            }





        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();

    }

    public ArrayList getStoreInfo(String token) {
        Cursor c;
        String searchDate = "";


        ArrayList infoList = new ArrayList();

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from stores where storeID= "+token;




        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            if (c != null) {
                if (c.moveToFirst()) {
                    do {



                        String storeName = c.getString(c.getColumnIndex("storeName"));
                        String ownerName = c.getString(c.getColumnIndex("owerName"));
                        String storeAddress = c.getString(c.getColumnIndex("storeAddress"));
                        String phone = c.getString(c.getColumnIndex("storePhone"));

                        infoList.add(storeName);
                        infoList.add(storeAddress);
                        infoList.add(ownerName);
                        infoList.add(phone);

                    } while (c.moveToNext());

                }

            }

        }

        db.close();

//        Log.e("check",infoList.get(0).toString() + " " +infoList.get(1).toString() + " ");


        return infoList;

    }

    public void updateRead(String shopID) {
        SQLiteDatabase db = this.getReadableDatabase();

        String ID = shopID;

        // db.update("cart","ID" + "=" + ID, null);
        ContentValues cv = new ContentValues();

        cv.put("status", "1");



        Log.e("update","updated");


        db.update("inbox", cv, "shopID  =?", new String[]{shopID});

        db.close();
    }


}