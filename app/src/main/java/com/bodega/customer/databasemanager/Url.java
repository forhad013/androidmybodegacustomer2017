package com.bodega.customer.databasemanager;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class Url {




    public static final String UPDATERENT ="update_rent";

    public static final String SERVER_DOMAIN = "http://www.mybodega.online/apis/";
    public static final String REGISTRATION =SERVER_DOMAIN+"v2_registration.php";
    public static final String UPDATE =SERVER_DOMAIN+"v2_update_client2.php";
    public static final String LOGIN =SERVER_DOMAIN+"v2_login.php";
    public static final String ORDER =SERVER_DOMAIN+"V2_customer-post-order.php";
    public static final String EDIT =SERVER_DOMAIN+"edit-user.php";
    public static final String CHANGEPASSWORD =SERVER_DOMAIN+"v2_changePassword.php";
//    public static final String NEARSETSHOP =SERVER_DOMAIN+"v2_nearest-shops.php";
public static final String NEARSETSHOP =SERVER_DOMAIN+"nearestshops.php";
    public static final String SHOPDETAILS =SERVER_DOMAIN+"v2_shop-details.php";

    public static final String SEND_MESSAGE =SERVER_DOMAIN+"v2_sending-sms.php";

    public static final String PENDINGMESSAGE =SERVER_DOMAIN+"v2_pending-sms-request.php";

    public static final String CALL =SERVER_DOMAIN+"call-record.php";

    public static final String FORGET =SERVER_DOMAIN+"v2_forget-password-request.php";

    public static final String SOCKETCHATIP ="http://192.169.227.95:3000";
    public static final String SOCKETAPI ="http://192.169.227.95:3000/api/chat";

    public static final String MYORDERLIST =SERVER_DOMAIN+"customerorders.php";

    public static final String MYLASTORDERLIST =SERVER_DOMAIN+"customerlastorder.php";

    public static final String ODRERDETAILS =SERVER_DOMAIN+"customerorderdetails.php";


    public static final String CATEGORY =SERVER_DOMAIN+"catlist.php";
    public static final String CATEGORY_ADD =SERVER_DOMAIN+"catadd.php";
    public static final String CATEGORY_EDIT =SERVER_DOMAIN+"catedit.php";
    public static final String CATEGORY_DELETE =SERVER_DOMAIN+"catdelete.php";


    public static final String PRODUCT_SEARCH =SERVER_DOMAIN+"productsearch.php";

    public static final String PRODUCT_LIST =SERVER_DOMAIN+"productlist.php";
    public static final String PRODUCT_ADD =SERVER_DOMAIN+"productadd.php";
    public static final String PRODUCT_EDIT =SERVER_DOMAIN+"productedit.php";
    public static final String PRODUCT_DELETE =SERVER_DOMAIN+"productdelete.php";

}
