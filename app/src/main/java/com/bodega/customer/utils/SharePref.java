package com.bodega.customer.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Smartnjazzy on 8/30/2015.
 */
public class SharePref {

    public static final String PREF = "pref";
    static public String GCM_REGCODE = "gcm_regCode";
    static public String SOCKET_TOKEN="socket_token";

    static public String USERID = "userID";
    static public String USERNAME = "userName";

    static public String USERADDRESS = "userAddress";
    static public String LATITUDE = "latitude";
    static public String LONGITUDE = "longitude";
    static public String LOGEDIN = "login";
    static public String LOGEDIN_METHOD = "login_method";
    public static final String IS_LOGGED_IN = "is_logged_in";

    static public String USERPHONE = "userNumber";

    static public String SELECTED_ID = "selected_id";


    static public String OLDUSER = "oldUser";

    static public String FIRSTNAME = "firstName";
    static public String LASTNAME = "lastName";
    static public String USERPASSWWORD = "userPassword";
    static public String USEREMAIL = "userEmail";
    static public String FIRSTTIME = "firstTime";
    static public String CURRENTSHOPID = "shopId";
    static public String CURRENTSHOPNAME = "shopName";
    static public String ORDERDETAILS = "order";
    static public String TIPS = "tips";
    static public String CREDITCARDFEE = "cardfee";
    static public String PAYMENTTYPE = "paymentType";

    static public String ORDERDELIVERYVALUE = "deliveryValue";
    static public String TOTALORDERPRICEVALUE = "totaldeliveryValue";
    static public String SPECIALORDER = "specialOrder";
    static public String ORDERTYPE = "orderType";
    static public String TOTALPRODUCTPRICE= "totalProductPrice";
    static public String CURRENTSHOPNFEES= "shopFees";
    static public String STRIPE_ID= "stripeId";
    static public String BODEGA_FEE= "bodegaFee";
    SharedPreferences settings;


    public SharePref(Context mContext) {
        settings = mContext.getSharedPreferences(PREF, Context.MODE_PRIVATE);
    }


    public boolean setIsLoggedIn(boolean value) {
        settings.edit().putBoolean(IS_LOGGED_IN, value).commit();
        return value;
    }

    public boolean isLoggedIn() {
        return settings.getBoolean(IS_LOGGED_IN, false);
    }
    public int getshareprefdata(String KEY) {
        return settings.getInt(KEY, 160);
    }

    public void setshareprefdata(String KEY, int value) {

        settings.edit().putInt(KEY, value).commit();
    }


    public String getshareprefdatastring(String KEY) {
        return settings.getString(KEY, "");
    }

    public void setshareprefdatastring(String KEY, String values) {

        settings.edit().putString(KEY, values).commit();
    }

    public Boolean getshareprefdataBoolean(String KEY) {
        return settings.getBoolean(KEY, false);
    }

    public void setshareprefdataBoolean(String KEY, Boolean values) {

        settings.edit().putBoolean(KEY, values).commit();
    }
}
