package com.bodega.customer.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Mac on 10/5/16.
 */
public class ClusterMarkerLocation implements ClusterItem {
    public  String shopName;
    public  String shopInfo;
    private LatLng position;

    public ClusterMarkerLocation( LatLng latLng,String shopName, String shopInfo ) {
        this.shopName = shopName;
        this.shopInfo = shopInfo;
        position = latLng;
    }
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopInfo() {
        return shopInfo;
    }

    public void setShopInfo(String shopInfo) {
        this.shopInfo = shopInfo;
    }

    @Override
    public LatLng getPosition() {
        return position;
    }


    public void setPosition( LatLng position ) {
        this.position = position;
    }
}
