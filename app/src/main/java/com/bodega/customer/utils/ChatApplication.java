package com.bodega.customer.utils;

import com.bodega.customer.databasemanager.Url;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;


public class ChatApplication  {

    private Socket mSocket;
    {
        try {
//            IO.Options options = new IO.Options();
//            options.port = 3000;
            mSocket = IO.socket(Url.SOCKETCHATIP);


        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }



}
