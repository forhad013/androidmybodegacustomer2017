package com.bodega.customer.utils;

import android.content.Context;

import com.bodega.customer.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

/**
 * Created by Mac on 10/5/16.
 */
public class ShopRender extends DefaultClusterRenderer<ClusterMarkerLocation> {

    public ShopRender(Context context, GoogleMap map, ClusterManager<ClusterMarkerLocation> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(ClusterMarkerLocation item, MarkerOptions markerOptions) {
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_shop));
        markerOptions.snippet(item.getShopName());
        markerOptions.title(item.getShopInfo());
        super.onBeforeClusterItemRendered(item, markerOptions);
    }
}
