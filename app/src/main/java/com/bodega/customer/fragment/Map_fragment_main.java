package com.bodega.customer.fragment;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.model.ShopListModel;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.halcyon.squareprogressbar.SquareProgressBar;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class Map_fragment_main extends Fragment implements ConnectionCallbacks,
		OnConnectionFailedListener, LocationListener {

	// LogCat tag
	private static final String TAG = "MAP";

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

	private Location mLastLocation;

	// Google client to interact with Google API
	private GoogleApiClient mGoogleApiClient;

	// boolean flag to toggle periodic location updates
	private boolean mRequestingLocationUpdates = false;

	private LocationRequest mLocationRequest;

	// Location updates intervals in sec
	private static int UPDATE_INTERVAL = 10000; // 10 sec
	private static int FATEST_INTERVAL = 5000; // 5 sec
	private static int DISPLACEMENT = 10; // 10 meters

	// UI elements
	private GoogleMap mMap;
	SupportMapFragment mapFragment;
	LocationManager locationManager;

	String provider;
	int count = 0;
	ImageButton profile;
	TextView topTxt;
	Marker m1;

	int success = 0;

	ArrayList shopIDList;

	HashMap<String ,Integer> mapStatus;
	HashMap<String ,Integer>  hashTemp;

	RelativeLayout tutorial;
	Location location;

	JSONParser jsonParser;

	ArrayList<Marker> markers;

	JSONObject json;

	String msg, message;

	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;
	Thread thread;

	String shopContact, shopID;

	Typeface mytTypeface, customFont;

	SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

	public ArrayList<ShopListModel> shopList;
	ShopListModel shopListModel;

	Map<String, String> shopMap = new HashMap<String, String>();



	TextView titleText;

	double lat, lon;

	String firstTime;
	LatLng firstPoint = null;
	LatLng secondPoint = null;

	float distanceInMeter;

	LatLng currentLoc;
	TextView distanceInfo;
	ArrayList<LatLng> trackList;
	double tempLat = 40.733979, tempLon = -73.988894;

	String loginStatus;

	Button gotIt;
	SharePref sharePref;
	SquareProgressBar squareProgressBar;
	boolean isInternetOn =false;
	CountDownTimer coundown;
	ConnectionDetector cd;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_maps, container, false);

		mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

		mMap = mapFragment.getMap();

		shopIDList = new ArrayList();

		mapStatus = new HashMap<>();
		 squareProgressBar = (SquareProgressBar) rootView.findViewById(R.id.sprogressbar);

		sharePref = new SharePref(getActivity());

		progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
		progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
		progressSweetAlertDialog.setTitleText("Loading");
		progressSweetAlertDialog.setCancelable(false);


		//firstTime = sharePref.getshareprefdatastring(SharePref.FIRSTTIME);

		markers = new ArrayList<>();
		shopList = new ArrayList<>();

		jsonParser = new JSONParser();
		loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);

		squareProgressBar.setImage(R.drawable.ic_progress);
		squareProgressBar.setProgress(50.0);

		// profile.setVisibility(View.INVISIBLE);
		normalDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);




		cd= new ConnectionDetector(getActivity());

		isInternetOn = cd.isConnectingToInternet();

		if(isInternetOn||checkPlayServices()) {

			buildGoogleApiClient();

			createLocationRequest();
		}else{
			Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

		}

		mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker marker) {
				String shopname = marker.getTitle();

				String info = marker.getSnippet();
				String shopID1 = shopMap.get(shopname);



				if(mapStatus.get(shopID1)==0){

					marker.showInfoWindow();
					setOtherMarker(shopID1);
					mapStatus.put(shopID1,1);

				}else{
					if (!shopname.equals("You are here")) {
						marker.hideInfoWindow();
						String shopName = marker.getTitle();
						String shopID = shopMap.get(shopName);
						sharePref.setshareprefdatastring(SharePref.CURRENTSHOPID,shopID);
//						FragmentManager fm = getActivity().getSupportFragmentManager();
//						FragmentTransaction ft = fm.beginTransaction();
//						Fragment_ShopDetails llf = new Fragment_ShopDetails();
//
//						//((DrawerActivity) getActivity()).hideTextButton(false);
////
//						Bundle bundle = new Bundle();
//						bundle.putString("shopId", shopID);
//						mapStatus.put(shopID, 0);
//						llf.setArguments(bundle);
//
//					//	((DrawerActivity) getActivity()).fragmentStack.lastElement().onStop();
//						((DrawerActivity) getActivity()).fragmentStack.push(llf);
//						recheckMapHASH();
//						ft.add(R.id.frame_container, llf);
//						ft.commit();
						((DrawerActivity) getActivity()).displayView(6);

					}
				}

				return false;
			}
		});



		//  LOOP(location);
//
		mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
			@Override
			public void onInfoWindowClick(Marker marker) {


				String shopname = marker.getTitle();

				String info = marker.getSnippet();


				if (!shopname.equals("You are here")) {

					String shopName = marker.getTitle();
					String shopID = shopMap.get(shopName);
					sharePref.setshareprefdatastring(SharePref.CURRENTSHOPID,shopID);
//					FragmentManager fm = ((DrawerActivity) getActivity()).getSupportFragmentManager();
//					FragmentTransaction ft = fm.beginTransaction();
//					Fragment_ShopDetails llf = new Fragment_ShopDetails();
//					recheckMapHASH();
//						((DrawerActivity) getActivity()).hideTextButton(false);
//					marker.hideInfoWindow();
//					Bundle bundle = new Bundle();
//					bundle.putString("shopId", shopID);
//					mapStatus.put(shopID,0);
//					llf.setArguments(bundle);
//					//((DrawerActivity) getActivity()).fragmentStack.lastElement().onStop();
//					((DrawerActivity) getActivity()). fragmentStack.push(llf);
//					marker.hideInfoWindow();
//
//					ft.add(R.id.frame_container, llf);
//					ft.commit();
					((DrawerActivity) getActivity()).displayView(6);

				}
			}
		});

		return  rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		if (mGoogleApiClient != null) {
			if(isInternetOn)
			mGoogleApiClient.connect();
		}
	}

	@Override
	public void onResume() {
		((DrawerActivity) getActivity()). hideTextButton(true);
		((DrawerActivity) getActivity()). hideCartButton(true,0);
		super.onResume();
		((DrawerActivity) getActivity()).showTitle();
//		checkPlayServices();
//
//		// Resuming the periodic location updates
//		if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
//			startLocationUpdates();
//		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		//stopLocationUpdates();
	}

	/**
	 * Method to display the location on UI
	 * */
	private void displayLocation() {

		mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mGoogleApiClient);

		if (mLastLocation != null) {
			tempLat = mLastLocation.getLatitude();
			tempLon = mLastLocation.getLongitude();


			setUpMap();

		//	Toast.makeText(getActivity(),latitude + ", " + longitude ,Toast.LENGTH_SHORT);

		} else {

//			if(!checkLocationPermission()) {
//
//				ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//			}
			setLocation(tempLat, tempLon);

			new AsyncTaskEdit().execute();
		}
	}


	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case 1: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {

					buildGoogleApiClient();

					createLocationRequest();

				} else {
					setLocation(tempLat, tempLon);

					new AsyncTaskEdit().execute();
				}
				return;
			}
			// other 'case' lines to check for other
			// permissions this app might request
		}
	}
	public boolean checkLocationPermission()
	{
		String permission = "android.permission.ACCESS_FINE_LOCATION";
		int res = getActivity().checkCallingOrSelfPermission(permission);
		return (res == PackageManager.PERMISSION_GRANTED);
	}
	/**
	 * Method to toggle periodic location updates
	 * */


	/**
	 * Creating google api client object
	 * */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}

	/**
	 * Creating location request object
	 * */
	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
	}

	/**
	 * Method to verify google play services on the device
	 * */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getActivity());
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Toast.makeText(getActivity(),
						"This device is not supported.", Toast.LENGTH_LONG)
						.show();


			}
			return false;
		}
		return true;
	}

	/**
	 * Starting the location updates
	 * */
	protected void startLocationUpdates() {

		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);

	}

	/**
	 * Stopping location updates
	 */
	protected void stopLocationUpdates() {
		LocationServices.FusedLocationApi.removeLocationUpdates(
				mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
	}

	/**
	 * Google api callback methods
	 */
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
				+ result.getErrorCode());
	}



	public void setProgress(){

		final int[] progressAmount = {0};
		squareProgressBar.setProgress((double) progressAmount[0]);

		coundown = new CountDownTimer(30000, 100) {

					     public void onTick(long millisUntilFinished) {

							 progressAmount[0]=progressAmount[0]+10;

							 squareProgressBar.setProgress((double) progressAmount[0]);

							 if(progressAmount[0]==100){
								 progressAmount[0]=0;
							 }
				     }

				      public void onFinish() {

				    }
			  }.start();
	}

	public void stopProgressbar(){


	}




	@Override
	public void onConnected(Bundle arg0) {


		//	Log.e("onCon",Map_fragment_main.this.isVisible()+"");


		if(Map_fragment_main.this.isVisible()){
			displayLocation();
		}

//

//
//		 if (mRequestingLocationUpdates) {
//		  	startLocationUpdates();
//		 }
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onLocationChanged(Location location) {
		// Assign the new location
		mLastLocation = location;


		//displayLocation();
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onProviderDisabled(String provider) {

	}


	public void recheckMapHASH(){

		mapStatus = hashTemp;

	}

	public void setOtherMarker(String shopID){

		for(int i=0;i<mapStatus.size();i++){

			String temp = shopIDList.get(i).toString();
			if(!temp.equals(shopID)){
				mapStatus.put(temp,0);
			}
		}

	}
	public void errorDialog(){

		new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
				.setTitleText("Oops...")
				.setContentText("Location can't be retrieved, check your gps")
				.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sweetAlertDialog) {

//                                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
//
//
//                                startActivity(intent);
						getActivity().finish();

						sweetAlertDialog.dismiss();
					}
				}).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
			@Override
			public void onClick(SweetAlertDialog sweetAlertDialog) {



				sweetAlertDialog.dismiss();

			}
		}).setCancelText("Cancel").setConfirmText("Restart this page")
				.show();
	}

	public void setUpMap() {


		//progressSweetAlertDialog.show();
		squareProgressBar.setVisibility(View.VISIBLE);

		mMap.setMyLocationEnabled(true);

		MarkerOptions markerOptions = new MarkerOptions();

		mMap.getUiSettings().setMapToolbarEnabled(false);



		// markerOptions.title("Ahmedabad Cordinat Found here");

		// Marker m = mMap.addMarker(markerOptions);

		mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
			@Override
			public View getInfoWindow(Marker marker) {
				View myContentView = getActivity().getLayoutInflater().inflate(
						R.layout.custom_marker, null);
				TextView tvTitle = ((TextView) myContentView
						.findViewById(R.id.snippet));
				tvTitle.setText(marker.getTitle());
				TextView tvSnippet = ((TextView) myContentView
						.findViewById(R.id.title));
				tvSnippet.setText(marker.getSnippet());
				return myContentView;
			}

			@Override
			public View getInfoContents(Marker marker) {

				return null;
			}
		});

		setLocation(tempLat, tempLon);

		new AsyncTaskEdit().execute();

	}




	public void setLocation(double lat, double lon) {

		LatLng loc = new LatLng(lat, lon);


		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 16.0f));

	}




	class AsyncTaskEdit extends AsyncTask<String, String, String> {


		@Override
		protected void onPostExecute(String result) {
			if (success == 1) {


				try {


					setShopLocations();

					//progressSweetAlertDialog.dismiss();

					squareProgressBar.setVisibility(View.INVISIBLE);
					stopProgressbar();

				} catch (Exception e) {

				}

			} else {
				doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
				doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
				doneDialog.setContentText(message);
				doneDialog.show();
				stopProgressbar();
			}

			stopProgressbar();
			//progressSweetAlertDialog.dismiss();
			squareProgressBar.setVisibility(View.INVISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> pair = new ArrayList<NameValuePair>();


			pair.add(new BasicNameValuePair("myLatitude", tempLat + ""));

			pair.add(new BasicNameValuePair("myLongitude", tempLon + ""));


			Log.e("url", Url.NEARSETSHOP);

			json = null;

			json = jsonParser.makeHttpRequest(Url.NEARSETSHOP, "POST", pair);

			Log.e("edit", json + "");
			Log.e("edit", pair + "");

			try {

				success = json.getInt("success");


				message = json.getString("message");


				JSONObject	results = json.getJSONObject("results");
				if (success == 1) {


					JSONArray jsonArray = results.getJSONArray("shopList");


					for (int i = 0; i < jsonArray.length(); i++) {


						JSONObject object = jsonArray.getJSONObject(i);

						String shopID = object.getString("storeId");
						String shopName = object.getString("storeName");

						String shopLocation = object.getString("storeStreet");
						String shopLatitude = object.getString("storeLatitude");
						String shopLongitude = object.getString("storeLongitude");
						if (!shopLatitude.equals("") || !shopLongitude.equals("")) {

							//Log.e("asoda", Double.parseDouble(shopLatitude) + "");
							double tempLat = 0, tempLon = 0;
							try {
								tempLat = Double.parseDouble(shopLatitude);


								tempLon = Double.parseDouble(shopLongitude);
							} catch (Exception e) {

							}

							shopListModel = new ShopListModel(shopID, shopName, shopContact, shopLocation, tempLat, tempLon);


							mapStatus.put(shopID,0);
							shopList.add(shopListModel);
						}
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPreExecute() {

			try {

				setProgress();

				//Log.e("asd", "asdasd");
				// progressSweetAlertDialog.show();

				//   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
			} catch (IndexOutOfBoundsException e) {
				Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT).show();
			}

		}

	}


	public void setShopLocations() {


		for (int i = 0; i < shopList.size(); i++) {


			String temp = "m" + i + 1;

			Marker marker = null;

			String shopID = shopList.get(i).getShopID();

			String shopName = shopList.get(i).getShopName();

			String shopAddress = shopList.get(i).getShopLocation();

			double shopLat = shopList.get(i).getShopLatitude();

			double shopLon = shopList.get(i).getShopLongitude();


			shopIDList.add(shopID);
			shopMap.put(shopName,shopID);



			LatLng loc = new LatLng(shopLat, shopLon);

			//Log.e("loc",loc+"");

			if(shopLat!=0 || shopLon!=0) {

				//  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 16.0f));
				marker = mMap.addMarker(new MarkerOptions().position(new LatLng(shopLat, shopLon)).title(shopName).snippet(shopAddress));
				marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_shop));
				marker.showInfoWindow();

				markers.add(marker);
			}
		}

		hashTemp = mapStatus;


	}



}
