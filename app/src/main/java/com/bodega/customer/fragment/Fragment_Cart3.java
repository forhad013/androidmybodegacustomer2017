package com.bodega.customer.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.activity.StripePayment;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.model.CartListModel;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;



public class Fragment_Cart3 extends Fragment implements  GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String PUBLISHABLE_KEY = "pk_live_XXX";

    // Unique identifiers for asynchronous requests:
    private static final int LOAD_MASKED_WALLET_REQUEST_CODE = 1000;
    private static final int LOAD_FULL_WALLET_REQUEST_CODE = 1001;

  //  private SupportWalletFragment walletFragment;

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AX4EwFPyJOcB0ZbT91oC8qK610pEMT9s9w_3-78d9EqEHZAqgSisC6_CMKml2_hv5Hqi4w0sy_RUZdW1";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);
    JSONParser jsonParser;
   // Typeface myTypeface, custom;

    Button stripePayment;


    private static final String LOG_TAG = "Google Places Autocomplete";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";

    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";

    private static final String OUT_JSON = "/json";


    private static final String API_KEY =   "AIzaSyB_wVRhZ1qu9IVJO3JyS_fIPIfOkoLSjUA";


    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    int success = 0;

    JSONObject json;


    RadioButton cashRadio,stripeRadio,paypalRadio;


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    String loginStatus;

    SharePref sharePref;

    String cardNumberString,cvcString,expireString,PaymentToken;
    String [] addressContent;
    float totalProdcutPrice=0;

    LinearLayout delLin0,delLin1,delLin2,delLin3,delLin4;

    TextView d2OptionValueTxt,d3OptionValueTxt,d4OptionValueTxt;

    String message;

    AutoCompleteTextView addressAutoComplete;

    boolean isInternetOn;

   // public static final int mEnvironment = WalletConstants.ENVIRONMENT_TEST;


    ConnectionDetector cd;

    MyDatabase db;

    CircleImageView image;

    String gcmCode;

    ListView list;

    Fragment waletFragment;

    String paymentid, paymenttoken, payerid, paymenttype, paymentmethod, paymentbrand, paymentstatus;

    private GoogleApiClient googleApiClient;

    TextView productPriceTotaltext;

    EditText firstNameEdt,emailEdt,contactEdt,addressEdt,noteEdt;
   // postalEdt,cityEdt,
    ArrayList<CartListModel> cartListModelsArrayList;

    String shopID,productIdString,productNameString,productPriceString,productImageString;

    String userID, firstname, lastname, telephone, email, address="", tipsString, postalcode, notes, deliverycost,specialCost,orderDetails;

    String totalOrderPrice;
    Fragment fragment;

    Button submit;
    String totalOrderAmount,creditCardFeeAmount;
    Button submitStripe;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_check_out3, container,
                false);

        submit = (Button) v.findViewById(R.id.submit);

        firstNameEdt= (EditText) v.findViewById(R.id.firstName);
      //  lastNameEdt= (EditText) v.findViewById(R.id.lastName);
        emailEdt =  (EditText) v.findViewById(R.id.email);
        contactEdt= (EditText) v.findViewById(R.id.checkoutPhoneNumber);
        addressAutoComplete= (AutoCompleteTextView) v.findViewById(R.id.checkoutAddress);
//        postalEdt= (EditText) v.findViewById(R.id.postalCode);
//        cityEdt= (EditText) v.findViewById(R.id.city);
        noteEdt= (EditText) v.findViewById(R.id.checkoutInstruction);

       // submitStripe= (Button) v.findViewById(R.id.submitStripe);
        cashRadio = (RadioButton) v.findViewById(R.id.cash);

        stripeRadio = (RadioButton) v.findViewById(R.id.stripe);

        paypalRadio = (RadioButton) v.findViewById(R.id.paypal);

        stripePayment = (Button) v.findViewById(R.id.submitStripe);

        jsonParser = new JSONParser();
        sharePref = new SharePref(getActivity());
        loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
        userID = sharePref.getshareprefdatastring(SharePref.USERID);
        firstNameEdt.setText(sharePref.getshareprefdatastring(SharePref.FIRSTNAME)+" "+sharePref.getshareprefdatastring(SharePref.LASTNAME));

        emailEdt.setText(sharePref.getshareprefdatastring(SharePref.USEREMAIL));
        contactEdt.setText(sharePref.getshareprefdatastring(SharePref.USERPHONE));
        tipsString = sharePref.getshareprefdatastring(SharePref.TIPS);
        paymenttype = sharePref.getshareprefdatastring(SharePref.PAYMENTTYPE);


        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
//
//        myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRnd-Book.otf");
//        custom = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        totalOrderAmount = sharePref.getshareprefdatastring(SharePref.TOTALORDERPRICEVALUE);

        Log.e("totalOrderAmount",totalOrderAmount);
        creditCardFeeAmount =sharePref.getshareprefdatastring(SharePref.CREDITCARDFEE);
        Log.e("creditCardFeeAmount",creditCardFeeAmount);
        orderDetails=sharePref.getshareprefdatastring(SharePref.ORDERDETAILS);

        Log.e("order",orderDetails);
        firstname = firstNameEdt.getText().toString();
       // lastname =  lastNameEdt.getText().toString();

        telephone =  contactEdt.getText().toString();
        email =  emailEdt.getText().toString();
      //   address =  addressEdt.getText().toString();
       // city =  cityEdt.getText().toString();
       // postalcode =  postalEdt.getText().toString();
        notes =  noteEdt.getText().toString();
        deliverycost =  sharePref.getshareprefdatastring(SharePref.ORDERDELIVERYVALUE);
        specialCost = sharePref.getshareprefdatastring(SharePref.SPECIALORDER);

        gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);


        shopID = sharePref.getshareprefdatastring(SharePref.CURRENTSHOPID);


        Log.e("shopiD",shopID);

        fragment = new Fragment_category();


       // backBtn = (ImageButton) v.findViewById(R.id.backBtn);

        db = new MyDatabase(getActivity());

        cd = new ConnectionDetector(getActivity());

        isInternetOn = cd.isConnectingToInternet();


        addressAutoComplete.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.address_list_item));
//
        addressAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                address = (String) parent.getItemAtPosition(position);

                //  checkLocation();



                if(!checkLocation()){
                    address= "";

                    //   Toast.makeText(getApplicationContext(), "MyBodega is not open for service outside of  New Jersey and New York. Come back later as we expand to new areas.", Toast.LENGTH_SHORT).show();

                    String msg ="MyBodega is not open for service outside of  New Jersey and New York. Come back later as we expand to new areas.";
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText(msg)
                            .show();
                }

                // Toast.makeText(getApplicationContext(), addressString, Toast.LENGTH_SHORT).show();

            }
        });


        productPriceTotaltext = (TextView) v.findViewById(R.id.totalProdcutPrice);

        delLin0 = (LinearLayout) v.findViewById(R.id.delLin0);

        delLin1 = (LinearLayout) v.findViewById(R.id.delLin1);
        delLin2 = (LinearLayout) v.findViewById(R.id.delLin2);
        delLin3 = (LinearLayout) v.findViewById(R.id.delLin3);
        delLin4 = (LinearLayout) v.findViewById(R.id.delLin4);

        d2OptionValueTxt = (TextView) v.findViewById(R.id.dOptionValue2);
        d3OptionValueTxt= (TextView) v.findViewById(R.id.dOptionValue3);
        d4OptionValueTxt= (TextView) v.findViewById(R.id.dOptionValue4);




        stripePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getActivity(), StripePayment.class);
                startActivity(in);
            }
        });

//        submitStripe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//
//
//            }
//        });




        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                firstname = firstNameEdt.getText().toString();
              //  lastname =  lastNameEdt.getText().toString();

                telephone =  contactEdt.getText().toString();
                email =  emailEdt.getText().toString();
               // address =  addressEdt.getText().toString();
              //  city =  cityEdt.getText().toString();
              //  postalcode =  postalEdt.getText().toString();
                notes =  noteEdt.getText().toString();
                if(nullCheck()) {

                    if (isInternetOn) {

                     //   new AsyncTaskOrderSubmit().execute();

                        if(paymenttype.equals("cash")){
                            paymentmethod= "cash";
                            paymentbrand = "cash";
                            paymenttype = "cash";
                            new AsyncTaskOrderSubmit().execute();
                        } else if(paymenttype.equals("credit")){
                            paymentmethod= "stripe";
                            paymenttype = "credit";
                            PaymentDialoge();
                        }


                    }
                }
            }
        });



        cashRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    paypalRadio.setChecked(false);
                    stripeRadio.setChecked(false);
                }
            }
        });


        paypalRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    cashRadio.setChecked(false);
                    stripeRadio.setChecked(false);
                }
            }
        });


        stripeRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    paypalRadio.setChecked(false);
                    cashRadio.setChecked(false);
                    stripeRadio.setChecked(true);
                }
            }
        });




        return v;
    }


    public boolean checkLocation(){

        boolean status=false;

        addressContent = address.split("\\, ");

        if(addressContent.length!=0){

            for(int i=0;i<addressContent.length;i++) {
                String temp = addressContent[i];

                Log.e("temp",temp);
                if(temp.equals("NY")||temp.contains("NJ")||temp.contains("new york")||temp.contains("new jersy")||temp.contains("New York")||temp.contains("New Jersy")){

                    status = true;
                    break;
                }
            }
        }

        return status;
    }

    public void setCart(){

        cartListModelsArrayList = new ArrayList<>();

        cartListModelsArrayList = db.getCartList(shopID);



    }




    @Override
    public void onResume() {

        ((DrawerActivity)getActivity()).hideTitle();
        ((DrawerActivity)getActivity()).hideTextButton(true);
        super.onResume();
    }

    public void onBuyPressed() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(totalOrderAmount), "USD", "sample item",
                paymentIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.e("Show", confirm.toJSONObject().toString(4));
                        Log.e("Show", confirm.getPayment().toJSONObject().toString(4));

                       JSONObject jsonObject = confirm.toJSONObject();

                        PaymentToken = jsonObject.getJSONObject("response").getString("id");

                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         */
                        Toast.makeText(getActivity(), "PaymentConfirmation info received" +
                                " from PayPal", Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), "an extremely unlikely failure" +
                                " occurred:", Toast.LENGTH_LONG).show();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "The user canceled.", Toast.LENGTH_LONG).show();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Toast.makeText(getActivity(), "An invalid Payment or PayPalConfiguration" +
                        " was submitted. Please see the docs.", Toast.LENGTH_LONG).show();
            }if (requestCode == LOAD_MASKED_WALLET_REQUEST_CODE) { // Unique, identifying constant
//                if (resultCode == Activity.RESULT_OK) {
//                    MaskedWallet maskedWallet = data.getParcelableExtra(WalletConstants.EXTRA_MASKED_WALLET);
//                    FullWalletRequest fullWalletRequest = FullWalletRequest.newBuilder()
//                            .setCart(Cart.newBuilder()
//                                    .setCurrencyCode("USD")
//                                    .setTotalPrice("20.00")
//                                    .addLineItem(LineItem.newBuilder() // Identify item being purchased
//                                            .setCurrencyCode("USD")
//                                            .setQuantity("1")
//                                            .setDescription("Premium Llama Food")
//                                            .setTotalPrice("20.00")
//                                            .setUnitPrice("20.00")
//                                            .build())
//                                    .build())
//                            .setGoogleTransactionId(maskedWallet.getGoogleTransactionId())
//                            .build();
//                    Wallet.Payments.loadFullWallet(googleApiClient, fullWalletRequest, LOAD_FULL_WALLET_REQUEST_CODE);
//                }
//            } else if (requestCode == LOAD_FULL_WALLET_REQUEST_CODE) {
//                if (resultCode == Activity.RESULT_OK) {
//                    FullWallet fullWallet = data.getParcelableExtra(WalletConstants.EXTRA_FULL_WALLET);
//                    String tokenJSON = fullWallet.getPaymentMethodToken().getToken();
//
//                    //A token will only be returned in production mode,
//                    //i.e. WalletConstants.ENVIRONMENT_PRODUCTION
//                    if (mEnvironment == WalletConstants.ENVIRONMENT_PRODUCTION)
//                    {
//                        com.stripe.model.Token token = com.stripe.model.Token.GSON.fromJson(
//                                tokenJSON, com.stripe.model.Token.class);
//
//                        // TODO: send token to your server
//                    }
//                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }

        }
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        getActivity().stopService(new Intent(getActivity(), PayPalService.class));
        super.onDestroy();
    }


    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
//            sb.append("&components=country:gr");
            //  sb.append("&components=country:gr");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("address", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("address", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            Log.e("josn",jsonObj+"");
            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e("address", "Cannot process JSON results", e);
        }

        return resultList;
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {


        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {


                    FilterResults filterResults = new FilterResults();
                    try{
                        if (constraint != null) {
                            // Retrieve the autocomplete results.
                            resultList = autocomplete(constraint.toString());

                            // Assign the data to the FilterResults
                            filterResults.values = resultList;
                            filterResults.count = resultList.size();
                        }
                    }catch (Exception e){
                        Log.d("error",e+"");
                    }
                    return filterResults;

                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    try{
                        if (results != null && results.count > 0) {
                            notifyDataSetChanged();
                        } else {
                            notifyDataSetInvalidated();
                        }
                    }catch (Exception e){
                        Log.d("error",e+"");
                    }
                }
            };
            return filter;
        }
    }
    public void onStart() {
        super.onStart();
//        googleApiClient.connect();
    }

    public void onStop() {
        super.onStop();
      //  googleApiClient.disconnect();
    }






    class AsyncTaskOrderSubmit extends AsyncTask<String, String, String> {

        int success = 5, mailcheck = 5;

        HttpResponse response;
        JSONObject response1;
        String responseBody;
        Boolean availableProduct, availablemail;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            {

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        Url.ORDER);

                httpPost.setHeader("content-type", "application/json");
                httpPost.setHeader("Accept", "application/json");
                //  JSONObject jsonObject1 = new JSONObject();
                JSONObject jsonObject = new JSONObject();


                try{

                    JSONArray productArray = null;
                    try {
                        if(!orderDetails.equals("")) {
                            productArray = new JSONArray(orderDetails);
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }


                    String type;
                    if(sharePref.getshareprefdatastring(SharePref.ORDERTYPE).equals("rush")){

                        type = "rush";
                    }else{
                        type="normal";
                    }

                    jsonObject.accumulate("deliverytype", type);
                    jsonObject.accumulate("orderdetails", productArray);
                    jsonObject.accumulate("deliverycost",deliverycost);
                    jsonObject.accumulate("notes", notes);
                    jsonObject.accumulate("specialprice", specialCost);
                    jsonObject.accumulate("cardFee", creditCardFeeAmount);

                    jsonObject.accumulate("address",address);
                    jsonObject.accumulate("tipsAmount",tipsString);
                    jsonObject.accumulate("email",email);
                    jsonObject.accumulate("telephone",telephone);


                    jsonObject.accumulate("firstname",firstname);
                    jsonObject.accumulate("storeid",shopID);
                    jsonObject.accumulate("customerid",userID );

                    jsonObject.accumulate("paymentid",paymentid );
                    jsonObject.accumulate("token",paymenttoken );
                    jsonObject.accumulate("payerid",payerid );
                    jsonObject.accumulate("type",paymenttype );
                    jsonObject.accumulate("method",paymentmethod );
                    jsonObject.accumulate("brand",paymentbrand);
                    jsonObject.accumulate("paymentstatus","1");
                    // jsonObject.accumulate("postalcode", postalcode);
                    // jsonObject.accumulate("city",city);
                    // jsonObject.accumulate("lastname",lastname );
                    Log.e("res",jsonObject.toString()+"");

                }
                catch (Exception e) {
//
//                    Log.d("InputStream", e.getLocalizedMessage());

                }

                StringEntity entity = null;




                try {
                    entity = new StringEntity( jsonObject.toString());
                    entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    // Log.d("ch",entity+"");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                httpPost.setEntity(entity);

                try {
                    //  response = httpClient.execute(httpPost);
//                    resC++;
//                    Log.e("Value of response count: ", resC + "");
                    ResponseHandler<String> responseHandler=new BasicResponseHandler();
                    responseBody =httpClient.execute(httpPost, responseHandler);
                    response1=new JSONObject(responseBody);
                    Log.e("sir", responseBody + "");



                    // invoiceNumber= response1.getString("orderno");



                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(String string) {

            progressSweetAlertDialog.dismiss();

            int success;
            String msg = "";
            try {
                success =  response1.getInt("success");

                msg = response1.getString("message");

                if(success==1){




                    db.clearCartItem(shopID);

                    ((DrawerActivity)getActivity()).hideCartButton(false,0);

                    doneDialog=  new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText("Success");
                    doneDialog.setContentText(msg);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            FragmentTransaction ft = fm.beginTransaction();
                            Fragment_last_order llf = new Fragment_last_order();


                            ((DrawerActivity) getActivity()).fragmentStack.lastElement().onStop();
                            ((DrawerActivity) getActivity()).fragmentStack.push(llf);

                            ft.add(R.id.frame_container, llf);
                            ft.commit();
                            sweetAlertDialog.dismiss();

                        }
                    });



                }else if(success ==0){





                    doneDialog=  new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE);


                    doneDialog.setContentText(msg);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    });

                }



            }catch (JSONException e) {
                e.printStackTrace();
            }





        }



        @Override
        protected void onPreExecute() {

            progressSweetAlertDialog.show();


        }

    }

    public boolean nullCheck() {
        boolean flag = false;

        if (!firstNameEdt.getText().toString().trim().equalsIgnoreCase("")) {
           // if (!lastNameEdt.getText().toString().trim().equalsIgnoreCase("")) {
                if (!contactEdt.getText().toString().trim().equalsIgnoreCase("")) {
                    if (!emailEdt.getText().toString().trim().equalsIgnoreCase("")) {
                        if (!address.trim().equalsIgnoreCase("")) {

                          //  if (!postalEdt.getText().toString().trim().equalsIgnoreCase("")) {

                            //    if (!cityEdt.getText().toString().trim().equalsIgnoreCase("")) {
///
                                    return true;

                             //   }else {

                           //         message = "Please Enter Your City!";
                           //     }

                          //  }else {

                         //       message = "Please Enter Postal Code!";
                         //   }

                        }else {

                            message = "Please Enter valid Address!";
                        }
                    } else {
                        message = "Please Enter valid Email!";
                    }
                } else {
                    message = "Please Enter Your Contact Number!";
                }

         //   } else {
          //      message = "Please Enter Your Last Name!";
           // }

        } else {
            message = "Please Enter Your  Name!";
        }


        if(!flag) {
            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(message)
                    .show();
        }



        return flag;


    }



    public void PaymentDialoge(){
        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.stripes_dialog1);
        // Set dialog title
        //dialog.setTitle("Category Add");

        dialog.show();

        Button done = (Button) dialog.findViewById(R.id.done);
        final EditText cardNumber = (EditText) dialog.findViewById(R.id.cardNo);
        final EditText cvc = (EditText) dialog.findViewById(R.id.cvc);
        final EditText expire = (EditText) dialog.findViewById(R.id.expire);
        // save = (Button) dialog.findViewById(R.id.submit);
        TextView amount = (TextView) dialog.findViewById(R.id.totalPayment);


        amount.setText("$"+sharePref.getshareprefdatastring(SharePref.TOTALORDERPRICEVALUE));

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cardNumberString = cardNumber.getText().toString();

                cvcString = cvc.getText().toString();

                expireString = expire.getText().toString();

                sendPaymentInfo(dialog);

            }
        });


//        expire.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//
//                if (!hasFocus) {
//
//
//                    try {
//                        specialPriceValue = Float.parseFloat(specialOrderEdt.getText().toString());
//                        getTotalPrice();
//                    }catch (Exception e){
//
//                    }
//                }
//
//            }
//        });
        expire.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                try {
                    String ex = expire.getText().toString();

                    if(ex.length()==2){
                        ex =ex+"/";
                        expire.setText(ex);
                        expire.moveCursorToVisibleOffset();
                    }

                }catch (Exception e){

                }
            }
        });





    }



    public void sendPaymentInfo(final Dialog dialog) {

        try {
            String temp = expireString.replace("/", " ");

            String[] time = temp.split("\\ ");

            int month = Integer.parseInt(time[0]);
            int year = Integer.parseInt(time[1]);

           final Card card = new Card(cardNumberString, month, year, cvcString);



            progressSweetAlertDialog.show();

            try {
             Stripe stripe = new Stripe("pk_test_HaqjEbFSxydh5XWiHBYRU2cA");

                stripe.createToken(
                        card,
                        new TokenCallback() {
                            public void onSuccess(Token token) {

                                com.stripe.Stripe.apiKey = "sk_test_awq6zoTvWVes2f89PbnpCZ0I";


                                PaymentToken = token.getId() + "";
                                paymenttoken = token.getId()+"";
//                            String stripeToken = token.getParameter("stripeToken");


                                String totalOrderAmount = sharePref.getshareprefdatastring(SharePref.TOTALORDERPRICEVALUE);
                                Log.e("token", totalOrderAmount + "");
                                Float total = Float.parseFloat(totalOrderAmount) * 100;

                                int b = (int) Math.round(total);

                                try {
                                    Map<String, Object> chargeParams = new HashMap<String, Object>();
                                    chargeParams.put("amount", b); // amount in cents, again
                                    chargeParams.put("currency", "usd");
                                    chargeParams.put("source", PaymentToken);
                                    chargeParams.put("description", "Example charge");
                                    chargeParams.put("application_fee", sharePref.getshareprefdatastring(SharePref.BODEGA_FEE));
                                    chargeParams.put("destination", sharePref.getshareprefdatastring(SharePref.STRIPE_ID));


                                    try {
                                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                                        StrictMode.setThreadPolicy(policy);
                                        //    RequestOptions requestOptions = RequestOptions.builder().setStripeAccount("acct_18G5qrAuFrqAcgAL").build();

                                        Charge charge = null;
                                        try {
                                            charge = Charge.create(chargeParams);
                                        } catch (AuthenticationException e) {
                                            e.printStackTrace();
                                        }


                                       Log.e("cha" , charge.toString());

                                        paymentid = charge.getId();
                                        payerid = charge.getSource().getId();


                                        progressSweetAlertDialog.dismiss();
                                        dialog.dismiss();
                                        new AsyncTaskOrderSubmit().execute();

                                    } catch (InvalidRequestException e) {
                                        e.printStackTrace();
                                        progressSweetAlertDialog.dismiss();
                                    } catch (APIConnectionException e) {
                                        e.printStackTrace();
                                        progressSweetAlertDialog.dismiss();
                                    } catch (APIException e) {
                                        e.printStackTrace();
                                        progressSweetAlertDialog.dismiss();
                                    }
                                } catch (CardException e) {

                                    progressSweetAlertDialog.dismiss();
                                    Log.e("eror",e.toString());

                                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Oops...")
                                            .setContentText("Your Card Has Been Declined")
                                            .show();

                                }

                            }

                            public void onError(Exception error) {
                                // Show localized error message

                                progressSweetAlertDialog.dismiss();

                                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("Your Card Has Been Declined")
                                        .show();
                            }
                        }
                );


            } catch (AuthenticationException e) {
                e.printStackTrace();

                progressSweetAlertDialog.dismiss();

                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Your Card Has Been Declined")
                        .show();
            }


        }catch (Exception e){
            progressSweetAlertDialog.dismiss();
            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Wrong card information")
                    .show();

        }
    }

    }


