package com.bodega.customer.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.activity.SoccetIOChat;
import com.bodega.customer.adapter.MessageListAdapter;
import com.bodega.customer.adapter.NewMessageListItem;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MessageList extends Fragment {


    ArrayList<NewMessageListItem>  messageListItemArrayList;

    ListView messageList;
    TextView barText;
    TextView emptyText;

    MyDatabase db;
    LinearLayout empty;
    ImageButton backBtn;

    JSONParser jsonParser;
    int error;

    boolean isInternetPresent=false;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    ConnectionDetector cd;

    String userID;

    SharePref sharePref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.activity_message_list, container,
                false);
        backBtn = (ImageButton) v.findViewById(R.id.backBtn);


        messageList = (ListView)v. findViewById(R.id.msgList);

        db = new MyDatabase(getActivity());

      //  messageListItemArrayList = db.getMessageList();

        empty = (LinearLayout) v.findViewById(R.id.empty);

        emptyText = (TextView) v.findViewById(R.id.emptyText);

        jsonParser = new JSONParser();

        sharePref = new SharePref(getActivity());

        cd=  new ConnectionDetector(getActivity());

        isInternetPresent = cd.isConnectingToInternet();

        barText = (TextView) v.findViewById(R.id.barText);

        userID = sharePref.getshareprefdatastring(SharePref.USERID);
        ((DrawerActivity)getActivity()).setCounter(0);

        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
        messageListItemArrayList = new ArrayList<>();


        chatIsempty();
        if(isInternetPresent){

          new  AsyncGetAllChat().execute();
        }

    //  ((DrawerActivity)getActivity()).hideCartButton(true,1);

        messageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(getActivity(), SoccetIOChat.class);
                intent.putExtra("shopId",messageListItemArrayList.get(position).getUserID());
                intent.putExtra("shopName",messageListItemArrayList.get(position).getUserName());

                startActivity(intent);

            }
        });
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   Log.i("si:",(DrawerActivity.class).fragmentStack.size()+"");
                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onPause();
                ft.remove(((DrawerActivity) getActivity()).fragmentStack.pop());

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onResume();
                ft.show(((DrawerActivity) getActivity()).fragmentStack.lastElement());
                //  Log.i("bbb",fragmentStack.lastElement()+"");


                ft.commit();
            }
        });


return v;
    }

    @Override
    public void onResume() {

        ((DrawerActivity) getActivity()). hideTextButton(false);
        ((DrawerActivity) getActivity()). hideCartButton(false,0);
        ((DrawerActivity) getActivity()).newMessageCounter =0;
                super.onResume();
    }

    //    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }

    class AsyncGetAllChat extends AsyncTask<String, String, String> {



        @Override
        protected void onPreExecute() {

            try {

                progressSweetAlertDialog.show();

                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {

            }

        }

        @Override
        protected String doInBackground(String... params) {

            messageListItemArrayList.clear();
            List<NameValuePair> pair = new ArrayList<NameValuePair>();


         //   pair.add(new BasicNameValuePair("conversation", conversationID));


            String url ="http://192.169.227.95:3000/api/chat/"+userID;
            JSONObject json =null;
              json = jsonParser.makeHttpRequest(url, "GET", pair);

            Log.e("jsonTset", json + "");

            try {
                error = json.getInt("error") ;

                if(error==0){



                JSONArray conversationsArray = json.getJSONArray("records");


            for(int i=0;i<conversationsArray.length();i++) {



                JSONObject jsonObject = conversationsArray.getJSONObject(i);


                String senderId = jsonObject.getString("sender_id");

                String senderName = jsonObject.getString("sender_name");

                String receiver_id = jsonObject.getString("receiver_id");

                String receiver_name = jsonObject.getString("receiver_name");

                String dateNdTime = jsonObject.getString("date");

                if(senderId.equals(userID)) {

                    NewMessageListItem newMessageListItem = new NewMessageListItem(receiver_id, receiver_name, dateNdTime);

                    messageListItemArrayList.add(newMessageListItem);
                }else {
                    NewMessageListItem newMessageListItem = new NewMessageListItem(senderId, senderName, dateNdTime);

                    messageListItemArrayList.add(newMessageListItem);
                }


            }


            }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("jsonLeng", messageListItemArrayList.size() + "");

            return  null;
        }

        @Override
        protected void onPostExecute(String result) {


            MessageListAdapter messageListAdapter = new MessageListAdapter(getActivity(),messageListItemArrayList);


            messageList.setAdapter(messageListAdapter);
            chatIsempty();
            progressSweetAlertDialog.dismiss();
        }

    }


    public void chatIsempty(){
        if(messageListItemArrayList.isEmpty()){
            empty.setVisibility(View.VISIBLE);

            messageList.setVisibility(View.INVISIBLE);
        }else{
            messageList.setVisibility(View.VISIBLE);
            empty.setVisibility(View.INVISIBLE);
        }
    }

}
