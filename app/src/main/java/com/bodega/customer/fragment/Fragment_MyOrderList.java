package com.bodega.customer.fragment;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.adapter.OrderAdapter;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.model.OrderModel;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Fragment_MyOrderList extends Fragment {


    ArrayList<OrderModel>  orderModelArrayList;

    ListView orderList;
    TextView barText;
    TextView emptyText;

    MyDatabase db;
    LinearLayout empty;
    ImageButton backBtn;

    JSONParser jsonParser;
    int error;

    boolean isInternetPresent=false;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    ConnectionDetector cd;

    String userID;

    SharePref sharePref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.activity_activity_order_details, container,
                false);
        backBtn = (ImageButton) v.findViewById(R.id.backBtn);


        orderList = (ListView)v. findViewById(R.id.msgList);

        db = new MyDatabase(getActivity());

      //  orderModelArrayList = db.getMessageList();

        empty = (LinearLayout) v.findViewById(R.id.empty);

        emptyText = (TextView) v.findViewById(R.id.emptyText);

        jsonParser = new JSONParser();

        sharePref = new SharePref(getActivity());

        cd=  new ConnectionDetector(getActivity());

        isInternetPresent = cd.isConnectingToInternet();

        barText = (TextView) v.findViewById(R.id.barText);

        userID = sharePref.getshareprefdatastring(SharePref.USERID);


        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
        orderModelArrayList = new ArrayList<>();


        chatIsempty();
        if(isInternetPresent){

          new  AsyncGetAllOrder().execute();
        }

    //  ((DrawerActivity)getActivity()).hideCartButton(true,1);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   Log.i("si:",(DrawerActivity.class).fragmentStack.size()+"");
                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onPause();
                ft.remove(((DrawerActivity) getActivity()).fragmentStack.pop());

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onResume();
                ft.show(((DrawerActivity) getActivity()).fragmentStack.lastElement());
                //  Log.i("bbb",fragmentStack.lastElement()+"");


                ft.commit();
            }
        });


return v;
    }

    @Override
    public void onResume() {
        ((DrawerActivity)getActivity()).hideTextButton(true);
        ((DrawerActivity)getActivity()).hideCartButton(true,0);
        super.onResume();
    }

    //    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }

    class AsyncGetAllOrder extends AsyncTask<String, String, String> {



        @Override
        protected void onPreExecute() {

            try {

                progressSweetAlertDialog.show();

                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {

            }

        }

        @Override
        protected String doInBackground(String... params) {

            orderModelArrayList.clear();
            List<NameValuePair> pair = new ArrayList<NameValuePair>();


          pair.add(new BasicNameValuePair("customerid", userID));



            JSONObject json =null;
              json = jsonParser.makeHttpRequest(Url.MYLASTORDERLIST, "GET", pair);

            Log.e("jsonTset", json + "");

            try {
                error = json.getInt("error") ;

                if(error==0){



                JSONArray conversationsArray = json.getJSONArray("records");


            for(int i=0;i<conversationsArray.length();i++) {



                JSONObject jsonObject = conversationsArray.getJSONObject(i);



                 String id= jsonObject.getString("id");

                 String customerid= jsonObject.getString("customerid");

                 String storeid= jsonObject.getString("storeid");

                 String firstname= jsonObject.getString("firstname");

                 String lastname= jsonObject.getString("lastname");

                 String telephone= jsonObject.getString("telephone");

                 String email= jsonObject.getString("email");

                 String address= jsonObject.getString("address");

                 String city= jsonObject.getString("city");

                 String postalcode= jsonObject.getString("postalcode");

                 String notes= jsonObject.getString("notes");

                 String deliverycost= jsonObject.getString("deliverycost");

                 String deliverytype= jsonObject.getString("deliverytype");

                 String specialprice= jsonObject.getString("specialprice");

                 String status= jsonObject.getString("status");

                 String createdAt= jsonObject.getString("created_at");

                 String updatedAt= jsonObject.getString("updated_at");

                 String storename= jsonObject.getString("storename");




                    OrderModel orderModel = new OrderModel( id,  customerid,  storeid,  firstname,  lastname,  telephone,  email,  address,  city,  postalcode,  notes,  deliverycost,  deliverytype,  specialprice,  status,  createdAt,  updatedAt, storename);

                    orderModelArrayList.add(orderModel);



            }


            }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("jsonLeng", orderModelArrayList.size() + "");

            return  null;
        }

        @Override
        protected void onPostExecute(String result) {


            OrderAdapter orderAdapter = new OrderAdapter(getActivity(),orderModelArrayList);


            orderList.setAdapter(orderAdapter);
            chatIsempty();
            progressSweetAlertDialog.dismiss();
        }

    }


    public void chatIsempty(){
        if(orderModelArrayList.isEmpty()){
            empty.setVisibility(View.VISIBLE);

            orderList.setVisibility(View.INVISIBLE);
        }else{
            orderList.setVisibility(View.VISIBLE);
            empty.setVisibility(View.INVISIBLE);
        }
    }

}
