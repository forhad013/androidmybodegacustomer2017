package com.bodega.customer.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.activity.LoginActivity;
import com.bodega.customer.activity.SoccetIOChat;
import com.bodega.customer.adapter.OrderProductAdapter;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.gcm.RegistrationIntentService;
import com.bodega.customer.model.OrderProductModel;
import com.bodega.customer.twillio.ClientActivity;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.bodega.customer.R.id.storeName;

public class Fragment_last_order extends Fragment {
    TextView storeNameTxt,amount,statusTxt,date,time,type,payment,specialPrice,tipsPrice,deliveryCost;
    RelativeLayout callBtn,msgBtn;

    ListView productList;
    String id,  customerid,  storeid,  firstname,  lastname,  telephone,  email,  address,  city,  postalcode,  notes,  deliverycost,  deliverytype,  specialprice,  status,  createdAt,  updatedAt,  storename;
    String cardFee;
    String tipsAmount,totalProductPrice;
    String storeContact,gcmCode,loginStatus;
    boolean isInternetOn;

    ConnectionDetector cd;

    MyDatabase db;
    
    ArrayList<OrderProductModel> orderProductModelArrayList;

    String userID,message,storeOwnerName,profilePicString;
    TextView totalPriceTv;
    int error;
    ;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    SharePref sharePref;
    JSONParser jsonParser;

    Button cancelBtn;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.activity_activity_order_details, container, false);

         storeNameTxt = (TextView)  v.findViewById(storeName);
         amount = (TextView)  v.findViewById(R.id.amount);
         statusTxt = (TextView) v. findViewById(R.id.status);
         date = (TextView) v. findViewById(R.id.date);
         time = (TextView) v. findViewById(R.id.time);
         type = (TextView)  v.findViewById(R.id.type);
        totalPriceTv = (TextView)  v.findViewById(R.id.total);
         payment = (TextView) v. findViewById(R.id.payment);
        callBtn = (RelativeLayout) v.findViewById(R.id.call);

        specialPrice= (TextView)  v.findViewById(R.id.specialPrice);
        tipsPrice= (TextView)  v.findViewById(R.id.tipsAmount);
        deliveryCost= (TextView)  v.findViewById(R.id.deliverycost);


        msgBtn= (RelativeLayout)v. findViewById(R.id.sms);
        productList = (ListView) v.findViewById(R.id.list);
        cancelBtn = (Button) v.findViewById(R.id.cancel_button);
        cd = new ConnectionDetector(getActivity());

        sharePref = new SharePref(getActivity());

        jsonParser = new JSONParser();

        orderProductModelArrayList = new ArrayList<>();
        
        db = new MyDatabase(getActivity());

        isInternetOn = cd.isConnectingToInternet();

        userID = sharePref.getshareprefdatastring(SharePref.USERID);


        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);




        if(isInternetOn){

            new AsyncOrderDetails().execute();
        }


        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                if (loginStatus.equals("yes")) {
//                    CallToOwner call = new CallToOwner(getActivity(), storeContact);
//                    call.call();

                    Intent in = new Intent(getActivity(), ClientActivity.class);

                    in.putExtra("image", profilePicString);
                    in.putExtra("ownerName", storename);
                    in.putExtra("storeContact", telephone);



                    startActivity(in);




                    startActivity(in);
                } else {

                    if(!gcmCode.isEmpty()) {

                        Intent in = new Intent(getActivity(), LoginActivity.class);

                        in.putExtra("token", storeOwnerName);

                        startActivity(in);
                    }else {

                        if(isInternetOn) {
                            if (checkPlayServices()) {
                                Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
                                getActivity().startService(intent);

                            }
                            doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                            doneDialog.setTitleText("Wait for few second");
                            // doneDialog.setContentText("Login unsuccessful");
                            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    doneDialog.dismiss();
                                }
                            });
                            doneDialog.show();

                        }

                    }
                }
            }
        });


        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                doneDialog.setTitleText("Sorry");
                 doneDialog.setContentText("You need to call store owner to cancel the order");
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        doneDialog.dismiss();
                    }
                });
                doneDialog.show();

            }
        });

        msgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                if(loginStatus.equals("yes")) {



                    db.saveStore(storeid,storename,storeOwnerName,"",storeContact);


                    Intent in = new Intent(getActivity(), SoccetIOChat.class);

                    in.putExtra("shopId", storeid);

                    in.putExtra("shopEmail", email);
                    in.putExtra("shopName", storename);
                
                    startActivity(in);
                }else{

                    if(!gcmCode.isEmpty()) {

                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        in.putExtra("token", storeOwnerName);
                        startActivity(in);
                    }else{
                        if(isInternetOn) {
                            if (checkPlayServices()) {
                                Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
                                getActivity().startService(intent);

                            }
                            doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                            doneDialog.setTitleText("Wait for few second");
                            // doneDialog.setContentText("Login unsuccessful");
                            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    doneDialog.dismiss();
                                }
                            });
                            doneDialog.show();

                        }
                    }
                }
            }
        });


        ImageButton bckBtn = (ImageButton) v.findViewById(R.id.backBtn);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onPause();
                ft.remove(((DrawerActivity) getActivity()).fragmentStack.pop());

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onResume();
                ft.show(((DrawerActivity) getActivity()).fragmentStack.lastElement());
                //  Log.i("bbb",fragmentStack.lastElement()+"");


                ft.commit();
            }
        });


        return  v;
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(getActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                 getActivity().finish();
            }
            return false;
        }
        return true;
    }


    class AsyncOrderDetails extends AsyncTask<String, String, String> {



        @Override
        protected void onPreExecute() {

            try {

                progressSweetAlertDialog.show();

                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {

            }

        }

        @Override
        protected String doInBackground(String... params) {

            orderProductModelArrayList.clear();
            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("customerid", userID));



            JSONObject json =null;
            json = jsonParser.makeHttpRequest(Url.MYLASTORDERLIST, "GET", pair);

            Log.e("jsonTset", json + "");

            try {
                error = json.getInt("error") ;
                message =json.getString("message");
                if(error==0) {


                    JSONObject records = json.getJSONObject("records");


                    id = records.getString("id");
                    storeid = records.getString("storeid");
                    firstname = records.getString("firstname");
                    lastname = records.getString("lastname");
                    telephone = records.getString("telephone");
                    email = records.getString("email");
                    address = records.getString("email");
                    notes = records.getString("notes");
                    deliverycost = records.getString("deliverycost");
                    deliverytype = records.getString("deliverytype");
                    specialprice = records.getString("specialprice");
                    status = records.getString("deliverytype");
                    createdAt = records.getString("created_at");
//                    updatedAt= records.getString("updated_at");
                    storename = records.getString("storename");
                    tipsAmount = records.getString("tips");


                    try {
                        JSONArray conversationsArray = records.getJSONArray("products");

                        Float total = 0.00f;

                        for (int i = 0; i < conversationsArray.length(); i++) {


                            JSONObject jsonObject = conversationsArray.getJSONObject(i);


                            String productID = "";

                            String productName = jsonObject.getString("productname");

                            String quantity = jsonObject.getString("quantity");

                            String totalPrice = jsonObject.getString("totalprice");

                            total = total + Float.parseFloat(totalPrice);


                            OrderProductModel orderModel = new OrderProductModel(productID, productName, quantity, totalPrice);
                            orderProductModelArrayList.add(orderModel);


                        }

                        totalProductPrice = total + "";


                    }catch (Exception e){
                        totalProductPrice ="0";
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



            return  null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressSweetAlertDialog.dismiss();
            if(error==0) {
                setData();

                OrderProductAdapter orderAdapter = new OrderProductAdapter(getActivity(), orderProductModelArrayList);


                productList.setAdapter(orderAdapter);
            }else{
                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                doneDialog.setTitleText(message);
                // doneDialog.setContentText("Login unsuccessful");
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        getActivity().onBackPressed();
                        doneDialog.dismiss();
                    }
                });
                doneDialog.show();

            }


        }

    }


    public void setData(){

        String[] orderTime = createdAt.split("\\ ");

        String dateS = orderTime[0];
        String timeS = orderTime[1];


        storeNameTxt.setText(storename);

        String x = status;

        if(x.equals("0")){
            x = "Ordered";
        }else if(x.equals("0")){
            x.equals("Delivered");
        }else{
            x.equals("Canceled");
        }

        statusTxt.setText("Status : "+x);
        date.setText("Date : "+dateS);
        time.setText("Time : "+timeS);
        type.setText("Order Type : "+deliverytype);

        if(tipsAmount.equals("null")){
            tipsAmount = "0";
        }
        if(deliverycost.equals("null")){
            deliverycost = "0";
        }
        if(specialprice.equals("null")){
            specialprice = "0";
        }


        specialPrice.setText("Special Price: $"+specialprice);
        tipsPrice.setText("Tips : $"+tipsAmount);
        deliveryCost.setText("Delivery Cost: $"+deliverycost);


        Float total = Float.parseFloat(specialprice)+Float.parseFloat(tipsAmount)+Float.parseFloat(deliverycost)+Float.parseFloat(totalProductPrice);

          totalPriceTv.setText("Total Price: $"+total+"");

    }
}
