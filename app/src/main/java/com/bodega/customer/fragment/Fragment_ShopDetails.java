package com.bodega.customer.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.activity.LoginActivity;
import com.bodega.customer.activity.SoccetIOChat;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.gcm.RegistrationIntentService;
import com.bodega.customer.model.PopularProductModel;
import com.bodega.customer.twillio.ClientActivity;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class Fragment_ShopDetails extends Fragment {

    TextView storeNameTxt, address, owmer, open, close, paymentTextView, credit, paypal, other, freeDelivery, minimum, feeDelivery, rushDelivery;
    TextView smsOrder, phoneOrder, mondayTime, tusedayTime, wednesdayTime, thursdayTime, fridayTime, saturdayTime, sundayTime;
    RelativeLayout call, sms;
    JSONParser jsonParser;
    JSONObject jsonObject, storeDetail, storeHour, storeDeliveryFee;
    JSONArray jsonArray;
    String storeId;
    String message;
    String storeID, storeName, storeContact, storeLegalName, storeLocation, storeLatitude, storeLongitude, storeOwnerEmail, storeOwnerFirstName, storeOwnerLastName, storeOwnerWebAddress, storeTaxAmount, storeHours, storeDeliveryFess;
   // Typeface myTypeface, custom;

    PopularProductModel productsModel;

    String callOptionString;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    int success = 0;

    boolean dOption1, dOption2, dOption3, dOption4,dOption0;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    String dOption2Value, dOption3Value, dOption4Value;
    String trueOrfalse1, trueOrfalse2, trueOrfalse3, trueOrfalse4,trueOrfalse0;
    String loginStatus;
    ArrayList daysHour;
    SharePref sharePref;
    MapView shopMapView;
    GoogleMap googleMap;
    String storeStreetAddress;

    JSONArray products;

    GridView listProduct;
    TextView item3,item4,item5,item6,item7,item8,item9,item10,item11,item12,item13,item14;


    SupportMapFragment mapFragment;

    FragmentManager mFragmentManager;

    Date currenttime;
    String startTime,endTime;

    JSONObject storeImages;

    ImageButton backBtn;
    double Lat, Lon;
    Marker m1;

    TextView deliveryOption1Txt,deliveryOption2Txt;

    String logedIn;

    String profilePicString,stripeaccountid,bodegaFee;

    boolean isInternetOn;

    ConnectionDetector cd;

    MyDatabase db;

    CircleImageView image;

    String gcmCode,storeLandLine;

    TextView item1,item2;

    LinearLayout productlayout;

    Button allcategory;



    TextView p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p12,p11,p13,p14,popularTitle;

    ArrayList<PopularProductModel> productsModelArrayList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_shop_details, container,
                false);

        jsonParser = new JSONParser();
        sharePref = new SharePref(getActivity());
        loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);

        image  = (CircleImageView) v.findViewById(R.id.image);
        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
//
//        myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRnd-Book.otf");
//        custom = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoCondensed-Regular.ttf");

        storeNameTxt = (TextView) v.findViewById(R.id.storeName);
        address = (TextView) v.findViewById(R.id.address);
        owmer = (TextView) v.findViewById(R.id.ownerName);
        open = (TextView) v.findViewById(R.id.open);


        p1 = (TextView) v.findViewById(R.id.p1);
        p2 = (TextView) v.findViewById(R.id.p2);
        p3 = (TextView) v.findViewById(R.id.p3);
        p4 = (TextView) v.findViewById(R.id.p4);
        p5 = (TextView) v.findViewById(R.id.p5);
        p6 = (TextView) v.findViewById(R.id.p6);
        p7 = (TextView) v.findViewById(R.id.p7);
        p8 = (TextView) v.findViewById(R.id.p8);
        p9 = (TextView) v.findViewById(R.id.p9);
        p10 = (TextView) v.findViewById(R.id.p10);

        p11 = (TextView) v.findViewById(R.id.p11);
        p12 = (TextView) v.findViewById(R.id.p12);
        p13 = (TextView) v.findViewById(R.id.p13);
        p14 = (TextView) v.findViewById(R.id.p14);
        popularTitle= (TextView) v.findViewById(R.id.title);
        paymentTextView = (TextView) v.findViewById(R.id.payment);


        allcategory = (Button) v.findViewById(R.id.allCategory);




        gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);



        deliveryOption1Txt = (TextView) v.findViewById(R.id.option1);

        deliveryOption2Txt = (TextView) v.findViewById(R.id.option2);

        productsModelArrayList = new ArrayList<>();

        productlayout = (LinearLayout) v.findViewById(R.id.productLayout);


        smsOrder = (TextView) v.findViewById(R.id.smsOrder);
        phoneOrder = (TextView) v.findViewById(R.id.phoneOrder);
        mondayTime = (TextView) v.findViewById(R.id.MondayTime);
        tusedayTime = (TextView) v.findViewById(R.id.tuesdayTime);
        wednesdayTime = (TextView) v.findViewById(R.id.wednesdayTime);
        thursdayTime = (TextView) v.findViewById(R.id.thursdayTime);
        fridayTime = (TextView) v.findViewById(R.id.fridayTime);
        saturdayTime = (TextView) v.findViewById(R.id.saturdayTime);
        sundayTime = (TextView) v.findViewById(R.id.sundayTime);
        call = (RelativeLayout) v.findViewById(R.id.call);
        sms = (RelativeLayout) v.findViewById(R.id.sms);

        backBtn = (ImageButton) v.findViewById(R.id.backBtn);

        db = new MyDatabase(getActivity());

        ((DrawerActivity)getActivity()).hideTextButton(false);
        ((DrawerActivity)getActivity()).hideCartButton(false,0);
//        googleMap = shopMapView.getMap();

        cd = new ConnectionDetector(getActivity());

        isInternetOn = cd.isConnectingToInternet();

        storeId = sharePref.getshareprefdatastring(SharePref.CURRENTSHOPID);



        sharePref.setshareprefdatastring(SharePref.CURRENTSHOPID,storeId);

        sharePref.setshareprefdatastring(SharePref.CURRENTSHOPNAME,storeName);
       // Log.e("shopiD", storeId);
        daysHour = new ArrayList();

     //   Log.e("storeId", storeId);
        if(isInternetOn) {
            new AsyncTaskShopDetails().execute();
        }else{
            Toast.makeText(getActivity(), "No Internet conenction Found", Toast.LENGTH_SHORT).show();
        }
          setCounter();
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                if (loginStatus.equals("yes")) {
//                    CallToOwner call = new CallToOwner(getActivity(), storeContact);
//                    call.call();

                    Intent in = new Intent(getActivity(), ClientActivity.class);

                    in.putExtra("image", profilePicString);
                    in.putExtra("ownerName", storeOwnerFirstName + " " + storeOwnerLastName);

                    if(callOptionString.equals("userContact")){

                        in.putExtra("storeContact", storeContact);
                        Log.e("storeContact",storeContact);
                    }else{
                        in.putExtra("storeContact", storeLandLine);
                        Log.e("storeLandLine",storeLandLine);
                    }



                    startActivity(in);
                } else {

                    if(!gcmCode.isEmpty()) {

                        Intent in = new Intent(getActivity(), LoginActivity.class);

                        in.putExtra("token", storeOwnerFirstName + " " + storeOwnerLastName);

                        startActivity(in);
                    }else {

                        if(isInternetOn) {
                            if (checkPlayServices()) {
                                  Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
                                getActivity().startService(intent);

                            }
                            doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                            doneDialog.setTitleText("Wait for few second");
                            // doneDialog.setContentText("Login unsuccessful");
                            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                             doneDialog.dismiss();
                                }
                            });
                            doneDialog.show();

                        }

                    }
                }
            }
        });

        popularTitle.setVisibility(View.GONE);
        allcategory.setVisibility(View.GONE);
        p1.setVisibility(View.GONE);
        p2.setVisibility(View.GONE);
        p3.setVisibility(View.GONE);
        p4.setVisibility(View.GONE);
        p5.setVisibility(View.GONE);
        p6.setVisibility(View.GONE);
        p7.setVisibility(View.GONE);
        p8.setVisibility(View.GONE);
        p9.setVisibility(View.GONE);
        p10.setVisibility(View.GONE);
        p11.setVisibility(View.GONE);
        p12.setVisibility(View.GONE);
        p13.setVisibility(View.GONE);
        p14.setVisibility(View.GONE);


        allcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new Fragment_search_list();

                ((DrawerActivity)getActivity()).mFragmentManager = ((DrawerActivity)getActivity()).getSupportFragmentManager();

                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();


                ft.add(R.id.frame_container, fragment);
                ((DrawerActivity)getActivity()).fragmentStack.push(fragment);
                ft.commit();
            }
        });


        ((DrawerActivity) getActivity()). hideTextButton(false);
        ((DrawerActivity) getActivity()). hideCartButton(false,0);





        p1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(0);
            }
        });
        p2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(1);
            }
        });
        p3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(2);
            }
        });
        p4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(3);
            }
        });
        p5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(4);
            }
        });
        p6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(5);
            }
        });
        p7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(6);
            }
        });
        p8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(7);
            }
        });
        p9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(8);
            }
        });
        p10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(9);
            }
        });
        p11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(10);
            }
        });
        p12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(11);
            }
        });
        p13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(12);
            }
        });
        p14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartCation(13);
            }
        });




        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
                if(loginStatus.equals("yes")) {



                    db.saveStore(storeID,storeName,storeOwnerFirstName+" "+ storeOwnerLastName,storeStreetAddress,storeContact);


                    Intent in = new Intent(getActivity(), SoccetIOChat.class);

                    in.putExtra("shopId", storeID);
                    in.putExtra("shopEmail", storeOwnerEmail);
                    in.putExtra("shopName", storeName);
                    in.putExtra("conID", "");
                    startActivity(in);
                }else{

                    if(!gcmCode.isEmpty()) {

                        Intent in = new Intent(getActivity(), LoginActivity.class);
                        in.putExtra("token", storeOwnerFirstName + " " + storeOwnerLastName);
                        startActivity(in);
                    }else{
                        if(isInternetOn) {
                            if (checkPlayServices()) {
                                Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
                                getActivity().startService(intent);

                            }
                            doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                            doneDialog.setTitleText("Wait for few second");
                            // doneDialog.setContentText("Login unsuccessful");
                            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    doneDialog.dismiss();
                                }
                            });
                            doneDialog.show();

                        }
                    }
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


             //   Log.i("si:",(DrawerActivity.class).fragmentStack.size()+"");
                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onPause();
                ft.remove(((DrawerActivity) getActivity()).fragmentStack.pop());

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onResume();
                ft.show(((DrawerActivity) getActivity()).fragmentStack.lastElement());
                //  Log.i("bbb",fragmentStack.lastElement()+"");


                ft.commit();
            }
        });


        return v;
    }

    private void setUpMapIfNeeded()
    {


        mFragmentManager = getChildFragmentManager();

        FragmentTransaction ft = mFragmentManager.beginTransaction();

       Fragment fragment = new Fragment_map_short();

        Bundle args = new Bundle();
        args.putDouble("lat", Lat);
        args.putDouble("lon", Lon);

        fragment.setArguments(args);


        ft.add(R.id.map_content, fragment);

        ft.commit();

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(getActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                getActivity().finish();
            }
            return false;
        }
        return true;
    }


    public void addToCartCation(int position){

        db.checkCart(storeId,sharePref.getshareprefdatastring(SharePref.CURRENTSHOPNAME),
                productsModelArrayList.get(position).getProductId(),productsModelArrayList.get(position).getProductName()
                ,"1",productsModelArrayList.get(position).getPrice());
        setCounter();

    }
    public void setLocation(double lat, double lon) {

        LatLng loc = new LatLng(lat, lon);
        Log.e("sad", lat + " " + lon);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 13.0f));
        m1 = googleMap.addMarker(new MarkerOptions().position(loc).title(storeName));
        m1.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_shop));
        m1.showInfoWindow();
    }

    public void setDetails() {


        try {

            daysHour.add(getHours(storeHour.getJSONObject("MonDay")));
            daysHour.add(getHours(storeHour.getJSONObject("TuesDay")));
            daysHour.add(getHours(storeHour.getJSONObject("WednesDay")));
            daysHour.add(getHours(storeHour.getJSONObject("ThursDay")));
            daysHour.add(getHours(storeHour.getJSONObject("FriDay")));
            daysHour.add(getHours(storeHour.getJSONObject("SaturDay")));
            daysHour.add(getHours(storeHour.getJSONObject("SunDay")));


            sharePref.setshareprefdatastring(SharePref.CURRENTSHOPNFEES,storeDeliveryFee.toString());

            stripeaccountid = stripeaccountid+"";

            Log.e("stripeaccountid",stripeaccountid);
            if(stripeaccountid.equals("null")){


                dOption0 = true;
                trueOrfalse0 = "1";

                dOption1 = false;
                trueOrfalse1 = "0";
                dOption2 = false;
                trueOrfalse2 = "0";
                dOption3 = false;
                trueOrfalse3 = "0";
                dOption4 = false;
                trueOrfalse4= "0";

                sharePref.setshareprefdatastring(SharePref.STRIPE_ID,"");


            }else{

                sharePref.setshareprefdatastring(SharePref.STRIPE_ID,stripeaccountid);


                JSONObject deliveryOption4 = storeDeliveryFee.getJSONObject("dFour");

                dOption4Value = deliveryOption4.getString("value");

                trueOrfalse4 = deliveryOption4.getString("deliveryOption");

                if (trueOrfalse4.equals("0")) {


                    dOption4 = false;
                } else {
                    dOption4 = true;
                }



                JSONObject deliveryOption1 = storeDeliveryFee.getJSONObject("dOne");


                trueOrfalse1 = deliveryOption1.getString("deliveryOption");

                if (trueOrfalse1.equals("0")) {

                    dOption1 = false;

                } else {
                    dOption1 = true;
                }


                JSONObject deliveryOption2 = storeDeliveryFee.getJSONObject("dTwo");

                dOption2Value = deliveryOption2.getString("value");


                trueOrfalse2 = deliveryOption2.getString("deliveryOption");

                if (trueOrfalse2.equals("0")) {

                    dOption2 = false;

                } else {
                    dOption2 = true;
                }

                JSONObject deliveryOption3 = storeDeliveryFee.getJSONObject("dThree");

                dOption3Value = deliveryOption3.getString("value");

                trueOrfalse3 = deliveryOption3.getString("deliveryOption");

                if (trueOrfalse3.equals("0")) {
                    dOption3 = false;
                } else {
                    dOption3 = true;
                }

                JSONObject deliveryOption0 = storeDeliveryFee.getJSONObject("dZero");



                trueOrfalse0 = deliveryOption0.getString("deliveryOption");

                if (trueOrfalse0.equals("0")) {

                    dOption0 = false;

                } else {

                    dOption0 = true;
                }


            }





        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void setProducts(){

        if(products.length()!=0) {


            for (int i = 0; i < products.length(); i++) {


                JSONObject item = new JSONObject();

                try {
                    item = products.getJSONObject(i);


                    String productId = item.getString("id");
                    ;

                    String productName = item.getString("title");

                    String price = item.getString("unitprice");

                    String status = item.getString("instore");

                    if (status.equals("1")) {


                        productsModel = new PopularProductModel(productId, productName, price, status);


                        productsModelArrayList.add(productsModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        if(dOption0){

            allcategory.setVisibility(View.GONE);
        }

                Log.e("asd",productsModelArrayList.size()+"");

                if(productsModelArrayList.isEmpty()){
                    popularTitle.setVisibility(View.GONE);
                }else if(dOption0) {
                    popularTitle.setVisibility(View.GONE);
                    allcategory.setVisibility(View.GONE);
                    p1.setVisibility(View.GONE);
                    p2.setVisibility(View.GONE);
                    p3.setVisibility(View.GONE);
                    p4.setVisibility(View.GONE);
                    p5.setVisibility(View.GONE);
                    p6.setVisibility(View.GONE);
                    p7.setVisibility(View.GONE);
                    p8.setVisibility(View.GONE);
                    p9.setVisibility(View.GONE);
                    p10.setVisibility(View.GONE);
                    p11.setVisibility(View.GONE);
                    p12.setVisibility(View.GONE);
                    p13.setVisibility(View.GONE);
                    p14.setVisibility(View.GONE);


                }
                else{

                    allcategory.setVisibility(View.VISIBLE);

                    try{
                        p1.setText(setPopularName(productsModelArrayList.get(0).getProductName(),productsModelArrayList.get(0).getPrice()));
                        p1.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        p1.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    try{
                        p2.setText(setPopularName(productsModelArrayList.get(1).getProductName(),productsModelArrayList.get(1).getPrice()));
                        p2.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        e.printStackTrace();
                        p2.setVisibility(View.GONE);
                    }
                    try{
                        p3.setText(setPopularName(productsModelArrayList.get(2).getProductName(),productsModelArrayList.get(2).getPrice()));
                        p3.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        e.printStackTrace();
                        p3.setVisibility(View.GONE);
                    }
                    try{
                        p4.setText(setPopularName(productsModelArrayList.get(3).getProductName(),productsModelArrayList.get(3).getPrice()));
                        p4.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        e.printStackTrace();
                        p4.setVisibility(View.GONE);
                    }
                    try{
                        p5.setText(setPopularName(productsModelArrayList.get(4).getProductName(),productsModelArrayList.get(4).getPrice()));
                        p5.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        e.printStackTrace();
                        p5.setVisibility(View.GONE);
                    }
                    try{
                        p6.setText(setPopularName(productsModelArrayList.get(5).getProductName(),productsModelArrayList.get(5).getPrice()));
                        p6.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        e.printStackTrace();
                        p6.setVisibility(View.GONE);
                    }
                    try{
                        p7.setText(setPopularName(productsModelArrayList.get(6).getProductName(),productsModelArrayList.get(6).getPrice()));
                        p7.setVisibility(View.VISIBLE);
                    }catch (Exception e) {
                        p7.setVisibility(View.GONE);
                    }
                    try{
                        p8.setText(setPopularName(productsModelArrayList.get(7).getProductName(),productsModelArrayList.get(7).getPrice()));
                        p8.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        p8.setVisibility(View.GONE);
                    }
                    try{
                        p9.setText(setPopularName(productsModelArrayList.get(8).getProductName(),productsModelArrayList.get(8).getPrice()));
                        p9.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        p9.setVisibility(View.GONE);
                    }
                    try{
                        p10.setText(setPopularName(productsModelArrayList.get(9).getProductName(),productsModelArrayList.get(9).getPrice()));
                        p10.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        p10.setVisibility(View.GONE);
                    }
                    try{
                        p11.setText(setPopularName(productsModelArrayList.get(10).getProductName(),productsModelArrayList.get(10).getPrice()));
                        p11.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        p11.setVisibility(View.GONE);
                    }
                    try{
                        p12.setText(setPopularName(productsModelArrayList.get(11).getProductName(),productsModelArrayList.get(11).getPrice()));
                        p12.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        p12.setVisibility(View.GONE);
                    }
                    try{
                        p13.setText(setPopularName(productsModelArrayList.get(12).getProductName(),productsModelArrayList.get(12).getPrice()));
                        p13.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        p13.setVisibility(View.GONE);
                    }
                    try{
                        p14.setText(setPopularName(productsModelArrayList.get(13).getProductName(),productsModelArrayList.get(13).getPrice()));
                        p14.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        p14.setVisibility(View.GONE);
                    }
                }





    }

    public  String setPopularName(String name,String price){
        String text="";

        text = name;
        text = text + System.getProperty("line.separator")+"Price: $"+price;


        return text;
    }

    @Override
    public void onResume() {
        ((DrawerActivity)getActivity()).hideTextButton(true);

       setCounter();
        super.onResume();
    }

    public void setData(){



        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);

        Log.e("pp", profilePicString);

       ImageLoader.getInstance().displayImage(profilePicString, image, defaultOptions);

        checkOpenOrClose();


    }

    public String getHours(JSONObject days) {
        String endTimeString = "", startTimeString = "";
        String hours = "";

        try {
            JSONObject endTime = days.getJSONObject("endTime");

            endTimeString = endTime.get("time").toString().replace(" ", "") + " " + endTime.get("ind").toString().replace(" ", "");

            JSONObject startTime = days.getJSONObject("startTime");

            startTimeString = startTime.get("time").toString().replace(" ", "") + " " + startTime.get("ind").toString().replace(" ","");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        hours = startTimeString + " - " + endTimeString;

        return hours;
    }

    class AsyncTaskShopDetails extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            progressSweetAlertDialog.show();
            try {

                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("storeId", storeId));

            jsonObject = jsonParser.makeHttpRequest(Url.SHOPDETAILS, "POST", pair);

            Log.e("reg", jsonObject + "");


            try {


                success = jsonObject.getInt("success");
                message = jsonObject.getString("message");


                if (success == 1) {
                    storeDetail = jsonObject.getJSONObject("results");
                    JSONObject jsonObject2 = storeDetail.getJSONObject("storeDetails");

                   storeID = jsonObject2.getString("storeId");
                    storeName = jsonObject2.getString("storeName");
                    storeLocation = jsonObject2.getString("storeLocation");
                    storeLatitude = jsonObject2.getString("storeLatitude");
                    storeLongitude = jsonObject2.getString("storeLongitude");
                    callOptionString = jsonObject2.getString("storeActiveContact");
                    try {
                        Lat = Double.parseDouble(storeLatitude);
                        Lon = Double.parseDouble(storeLongitude);

                        setUpMapIfNeeded();
                    } catch (Exception e) {

                    }
                    storeOwnerEmail = jsonObject2.getString("storeOwnerEmail");
                    storeOwnerFirstName = jsonObject2.getString("storeOwnerFirstName");
                    if(storeOwnerFirstName.equals("null")){
                        storeOwnerFirstName ="";
                    }



                    storeOwnerLastName = jsonObject2.getString("storeOwnerLastName");

                    if(storeOwnerLastName.equals("null")){
                        storeOwnerLastName ="";
                    }

                    storeOwnerWebAddress = jsonObject2.getString("storeWebAddress");
                    stripeaccountid = jsonObject2.getString("stripeaccountid");
                    bodegaFee = jsonObject2.getString("bodegaFee");

                    storeTaxAmount = jsonObject2.getString("storeTaxAmount");
                    storeContact = jsonObject2.getString("storeContact");
                    storeLegalName = jsonObject2.getString("storeLegalName");
                    storeHour = jsonObject2.getJSONObject("storeHours");
                    storeLandLine=  jsonObject2.getString("storeLandLine");
                    storeDeliveryFee = jsonObject2.getJSONObject("storeDeliveryFess");
                    storeImages=storeDetail.getJSONObject("storeImages");

                    storeStreetAddress = jsonObject2.getString("storeStreet");
                    profilePicString = storeImages.getString("profilePic");




                    try {
                         products = storeDetail.getJSONArray("itemlist");

                    }catch (Exception e){

                        e.printStackTrace();
                    }

                    //JSONObject day = storeHour.getJSONObject("")


                    setDetails();
                    //setUpMap();

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressSweetAlertDialog.dismiss();

            if (success == 1) {

                try {

                    if (!storeLegalName.equals("")) {
                        storeNameTxt.setText(storeName);

                        sharePref.setshareprefdatastring(SharePref.BODEGA_FEE,bodegaFee);

                       // storeNameTxt.setTypeface(myTypeface);
                        storeNameTxt.setTextColor(getActivity().getResources().getColor(R.color.white_text));
                    } else {
                        storeNameTxt.setTextColor(getActivity().getResources().getColor(R.color.white_text));
                    }

                    if (!storeStreetAddress.equals("")) {
                        address.setText(storeStreetAddress);
//                        address.setTypeface(myTypeface);
                        address.setTextColor(getActivity().getResources().getColor(R.color.white_text));
                    } else {
                        address.setTextColor(getActivity().getResources().getColor(R.color.white_text));
                    }

                    if (!storeOwnerFirstName.equals("") || !storeOwnerLastName.equals("")) {
                        owmer.setText("Manager : "+storeOwnerFirstName + " " + storeOwnerLastName);

                        String s = storeOwnerFirstName + " " + storeOwnerLastName;
                        if (s.length()>10){
                            s = s.substring(0,10)+"..";
                        }
                        phoneOrder.setText("Call "+s);
//                        owmer.setTypeface(myTypeface);
                        owmer.setTextColor(getActivity().getResources().getColor(R.color.white_text));
                    } else {
                        owmer.setTextColor(getActivity().getResources().getColor(R.color.white_text));
                    }

                    mondayTime.setText(daysHour.get(0).toString());
                    tusedayTime.setText(daysHour.get(1).toString());
                    wednesdayTime.setText(daysHour.get(2).toString());
                    thursdayTime.setText(daysHour.get(3).toString());
                    fridayTime.setText(daysHour.get(4).toString());
                    saturdayTime.setText(daysHour.get(5).toString());
                    sundayTime.setText(daysHour.get(6).toString());

                    if (trueOrfalse1.equals("1")) {
                        deliveryOption1Txt.setText("Free Delivery");

                        if(trueOrfalse4.equals("1")){
                            deliveryOption2Txt.setText("Rush Delivery : $" + dOption4Value);
                        }

                    } else if(trueOrfalse2.equals("1")) {

                        deliveryOption1Txt.setText("Minimum Purchase : $"+dOption2Value);

                        if(trueOrfalse4.equals("1")){
                            deliveryOption2Txt.setText("Rush Delivery : $" + dOption4Value);
                        }

                    }else if(trueOrfalse3.equals("1")) {

                        deliveryOption1Txt.setText("Delivery Fee : $"+dOption3Value);

                        if(trueOrfalse4.equals("1")){
                            deliveryOption2Txt.setText("Rush Delivery : $" + dOption4Value);
                        }

                    }else if(trueOrfalse0.equals("1")) {

                        deliveryOption1Txt.setText("No Delivery");


                    }





                    if (Lat != 0 && Lon != 0) {


                    }

                    setData();

                    paymentTextView.setText("Payment : Cash");


                    Log.e("bodegaFee",bodegaFee);

                    setProducts();

                } catch (Exception e) {

                }

            } else {

                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                doneDialog.setTitleText("No data Found");
                doneDialog.show();
            }


        }


    }

    public void checkOpenOrClose() {
        boolean value = false;


        String weekDay;
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);

        Calendar calendar = Calendar.getInstance();
        weekDay = dayFormat.format(calendar.getTime());

        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        String time = timeFormat.format(Calendar.getInstance().getTime());

        try {
            currenttime = timeFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Log.e("time", weekDay + " " + time);


        String data = getDayData(weekDay);
        divideTime(data);


        //return value;
    }

    public void divideTime(String data){


        String[] shopTimes1 = data.split("\\ - ");
        String[] startTime1 = shopTimes1[0].split("\\ ");
        String[] endTime1 = shopTimes1[1].split("\\ ");

        Log.e("S",startTime1[0] );

//        if(startTime1[0].length()==4){
//            startTime1[0]= "0"+startTime1[0];
//        }
//        if(endTime1[0].length()==4){
//            endTime1[0]= "0"+endTime1[0];
//        }

        Log.e("jjjj",startTime1[0]+ "" +" "+endTime1[0]+" "+endTime1[1]);


        startTime = startTime1[0].replace(".",":").replace(" ","")+" "+startTime1[1];
        endTime = endTime1[0].replace(".",":").replace(" ", "") +" "+endTime1[1];
        Log.e("jjjj",startTime +" "+endTime);




        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        //	String time = timeFormat.format(Calendar.getInstance().getTime());

        Date date1 = null,date2 = null;

        try {
            date1 = timeFormat.parse(startTime);
            date2 = timeFormat.parse(endTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(currenttime.after(date1) && currenttime.before(date2)){

            open.setText("Store Open");
        }else{

            open.setText("Store Close");
        }
    }



    public String getDayData(String weekday) {

        String data="";

        Log.e("data",data);
        switch (weekday) {
            case "Sunday":

                data =daysHour.get(6).toString();

                break;
            case "Monday":
                data =daysHour.get(0).toString();

                break;
            case "Wednesday":
                data =daysHour.get(2).toString();
                break;
            case "Tuesday":
                data =daysHour.get(1).toString();

                break;

            case "Thursday":
                data =daysHour.get(3).toString();

                break;

            case "Friday":
                data =daysHour.get(4).toString();

                break;

            case "Saturday":
                data =daysHour.get(5).toString();

                break;
            default:
                break;
        }

        Log.e("data2", data);
        return data;
    }


    public void setCounter(){

      int count = db.getCartCounter(storeId);

        ((DrawerActivity)getActivity()).hideCartButton(false,count);

    }





//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }

}
