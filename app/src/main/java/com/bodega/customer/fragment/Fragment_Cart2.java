package com.bodega.customer.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.activity.LoginActivity;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.gcm.RegistrationIntentService;
import com.bodega.customer.model.CartListModel;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.SupportMapFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.bodega.customer.R.id.creditCard;


public class Fragment_Cart2 extends Fragment {


    JSONParser jsonParser;
   // Typeface myTypeface, custom;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    int success = 0;


    ImageButton forward;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    String loginStatus;

    SharePref sharePref;

    float totalProdcutPrice=0;

    String trueOrfalse1, trueOrfalse2, trueOrfalse3, trueOrfalse4,trueOrfalse0;
    boolean dOption1, dOption2, dOption3, dOption4,dOption0;
    String dOption2Value, dOption3Value, dOption4Value;


    LinearLayout delLin0,delLin1,delLin2,delLin3,delLin4;
    RelativeLayout cardLayout;

    TextView d2OptionValueTxt,d3OptionValueTxt,d4OptionValueTxt;
    CheckBox checkBox0,checkBox1,checkBox2,checkBox3,checkBox4;

    SupportMapFragment mapFragment;

    FragmentManager mFragmentManager;


    LinearLayout deliveryOptionLayout;

    ImageButton backBtn;


    TextView deliveryOption1,deliveryOption2;


    boolean isInternetOn;

    ConnectionDetector cd;



    MyDatabase db;

    CircleImageView image;

    String gcmCode;

    ListView list;

    TextView productPriceTotaltext;



    LinearLayout productlayout;


    RelativeLayout addToCart;
    String shopName;

    CartListModel cartListModel;

    TextView totalProductPriceTxt,totalPrice;

    ArrayList<CartListModel> cartListModelsArrayList;

    EditText creditFeeEditText;

    String shopID,tipsAmountString,productNameString,productPriceString,productImageString;

    Fragment fragment;

    float selectedDeliveryOptionValue=0,specialPriceValue=0,totalProcutPriceValue=0,TotalOrderPriceValue=0,TotalOrderPriceValueWithCreditFee=0,tipsAmountValue=0,creditCardFee =0;

    EditText specialOrderEdt,tipsAmount,deliveryFeeEdt;

    RadioButton cashPayment,cardPayment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_check_out2, container,
                false);



        totalProductPriceTxt = (TextView) v.findViewById(R.id.totalProdcutPrice);

        totalPrice = (TextView) v.findViewById(R.id.totalOrderPrice);


        jsonParser = new JSONParser();
        sharePref = new SharePref(getActivity());
        loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);

        String totalproductPriceString =sharePref.getshareprefdatastring(SharePref.TOTALPRODUCTPRICE) ;


                DecimalFormat df = new DecimalFormat("#.00");

        double productPricelfloat = (float) Float.parseFloat(totalproductPriceString);

        totalProductPriceTxt.setText("$"+ df.format(productPricelfloat));




        forward = (ImageButton) v.findViewById(R.id.forward);

        deliveryOptionLayout = (LinearLayout) v.findViewById(R.id.deliveryOptionLayout);
        creditFeeEditText = (EditText) v.findViewById(R.id.creditcardFee);
        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
//
//        myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRnd-Book.otf");
//        custom = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        checkBox0 = (CheckBox) v.findViewById(R.id.deliveryOption0);

        checkBox1 = (CheckBox) v. findViewById(R.id.deliveryOption1);

        checkBox2 = (CheckBox)  v.findViewById(R.id.deliveryOption2);

        checkBox3 = (CheckBox) v. findViewById(R.id.deliveryOption3);

        checkBox4 = (CheckBox)  v.findViewById(R.id.deliveryOption4);

        specialOrderEdt = (EditText) v.findViewById(R.id.specialPrice);

        tipsAmount = (EditText) v.findViewById(R.id.tipsAmount);
        gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);


        shopID = sharePref.getshareprefdatastring(SharePref.CURRENTSHOPID);


        specialOrderEdt.setText(sharePref.getshareprefdatastring(SharePref.SPECIALORDER));

        specialOrderEdt.setEnabled(false);

        Log.e("shopiD",shopID);

        fragment = new Fragment_category();


       // backBtn = (ImageButton) v.findViewById(R.id.backBtn);

        db = new MyDatabase(getActivity());

        cd = new ConnectionDetector(getActivity());

        isInternetOn = cd.isConnectingToInternet();

        cartListModelsArrayList = new ArrayList<>();

        cartListModelsArrayList = db.getCartList(shopID);

        productPriceTotaltext = (TextView) v.findViewById(R.id.totalProdcutPrice);

        delLin0 = (LinearLayout) v.findViewById(R.id.delLin0);

        delLin1 = (LinearLayout) v.findViewById(R.id.delLin1);
        delLin2 = (LinearLayout) v.findViewById(R.id.delLin2);
        delLin3 = (LinearLayout) v.findViewById(R.id.delLin3);
        delLin4 = (LinearLayout) v.findViewById(R.id.delLin4);
        cardLayout = (RelativeLayout) v.findViewById(R.id.cardLayout);

        cashPayment = (RadioButton) v.findViewById(R.id.cash);
        cardPayment = (RadioButton) v.findViewById(creditCard);

        d2OptionValueTxt = (TextView) v.findViewById(R.id.dOptionValue2);
        d3OptionValueTxt= (TextView) v.findViewById(R.id.dOptionValue3);
        d4OptionValueTxt= (TextView) v.findViewById(R.id.dOptionValue4);

       deliveryFeeEdt= (EditText) v.findViewById(R.id.deliveryFee);
        totalProcutPriceValue = Float.parseFloat(sharePref.getshareprefdatastring(SharePref.TOTALPRODUCTPRICE));


        cashPayment.setSelected(true);

        cardLayout.setVisibility(View.GONE);




        specialOrderEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {


                    try {
                        specialPriceValue = Float.parseFloat(specialOrderEdt.getText().toString());
                        getTotalPrice();
                    }catch (Exception e){

                    }
                }

            }
        });
        specialOrderEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getTotalPrice();
            }

            @Override
            public void afterTextChanged(Editable s) {
//                if (!mEditing && mEditTextView.hasFocus()) {
//                    mEditing = true;
//                }

                try {
                    specialPriceValue = Float.parseFloat(specialOrderEdt.getText().toString());
                    getTotalPrice();
                }catch (Exception e){

                }
            }
        });


        cashPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    cardLayout.setVisibility(View.GONE);
                    sharePref.setshareprefdatastring(SharePref.PAYMENTTYPE,"cash");
                    getTotalPrice();
                }

            }
        });

        cardPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    cardLayout.setVisibility(View.VISIBLE);
                    sharePref.setshareprefdatastring(SharePref.PAYMENTTYPE, "credit");
                    getTotalPrice();
                }

            }
        });


              tipsAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {


                    try {
                        tipsAmountValue = Float.parseFloat(tipsAmount.getText().toString());
                        getTotalPrice();
                    }catch (Exception e){

                    }
                }

            }
        });
        tipsAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getTotalPrice();
            }

            @Override
            public void afterTextChanged(Editable s) {
//                if (!mEditing && mEditTextView.hasFocus()) {
//                    mEditing = true;
//                }

                try {
                    tipsAmountValue = Float.parseFloat(tipsAmount.getText().toString());
                    getTotalPrice();
                }catch (Exception e){

                }
            }
        });


        sharePref.setshareprefdatastring(SharePref.PAYMENTTYPE,"cash");

        sharePref.setshareprefdatastring(SharePref.ORDERTYPE,"normal");



        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);

                gcmCode  = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);

                if(cashPayment.isChecked()||cardPayment.isChecked()) {

                    if (loginStatus.equals("yes")) {

                        String tipsString = tipsAmount.getText().toString();

                        if (tipsString.equals("")) {
                            tipsString = "0";
                        }

                        String specialString = specialOrderEdt.getText().toString();
                        if (specialString.equals("")) {
                            specialString = "0";
                        }

                        if (checkBox2.isChecked()) {

                            if (Float.parseFloat(dOption2Value) <= totalProcutPriceValue) {
                                sharePref.setshareprefdatastring(SharePref.TOTALPRODUCTPRICE, db.getCartTotal(shopID) + "");

                                sharePref.setshareprefdatastring(SharePref.TIPS, tipsString);
                                sharePref.setshareprefdatastring(SharePref.SPECIALORDER, specialString);

                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                Fragment_Cart3 llf = new Fragment_Cart3();


                                ((DrawerActivity) getActivity()).fragmentStack.lastElement().onStop();
                                ((DrawerActivity) getActivity()).fragmentStack.push(llf);

                                ft.add(R.id.frame_container, llf);
                                ft.commit();
                            } else {
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("Total amount is not sufficient to proceed ")
                                        .show();
                            }

                        } else {
                            sharePref.setshareprefdatastring(SharePref.TOTALPRODUCTPRICE, db.getCartTotal(shopID) + "");

                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            FragmentTransaction ft = fm.beginTransaction();
                            Fragment_Cart3 llf = new Fragment_Cart3();


                            ((DrawerActivity) getActivity()).fragmentStack.lastElement().onStop();
                            ((DrawerActivity) getActivity()).fragmentStack.push(llf);

                            ft.add(R.id.frame_container, llf);
                            ft.commit();
                        }

                    } else {


                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Please Login before Proceed")
                                .setConfirmText("Login")
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                getLoginPage();
                                sweetAlertDialog.dismiss();
                                //  sweetAlertDialog.dismiss();

                            }
                        }).setCancelText("Cancel").setConfirmText("Login")
                                .show();

                    }
                }else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Select a payment method")

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                            sweetAlertDialog.dismiss();
                            //  sweetAlertDialog.dismiss();

                        }
                    }).setConfirmText("OK")
                            .show();
                }
            }
        });

//        specialOrderEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus && mEditing) {
//                    mEditing = false;
//                    ///Do the thing
//                }
//            }
//        });





//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//             //   Log.i("si:",(DrawerActivity.class).fragmentStack.size()+"");
//                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();
//
//                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onPause();
//                ft.remove(((DrawerActivity) getActivity()).fragmentStack.pop());
//
//                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onResume();
//                ft.show(((DrawerActivity) getActivity()).fragmentStack.lastElement());
//                //  Log.i("bbb",fragmentStack.lastElement()+"");
//
//
//                ft.commit();
//            }
//        });
        setDeliveryOption();

        getTotalPrice();

//        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    selectedDeliveryOptionValue = Float.parseFloat("0");
//                    getTotalPrice();
//
//                    checkBox2.setEnabled(false);
//                    sharePref.setshareprefdatastring(SharePref.ORDERTYPE,"normal");
//                    checkBox3.setEnabled(false);
//                    checkBox4.setEnabled(false);
//
//                }else{
//                    checkBox2.setEnabled(true);
//
//                    checkBox3.setEnabled(true);
//                    checkBox4.setEnabled(true);
//                }
//            }
//        });
//        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    selectedDeliveryOptionValue = 0;
//                    getTotalPrice();
//                    checkBox1.setEnabled(false);
//                    sharePref.setshareprefdatastring(SharePref.ORDERTYPE,"normal");
//                    checkBox3.setEnabled(false);
//                    checkBox4.setEnabled(false);
//
//                }else{
//                    selectedDeliveryOptionValue = Float.parseFloat("0");
//                    getTotalPrice();
//                    checkBox1.setEnabled(true);
//
//                    checkBox3.setEnabled(true);
//                    checkBox4.setEnabled(true);
//                }
//            }
//        });
//        checkBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    selectedDeliveryOptionValue = Float.parseFloat(dOption3Value);
//                    getTotalPrice();
//                    checkBox2.setEnabled(false);
//                    sharePref.setshareprefdatastring(SharePref.ORDERTYPE,"normal");
//                    checkBox1.setEnabled(false);
//                    checkBox4.setEnabled(false);
//                }else{
//                    selectedDeliveryOptionValue = Float.parseFloat("0");
//                    getTotalPrice();
//                    checkBox2.setEnabled(true);
//
//                    checkBox1.setEnabled(true);
//                    checkBox4.setEnabled(true);
//                }
//            }
//        });
        checkBox4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    DecimalFormat df = new DecimalFormat("#.00");
                    selectedDeliveryOptionValue = Float.parseFloat(dOption4Value);

                    checkBox2.setEnabled(false);
                    sharePref.setshareprefdatastring(SharePref.ORDERTYPE,"rush");
                    deliveryFeeEdt.setText(""+df.format(Float.parseFloat(dOption4Value)));



                    getTotalPrice(); 
                    checkBox3.setEnabled(false);
                    checkBox1.setEnabled(false);
                }else{
                    //selectedDeliveryOptionValue = Float.parseFloat("0");
                    setDeliveryOption();
                    getTotalPrice();
                    checkBox2.setEnabled(true);

                    checkBox3.setEnabled(true);
                    checkBox1.setEnabled(true);
                }
            }
        });




        return v;
    }




    public void getLoginPage(){
        {
            if(!gcmCode.isEmpty()) {
                Intent in = new Intent(getActivity(), LoginActivity.class);


                startActivity(in);
            }else{

                if (checkPlayServices()) {
                    // Start IntentService to register this application with GCM.
                    Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
                    getActivity().startService(intent);

                }
                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                doneDialog.setTitleText("Wait for few second");
                // doneDialog.setContentText("Login unsuccessful");
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                        doneDialog.dismiss();
                    }
                });
                doneDialog.show();


            }

        }
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(getActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                getActivity().finish();
            }
            return false;
        }
        return true;
    }

    public void getTotalPrice(){
        TotalOrderPriceValue =0;

        try {
            selectedDeliveryOptionValue = Float.parseFloat(deliveryFeeEdt.getText().toString());
        }catch (Exception e){
            selectedDeliveryOptionValue = 0;
        }
        try {
            specialPriceValue = Float.parseFloat(specialOrderEdt.getText().toString());
        }catch (Exception e){
            specialPriceValue = 0;
        }
        try {
            totalProcutPriceValue = Float.parseFloat(totalProductPriceTxt.getText().toString().replace("$",""));
        }catch (Exception e){

        }

        try {
            tipsAmountValue = Float.parseFloat(tipsAmount.getText().toString());
        }catch (Exception e){
            tipsAmountValue = 0;
        }



        creditCardFee = 0;
     TotalOrderPriceValue =   selectedDeliveryOptionValue+specialPriceValue+totalProcutPriceValue+tipsAmountValue;


        Log.e("total",TotalOrderPriceValue+"");

        DecimalFormat df = new DecimalFormat("#.00");

        if(!sharePref.getshareprefdatastring(SharePref.PAYMENTTYPE).equals("cash")){

            Log.e("credit",sharePref.getshareprefdatastring(SharePref.PAYMENTTYPE)+"");
            getCreditFee();
        }

        TotalOrderPriceValueWithCreditFee = TotalOrderPriceValue+creditCardFee;

        Log.e("TotalOrderCreditFee",TotalOrderPriceValueWithCreditFee+"");
        //totalPrice.setText("$"+df.format(TotalOrderPriceValue));
        totalPrice.setText("$"+df.format(TotalOrderPriceValueWithCreditFee));

        sharePref.setshareprefdatastring(SharePref.TOTALORDERPRICEVALUE,TotalOrderPriceValueWithCreditFee+"");
        sharePref.setshareprefdatastring(SharePref.SPECIALORDER,specialPriceValue+"");
        sharePref.setshareprefdatastring(SharePref.ORDERDELIVERYVALUE,selectedDeliveryOptionValue+"");
        sharePref.setshareprefdatastring(SharePref.TIPS,tipsAmountValue+"");
        sharePref.setshareprefdatastring(SharePref.CREDITCARDFEE,creditCardFee+"");
    }


    public void getCreditFee(){
        DecimalFormat df = new DecimalFormat("#.00");
        creditCardFee=0;
        double d = ((TotalOrderPriceValue + .30 )/(1 - 0.029))-TotalOrderPriceValue;

        double roundOff = Math.round(d * 100.00) / 100.00;
        creditCardFee = (float) roundOff;
        Log.e("c",creditCardFee+"");
        creditFeeEditText.setText(""+df.format(creditCardFee)+"");

        if(creditCardFee<1){
            creditFeeEditText.setText("0"+df.format(creditCardFee)+"");
        }

    }




    @Override
    public void onResume() {
        ((DrawerActivity)getActivity()).hideTitle();
        ((DrawerActivity)getActivity()).hideTextButton(true);
        super.onResume();
    }



    public void setDeliveryOption(){


        String delDataString = sharePref.getshareprefdatastring(SharePref.CURRENTSHOPNFEES);

        JSONObject storeDeliveryFee = null;
        try {
            storeDeliveryFee = new JSONObject(delDataString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject deliveryOption4 = null;
        try {
            deliveryOption4 = storeDeliveryFee.getJSONObject("dFour");


            dOption4Value = deliveryOption4.getString("value");

            trueOrfalse4 = deliveryOption4.getString("deliveryOption");

            if (trueOrfalse4.equals("0")) {


                dOption4 = false;
            } else {
                dOption4 = true;
            }



            JSONObject deliveryOption1 = storeDeliveryFee.getJSONObject("dOne");


            trueOrfalse1 = deliveryOption1.getString("deliveryOption");

            if (trueOrfalse1.equals("0")) {

                dOption1 = false;

            } else {
                dOption1 = true;
            }


            JSONObject deliveryOption2 = storeDeliveryFee.getJSONObject("dTwo");

            dOption2Value = deliveryOption2.getString("value");


            trueOrfalse2 = deliveryOption2.getString("deliveryOption");

            if (trueOrfalse2.equals("0")) {

                dOption2 = false;

            } else {
                dOption2 = true;
            }

            JSONObject deliveryOption3 = storeDeliveryFee.getJSONObject("dThree");

            dOption3Value = deliveryOption3.getString("value");

            trueOrfalse3 = deliveryOption3.getString("deliveryOption");

            if (trueOrfalse3.equals("0")) {

                dOption3 = false;

            } else {

                dOption3 = true;
            }

            JSONObject deliveryOption0 = storeDeliveryFee.getJSONObject("dZero");



            trueOrfalse0 = deliveryOption0.getString("deliveryOption");

            if (trueOrfalse0.equals("0")) {

                dOption0 = false;

            } else {

                dOption0 = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        DecimalFormat df = new DecimalFormat("#.00");


        if(dOption0){
            delLin0.setVisibility(View.VISIBLE);
            delLin1.setVisibility(View.GONE);
            delLin2.setVisibility(View.GONE);
            delLin3.setVisibility(View.GONE);
            delLin4.setVisibility(View.GONE);
            checkBox0.setChecked(true);

            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("This Store is Not open for Delivery.")
                    .show();


        }else {
            delLin0.setVisibility(View.GONE);
        }
        if(dOption1){
            delLin1.setVisibility(View.GONE);
            delLin2.setVisibility(View.GONE);
            delLin0.setVisibility(View.GONE);
            delLin3.setVisibility(View.GONE);
            deliveryFeeEdt.setText(""+"0.00");
            selectedDeliveryOptionValue = 0.00f;

        }else{
            delLin0.setVisibility(View.GONE);
            delLin1.setVisibility(View.GONE);
            delLin2.setVisibility(View.GONE);
            delLin3.setVisibility(View.GONE);
        }

        if(dOption2){
            delLin0.setVisibility(View.GONE);
            delLin1.setVisibility(View.GONE);
            delLin2.setVisibility(View.GONE);
            delLin3.setVisibility(View.GONE);
            deliveryFeeEdt.setText(""+"0.00");
            selectedDeliveryOptionValue = 0.00f;

        }else{
            delLin0.setVisibility(View.GONE);
            delLin1.setVisibility(View.GONE);
            delLin2.setVisibility(View.GONE);
            delLin3.setVisibility(View.GONE);
        }

        if(dOption3){
            delLin0.setVisibility(View.GONE);
            delLin1.setVisibility(View.GONE);
            delLin2.setVisibility(View.GONE);
            delLin3.setVisibility(View.GONE);
            deliveryFeeEdt.setText(""+df.format(Float.parseFloat(dOption3Value)));
            selectedDeliveryOptionValue = Float.parseFloat(dOption3Value);

        }else{
            delLin0.setVisibility(View.GONE);
            delLin1.setVisibility(View.GONE);
            delLin2.setVisibility(View.GONE);
            delLin3.setVisibility(View.GONE);
        }


        if(dOption4){
            delLin0.setVisibility(View.GONE);
            delLin1.setVisibility(View.GONE);
            delLin2.setVisibility(View.GONE);
            delLin3.setVisibility(View.GONE);
            delLin4.setVisibility(View.VISIBLE);
            d4OptionValueTxt.setText("$"+df.format(Float.parseFloat(dOption4Value)));

        }else{
            delLin0.setVisibility(View.GONE);
            delLin1.setVisibility(View.GONE);
            delLin2.setVisibility(View.GONE);
            delLin3.setVisibility(View.GONE);
            delLin4.setVisibility(View.GONE);
            deliveryOptionLayout.setVisibility(View.GONE);
        }

    }




}
