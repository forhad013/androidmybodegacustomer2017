package com.bodega.customer.fragment;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.adapter.CategoryAdapter;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.model.CateogryModel;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class Fragment_category extends Fragment {

	Calendar myCalendar;

	TextView to, from, paid_t, sale_t, balance_t;



	JSONObject json;

	// Connection detector class
	ConnectionDetector cd;

	JSONParser jsonParser;

	Button addCategory;
	boolean isInternetPresent;


	SharePref sharePref;


	CateogryModel cateogryModel;




	SweetAlertDialog progressSweetAlertDialog,doneDialog;

	int success;

	String message;

	String storeID,userEmail;

	JSONObject result;
	ImageButton cancel,editBtn;

	CategoryAdapter productsAdapter;

	ArrayList<CateogryModel> cateogryModelArrayList;
	ListView catList;

	Button Addnew;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml
		View view = inflater.inflate(R.layout.fragment_category, container,
				false);

		// getActionBar().setDisplayHomeAsUpEnabled(true);
		// getActionBar().setHomeButtonEnabled(true);

		cd = new ConnectionDetector(getActivity());



		catList = (ListView) view.findViewById(R.id.catlist);



		isInternetPresent = cd.isConnectingToInternet();

		sharePref = new SharePref(getActivity());


		storeID = sharePref.getshareprefdatastring(SharePref.CURRENTSHOPID);

		userEmail = sharePref.getshareprefdatastring(SharePref.USEREMAIL);

		jsonParser = new JSONParser();



		 cateogryModelArrayList = new ArrayList<>();

		progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
		progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
		progressSweetAlertDialog.setTitleText("Loading");
		progressSweetAlertDialog.setCancelable(false);

		if(isInternetPresent){

			new AsyncTaskProducts().execute();
		}



		return view;
	}


	@Override
	public void onResume() {
		if(isInternetPresent){

			//new AsyncTaskProducts().execute();
		}
		((DrawerActivity)getActivity()).hideTextButton(true);
		super.onResume();
	}

	class AsyncTaskProducts extends AsyncTask<String, String, String> {


		@Override
		protected void onPostExecute(String result) {
			if (success == 0) {




				try {




					progressSweetAlertDialog.dismiss();

					CategoryAdapter categoryAdapter = new CategoryAdapter(getActivity(),cateogryModelArrayList);
					catList.setAdapter(categoryAdapter);

				//	setData();



				}catch (Exception e){

				}

			} else
			{
				doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);

				doneDialog.setTitleText(message);

				doneDialog.show();
			}


			progressSweetAlertDialog.dismiss();

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> pair = new ArrayList<NameValuePair>();



			cateogryModelArrayList.clear();
			pair.add(new BasicNameValuePair("storeid", storeID));

			json = jsonParser.makeHttpRequest(Url.CATEGORY, "POST", pair);

			Log.e("reg", json + "");



			try {

				success = json.getInt("error");
				message = json.getString("message");

				if(success==0) {




					JSONArray prouductsArray = new JSONArray();

					prouductsArray   = json.getJSONArray("records");

					for (int i = 0; i < prouductsArray.length(); i++) {



						JSONObject item = prouductsArray.getJSONObject(i);
						String catID="", catTitle="",status="";

						catID = item.getString("id");
						catTitle = item.getString("title");
						status = item.getString("status");




						cateogryModel = new CateogryModel(catID,catTitle,status);


						cateogryModelArrayList.add(cateogryModel);

					}
				}




			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPreExecute() {

			try {

				Log.e("asd","asdasd");
				progressSweetAlertDialog.show();

				//   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
			} catch (IndexOutOfBoundsException e) {
				Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT).show();
			}

		}

	}










//
//	class AsyncTaskUpdate extends AsyncTask<String, String, String> {
//
//		int success = 5, mailcheck = 5;
//
//		HttpResponse response;
//		JSONObject response1;
//		String responseBody;
//		Boolean availableProduct, availablemail;
//
//		@Override
//		protected String doInBackground(String... params) {
//			// TODO Auto-generated method stub
//			{
//
//				HttpClient httpClient = new DefaultHttpClient();
//				HttpPost httpPost = new HttpPost(
//						Url.PRODUCT_UPDATE);
//
//				httpPost.setHeader("content-type", "application/json");
//				httpPost.setHeader("Accept", "application/json");
//				//  JSONObject jsonObject1 = new JSONObject();
//				JSONObject jsonObject = new JSONObject();
//
//
//				ArrayList<JsonModelBasicInventoryUpdate> products = new ArrayList<>();
//
//				JSONArray jsonArray = new JSONArray();
//
//				JSONObject main = new JSONObject();
//
//
//				//     setDeliveryOption();
//
//
//
//
//
//				try{
//
//
//
//					for(int i=0;i<14;i++) {
//						JsonModelBasicInventoryUpdate product = new JsonModelBasicInventoryUpdate();
//						//product.inventory_id = deliveryOptionArrayList.get(i).optionName;
//
//						product.inventory_item_name = productsModelArrayList.get(i).getProName();
//						product.inventory_price = productsModelArrayList.get(i).getProPrice();
//						product.inventory_status = productsModelArrayList.get(i).getStatus();
//
//
//						products.add(product);
//					}
//
//
//
//						for (int i=0;i < products.size();i++){
//							jsonArray.put(products.get(i).getJSONObject());
//						}
//						//Log.e("su",json_submodel.PID[0]+"");
//
//
//					try {
//						main.accumulate("products",jsonArray);
//
//				 	main.accumulate("userEmail",userEmail);
//
//					} catch (JSONException e) {
//						e.printStackTrace();
//					}
//
//
//
//				}
//				catch (Exception e) {
////
////                    Log.d("InputStream", e.getLocalizedMessage());
//
//				}
//
//				StringEntity entity = null;
//
//
//
//
//				try {
//					entity = new StringEntity( main.toString());
//					entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
//					// Log.d("ch",entity+"");
//
//			 	Log.e("res",main.toString()+"");
//
//				} catch (UnsupportedEncodingException e) {
//					e.printStackTrace();
//				}
//				httpPost.setEntity(entity);
//
//				try {
//					//  response = httpClient.execute(httpPost);
////                    resC++;
////                    Log.e("Value of response count: ", resC + "");
//					ResponseHandler<String> responseHandler=new BasicResponseHandler();
//					responseBody =httpClient.execute(httpPost, responseHandler);
//					//response1=new JSONObject(responseBody);
//					Log.e("res", responseBody + "");
//
//
//
//
//
//				} catch (IOException e) {
//					e.printStackTrace();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//			return null;
//		}
//
//
//		@Override
//		protected void onPostExecute(String string) {
//
//			progressSweetAlertDialog.dismiss();
//
////			int success;
////			String msg = "";
////			try {
////				success =  response1.getInt("success");
////
////				msg = response1.getString("message");
////
////				if(success==1){
////
////
////					doneDialog=  new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
////					doneDialog.setTitleText("Success");
////					doneDialog.setContentText(msg);
////					doneDialog.show();
////					doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
////						@Override
////						public void onClick(SweetAlertDialog sweetAlertDialog) {
////
////							doneDialog.dismiss();
////
////							enableAll(false);
////							update.setVisibility(View.INVISIBLE);
////
////						}
////					});
////
////
////				}else if(success ==0){
////
////					//getError(response1);
////
////
////
////					doneDialog=  new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE);
////
////
////					doneDialog.setContentText(message);
////					doneDialog.show();
////					doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
////						@Override
////						public void onClick(SweetAlertDialog sweetAlertDialog) {
////							sweetAlertDialog.dismiss();
////						}
////					});
////
////				}
////
////
////
////			}catch (JSONException e) {
////				e.printStackTrace();
////			}
//
//
//			update.setEnabled(true);
//
//
//
//		}
//
//
//
//		@Override
//		protected void onPreExecute() {
//
//			progressSweetAlertDialog.show();
//			update.setEnabled(false);
//
//		}
//
//	}

}