package com.bodega.customer.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.model.ShopListModel;
import com.bodega.customer.utils.ClusterMarkerLocation;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.OwnIconRender;
import com.bodega.customer.utils.SharePref;
import com.bodega.customer.utils.TrackGPS;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.halcyon.squareprogressbar.SquareProgressBar;
import cn.pedant.SweetAlert.SweetAlertDialog;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class Map_fragment_main_permission extends Fragment implements

        ClusterManager.OnClusterClickListener<ClusterMarkerLocation>,
        ClusterManager.OnClusterInfoWindowClickListener<ClusterMarkerLocation>,
        ClusterManager.OnClusterItemClickListener<ClusterMarkerLocation>,
        ClusterManager.OnClusterItemInfoWindowClickListener<ClusterMarkerLocation> {

    // LogCat tag
    private static final String TAG = "MAP";
    CountDownTimer coundown;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    SquareProgressBar squareProgressBar;
    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    // UI elements
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    LocationManager locationManager;

    String provider;
    int count = 0;
    ImageButton profile;
    TextView topTxt;
    Marker m1;

    int success = 0;
    private ClusterMarkerLocation clusterMarkerLocation;

    private Cluster<ClusterMarkerLocation> clickedCluster;

    ArrayList shopIDList;

    HashMap<String, Integer> mapStatus;
    HashMap<String, Integer> hashTemp;

    RelativeLayout tutorial;
    Location location;

    Context context;

    JSONParser jsonParser;

    ArrayList<Marker> markers;

    JSONObject json;

    String msg, message;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    ClusterManager<ClusterMarkerLocation> clusterManager;
    String shopContact, shopID;

    Typeface mytTypeface, customFont;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    public ArrayList<ShopListModel> shopList;
    ShopListModel shopListModel;

    Map<String, String> shopMap = new HashMap<String, String>();

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    TextView titleText;

    double lat, lon;

    TrackGPS gps;

    String firstTime;
    LatLng firstPoint = null;
    LatLng secondPoint = null;

    float distanceInMeter;

    LatLng currentLoc;
    TextView distanceInfo;
    ArrayList<LatLng> trackList;
    double tempLat = 40.733979, tempLon = -73.988894;

    String loginStatus;

    Button gotIt;
    SharePref sharePref;

    boolean isInternetOn = false;
    ConnectionDetector cd;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_maps, container, false);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        mMap = mapFragment.getMap();

        shopIDList = new ArrayList();

        mapStatus = new HashMap<>();


        sharePref = new SharePref(getActivity());

        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        context = getActivity();
        //firstTime = sharePref.getshareprefdatastring(SharePref.FIRSTTIME);

        markers = new ArrayList<>();
        shopList = new ArrayList<>();

        jsonParser = new JSONParser();
        loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);

        clusterManager = new ClusterManager<ClusterMarkerLocation>(getActivity(), mMap);

        // profile.setVisibility(View.INVISIBLE);
        normalDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
        squareProgressBar = (SquareProgressBar) rootView.findViewById(R.id.sprogressbar);
        squareProgressBar.setImage(R.drawable.ic_progress);
        squareProgressBar.setProgress(50.0);
        cd = new ConnectionDetector(getActivity());

        isInternetOn = cd.isConnectingToInternet();

        if (isInternetOn || checkPlayServices()) {
            Map_fragment_main_permissionPermissionsDispatcher.initLocationWithCheck(this);

        } else {
            // Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Location can't be retrieved, check your gps and internet connection")
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

//                                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
//
//
//                                startActivity(intent);
                            getActivity().finish();

                            sweetAlertDialog.dismiss();
                        }
                    }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {


                    sweetAlertDialog.dismiss();

                }
            }).setCancelText("Cancel").setConfirmText("Restart this page")
                    .show();

        }

//        clusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<ClusterMarkerLocation>() {
//            @Override
//            public boolean onClusterClick(Cluster<ClusterMarkerLocation> cluster) {
//
//                String shopname = cluster.getItems().iterator().next().getShopName()+"";
//
//                Log.e("sh",shopname);
//
//                if(!shopname.equals("null")) {
//
//                    String info = cluster.getItems().iterator().next().getShopInfo();
//                    String shopID1 = shopMap.get(shopname);
//
//
//                    if (mapStatus.get(shopID1) == 0) {
//
//                        cluster.getItems().iterator().next().getShopName().showInfoWindow();
//                        setOtherMarker(shopID1);
//                        mapStatus.put(shopID1, 1);
//
//                    } else {
//                        if (!shopname.equals("You are here")) {
//                            marker.hideInfoWindow();
//                            String shopName = marker.getTitle();
//                            String shopID = shopMap.get(shopName);
//                            sharePref.setshareprefdatastring(SharePref.CURRENTSHOPID, shopID);
//
//                            ((DrawerActivity) getActivity()).displayView(6);
//
//                        }
//                    }
//                }
//
//
//                return false;
//            }
//        });


        //  LOOP(location);
//
//        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
//            @Override
//            public void onInfoWindowClick(Marker marker) {
//
//
//                String shopname = marker.getTitle();
//
//                String info = marker.getSnippet();
//
//
//                if (!shopname.equals("You are here")) {
//
//                    String shopName = marker.getTitle();
//                    String shopID = shopMap.get(shopName);
//                    sharePref.setshareprefdatastring(SharePref.CURRENTSHOPID, shopID);
//
//                    ((DrawerActivity) getActivity()).displayView(6);
//
//                }
//            }
//        });


        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {

                String s = marker.getTitle() + "";

                //  Log.e("s",s);
                if (s.equals("null")) {
                    return null;
                } else {


                    View myContentView = getActivity().getLayoutInflater().inflate(
                            R.layout.custom_marker, null);
                    TextView tvTitle = ((TextView) myContentView
                            .findViewById(R.id.snippet));
                    tvTitle.setText(marker.getTitle());
                    TextView tvSnippet = ((TextView) myContentView
                            .findViewById(R.id.title));
                    tvSnippet.setText(marker.getSnippet());
                    return myContentView;
                }
            }

            @Override
            public View getInfoContents(Marker marker) {

                return null;
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            if (isInternetOn)
                mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        ((DrawerActivity) getActivity()).hideTextButton(true);
        ((DrawerActivity) getActivity()).hideCartButton(true, 0);
        super.onResume();

//		checkPlayServices();
//
//		// Resuming the periodic location updates
//		if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
//			startLocationUpdates();
//		}
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //stopLocationUpdates();
    }

    /**
     * Method to display the location on UI
     */
    private void displayLocation() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            tempLat = mLastLocation.getLatitude();
            tempLon = mLastLocation.getLongitude();


            setUpMap();

            //	Toast.makeText(getActivity(),latitude + ", " + longitude ,Toast.LENGTH_SHORT);

        } else {

//			if(!checkLocationPermission()) {
//
//				ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//			}
            setLocation(tempLat, tempLon);

            new AsyncTaskEdit().execute();
        }
    }


    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }




    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    void initLocation() {
        gps = new TrackGPS(context);


        if(gps.canGetLocation()){


            tempLat = gps.getLongitude();
            tempLon = gps .getLatitude();

            Log.e("tempLat",tempLat+"");
            Log.e("tempLon",tempLon+"");

            if(tempLat!=0 || tempLon!=0) {
                setUpMap();
            }else{
                initLocation();
            }
            // showSuggestionBox();

            // Toast.makeText(getApplicationContext(),"Longitude:"+Double.toString(longitude)+"\nLatitude:"+Double.toString(latitude),Toast.LENGTH_SHORT).show();
        }
        else
        {

            gps.showSettingsAlert();
        }
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void onFineLocationDenied() {
        Toast.makeText(context,"Permission Denied", Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    void onFineLocationNeverAskAgain() {
        Toast.makeText(context, "Permission Denied Forever", Toast.LENGTH_SHORT).show();
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForFineLocation(PermissionRequest request) {
        showRationaleDialog(request);
    }

    private void showRationaleDialog( final PermissionRequest request) {
        new AlertDialog.Builder(context)
                .setPositiveButton(R.string.map_button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.map_button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .setCancelable(false)
                .setMessage("Need permission for GPS" )
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Map_fragment_main_permissionPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
}


    /**
     * Method to toggle periodic location updates
     * */


    /**
     * Creating google api client object
     */


    /**
     * Creating location request object
     */


    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getActivity(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();


            }
            return false;
        }
        return true;
    }

    /**
     * Starting the location updates
     */
//    protected void startLocationUpdates() {
//
//        LocationServices.FusedLocationApi.requestLocationUpdates(
//                mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);
//
//    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
    }


    public void recheckMapHASH() {

        mapStatus = hashTemp;

    }

    public void setOtherMarker(String shopID) {

        for (int i = 0; i < mapStatus.size(); i++) {

            String temp = shopIDList.get(i).toString();
            if (!temp.equals(shopID)) {
                mapStatus.put(temp, 0);
            }
        }

    }

    public void errorDialog() {

        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Oops...")
                .setContentText("Location can't be retrieved, check your gps")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

//                                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
//
//
//                                startActivity(intent);
                        getActivity().finish();

                        sweetAlertDialog.dismiss();
                    }
                }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {


                sweetAlertDialog.dismiss();

            }
        }).setCancelText("Cancel").setConfirmText("Restart this page")
                .show();
    }


    public void setProgress() {

        squareProgressBar.setVisibility(View.VISIBLE);

        final int[] progressAmount = {0};
        squareProgressBar.setProgress((double) progressAmount[0]);

        coundown = new CountDownTimer(30000, 100) {

            public void onTick(long millisUntilFinished) {

                progressAmount[0] = progressAmount[0] + 10;

                squareProgressBar.setProgress((double) progressAmount[0]);

                if (progressAmount[0] == 100) {
                    progressAmount[0] = 0;
                }
            }

            public void onFinish() {

            }
        }.start();
    }

    public void stopProgressbar() {


    }


    public void setUpMap() {


        // progressSweetAlertDialog.show();

        squareProgressBar.setVisibility(View.VISIBLE);

//        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
////            List<String> permissionsNeeded = new ArrayList<String>();
////
////            final List<String> permissionsList = new ArrayList<String>();
////            if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
////                permissionsNeeded.add("GPS Fine Location");
////            if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
////                permissionsNeeded.add("GPS Coarse Location");
////
////            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
////                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
//
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        mMap.getUiSettings().setMapToolbarEnabled(false);

        MarkerOptions markerOptions = new MarkerOptions();


        // markerOptions.title("Ahmedabad Cordinat Found here");

        // Marker m = mMap.addMarker(markerOptions);



        setLocation(tempLat, tempLon);

        new AsyncTaskEdit().execute();

    }


    public void setLocation(double lat, double lon) {

        LatLng loc = new LatLng(lat, lon);

        Log.e("lat",lat+"");
        Log.e("lon",lon+"");


        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 16.0f));

    }

    @Override
    public boolean onClusterClick(Cluster<ClusterMarkerLocation> cluster) {

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    cluster.getPosition(), (float) Math.floor(mMap
                            .getCameraPosition().zoom + 1)), 300,
                    null);
            return true;



//        Log.e("info","clusterCLieck");
//        return false;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<ClusterMarkerLocation> cluster) {
        Log.e("info","clusterCLieck");
    }

    @Override
    public boolean onClusterItemClick(ClusterMarkerLocation clusterMarkerLocation) {

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                clusterMarkerLocation.getPosition(), (float) Math.floor(mMap
                        .getCameraPosition().zoom + 1)), 300,
                null);
        return true;

    }

    @Override
    public void onClusterItemInfoWindowClick(ClusterMarkerLocation clusterMarkerLocation) {

        String shopname = clusterMarkerLocation.getShopName()+"";

                Log.e("sh",shopname);

                if(!shopname.equals("null")) {

                    String info =  clusterMarkerLocation.getShopInfo();

                    String shopID1 = shopMap.get(shopname);



                        if (!shopname.equals("You are here")) {

                            String shopName =shopname;
                            String shopID = shopMap.get(shopName);
                            sharePref.setshareprefdatastring(SharePref.CURRENTSHOPID, shopID);

                            ((DrawerActivity) getActivity()).displayView(6);

                        }

                }


        Log.e("info","clusterCLieck");

    }


    class AsyncTaskEdit extends AsyncTask<String, String, String> {


        @Override
        protected void onPostExecute(String result) {
            if (success == 1) {


                try {


                    setShopLocations();

                    squareProgressBar.setVisibility(View.INVISIBLE);
                    stopProgressbar();
                   //progressSweetAlertDialog.dismiss();


                } catch (Exception e) {

                }

            } else {
                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                doneDialog.setContentText(message);
                doneDialog.show();
            }


            progressSweetAlertDialog.dismiss();

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("myLatitude", tempLat + ""));

            pair.add(new BasicNameValuePair("myLongitude", tempLon + ""));


            Log.e("url", Url.NEARSETSHOP);

            json = null;

            json = jsonParser.makeHttpRequest(Url.NEARSETSHOP, "POST", pair);

            Log.e("edit", pair + "");


            try {

                success = json.getInt("success");


                message = json.getString("message");


                JSONObject results = json.getJSONObject("results");
                if (success == 1) {
                    shopList.clear();

                    JSONArray jsonArray = results.getJSONArray("shopList");


                    for (int i = 0; i < jsonArray.length(); i++) {


                        JSONObject object = jsonArray.getJSONObject(i);

                        String shopID = object.getString("storeId");
                        String shopName = object.getString("storeName");

                        String shopLocation = object.getString("storeStreet");
                        String shopLatitude = object.getString("storeLatitude");
                        String shopLongitude = object.getString("storeLongitude");
                        if (!shopLatitude.equals("") || !shopLongitude.equals("")) {

                            //Log.e("asoda", Double.parseDouble(shopLatitude) + "");
                            double tempLat = 0, tempLon = 0;
                            try {
                                tempLat = Double.parseDouble(shopLatitude);


                                tempLon = Double.parseDouble(shopLongitude);
                            } catch (Exception e) {

                            }



                            shopListModel = new ShopListModel(shopID, shopName, shopContact, shopLocation, tempLat, tempLon);


                            mapStatus.put(shopID, 0);
                            shopList.add(shopListModel);
                        }


                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

            try {

                //Log.e("asd", "asdasd");
                // progressSweetAlertDialog.show();

                setProgress();
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

    }


    public void setShopLocations() {

        mMap.setOnCameraChangeListener(clusterManager);

        mMap.clear();
        clusterManager.clearItems();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(tempLat, tempLon), 16.0f));
        for (int i = 0; i < shopList.size(); i++) {


            String temp = "m" + i + 1;

            Marker marker = null;

            String shopID = shopList.get(i).getShopID();

            String shopName = shopList.get(i).getShopName();

            String shopAddress = shopList.get(i).getShopLocation();

            double shopLat = shopList.get(i).getShopLatitude();

            double shopLon = shopList.get(i).getShopLongitude();


            shopIDList.add(shopID);
            shopMap.put(shopName, shopID);

            //Log.e("shopName",shopName);


            LatLng loc = new LatLng(shopLat, shopLon);
            clusterManager.addItem( new ClusterMarkerLocation( new LatLng( shopLat, shopLon),shopName,shopAddress) );
            

            //Log.e("loc",loc+"");

//            if (shopLat != 0 || shopLon != 0) {
//
//                //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 16.0f));
//                marker = mMap.addMarker(new MarkerOptions().position(new LatLng(shopLat, shopLon)).title(shopName).snippet(shopAddress));
//                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_shop));
//                marker.showInfoWindow();
//
//                markers.add(marker);
//            }
        }

        clusterManager.setRenderer(new OwnIconRender(getActivity(),mMap,clusterManager));
        clusterManager.getMarkerCollection().setOnInfoWindowAdapter(
                new MyCustomAdapterForItems());

        mMap.setOnInfoWindowClickListener(clusterManager);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gps.getLatitude(), gps.getLongitude()), 16.0f));
        clusterManager.setOnClusterClickListener(this);
        clusterManager.setOnClusterInfoWindowClickListener(this);
        clusterManager.setOnClusterItemClickListener(this);
        clusterManager.setOnClusterItemInfoWindowClickListener(this);
        hashTemp = mapStatus;


    }

    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyCustomAdapterForItems() {

            myContentsView = getActivity().getLayoutInflater().inflate(
                    R.layout.custom_marker, null);
        }
        @Override
        public View getInfoWindow(Marker marker) {

            Log.e("test","adapter");

            if(marker.getTitle().equals("")){

                return null;
            }else {

                TextView tvTitle = ((TextView) myContentsView
                        .findViewById(R.id.title));
                TextView tvSnippet = ((TextView) myContentsView
                        .findViewById(R.id.snippet));

                tvTitle.setText(marker.getTitle());
                tvSnippet.setText(marker.getSnippet());

                return myContentsView;
            }
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }
}


