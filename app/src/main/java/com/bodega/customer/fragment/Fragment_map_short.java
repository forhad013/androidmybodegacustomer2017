package com.bodega.customer.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.model.ShopListModel;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Fragment_map_short extends Fragment     {

	private GoogleMap mMap;
	SupportMapFragment mapFragment;
	LocationManager locationManager;

	String provider;

	ImageButton profile;
	TextView topTxt;
	Marker m1;

	int success = 0;

	RelativeLayout tutorial;
	Location location;

	JSONParser jsonParser;

	ArrayList<Marker> markers;

	JSONObject json;

	String msg, message;




	String shopContact, shopID;

	Typeface mytTypeface, customFont;

	SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

	public ArrayList<ShopListModel> shopList;
	ShopListModel shopListModel;

	Map<String, String> shopMap = new HashMap<String, String>();



	TextView titleText;

	double lat, lon;

	String firstTime;
	LatLng firstPoint = null;
	LatLng secondPoint = null;

	float distanceInMeter;

	LatLng currentLoc;
	TextView distanceInfo;
	ArrayList<LatLng> trackList;
	double Lat, Lon;

	String loginStatus;

	Button gotIt;
	SharePref sharePref;

	ConnectionDetector cd;

	boolean isInternetON;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_maps, container, false);
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.



		Bundle bundle = getArguments();

		lat = bundle.getDouble("lat");

		lon = bundle.getDouble("lon");



		cd  = new ConnectionDetector(getActivity());

		isInternetON = cd.isConnectingToInternet();


		mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

		mMap = mapFragment.getMap();



		locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
		Criteria criteria = new Criteria();


//		profile = (ImageButton) rootView.findViewById(R.id.profile);
//
//
//		topTxt = (TextView) rootView.findViewById(R.id.barText);
//
//
//		tutorial = (RelativeLayout) rootView.findViewById(R.id.tutorial);

	//	gotIt = (Button) rootView.findViewById(R.id.gotIT);
		mytTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoCondensed-Regular.ttf");

		customFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRnd-Book.otf");

		sharePref = new SharePref(getActivity());
		provider = locationManager.getBestProvider(criteria, false);

		progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
		progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
		progressSweetAlertDialog.setTitleText("Loading");
		progressSweetAlertDialog.setCancelable(false);


	//	firstTime = sharePref.getshareprefdatastring(SharePref.FIRSTTIME);

		markers = new ArrayList<>();
		shopList = new ArrayList<>();

		jsonParser = new JSONParser();
		loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);


		// profile.setVisibility(View.INVISIBLE);
		normalDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);

		providerCheck();


		return rootView;
	}

	public void providerCheck(){


		if(isInternetON) {
			setUpMap();

		}else{
			Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

		}


	}


	public void setUpMap() {




		mMap.setMyLocationEnabled(true);

		MarkerOptions markerOptions = new MarkerOptions();




		mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker marker) {
				return false;
			}
		});



		setLocation(lat, lon);



	}










	public void setLocation(double lat, double lon) {



		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 15.0f));
 	 	m1 = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
	 	m1.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_shop));

	}






}