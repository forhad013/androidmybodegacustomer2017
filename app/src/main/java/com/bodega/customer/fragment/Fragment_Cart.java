package com.bodega.customer.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.model.CartListModel;
import com.bodega.customer.model.JsonModelProducts;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.FontChangeCrawler;
import com.bodega.customer.utils.SharePref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class Fragment_Cart extends Fragment {



    JSONParser jsonParser;
   // Typeface myTypeface, custom;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    int success = 0;

    LinearLayout emptyCart;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    String loginStatus;

    SharePref sharePref;

    float totalProdcutPrice=0;
    float totalProdcutPriceWithSpecial=0;

    String trueOrfalse1, trueOrfalse2, trueOrfalse3, trueOrfalse4,trueOrfalse0;
    boolean dOption1= false, dOption2= false, dOption3= false, dOption4= false,dOption0= false;
    String dOption2Value, dOption3Value, dOption4Value;

    DecimalFormat df = new DecimalFormat("#.00");
    ImageButton backBtn,forward;


    TextView deliveryOption1,deliveryOption2;


    boolean isInternetOn;

    ConnectionDetector cd;


    EditText specialOrderEdt;

    MyDatabase db;

    CircleImageView image;

    String gcmCode;

    ListView list;

    TextView productPriceTotaltext;


    JSONArray productArray;

    LinearLayout productlayout;


    RelativeLayout addToCart;
    String shopName;

    CartListModel cartListModel;

    ArrayList<CartListModel> cartListModelsArrayList;

    String shopID,productIdString,productNameString,productPriceString,productImageString;

    Fragment fragment;

    Float specialPriceValue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_check_out, container,
                false);

        list = (ListView) v.findViewById(R.id.list);

        jsonParser = new JSONParser();
        sharePref = new SharePref(getActivity());
        loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);

        emptyCart = (LinearLayout) v.findViewById(R.id.empty);

        forward = (ImageButton) v.findViewById(R.id.forward);

        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
//
//        myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRnd-Book.otf");
//        custom = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoCondensed-Regular.ttf");


        specialOrderEdt = (EditText) v.findViewById(R.id.specialPrice);
        gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);


        shopID = sharePref.getshareprefdatastring(SharePref.CURRENTSHOPID);


        Log.e("shopiD",shopID);

        fragment = new Fragment_category();



       // backBtn = (ImageButton) v.findViewById(R.id.backBtn);

        db = new MyDatabase(getActivity());

        cd = new ConnectionDetector(getActivity());

        isInternetOn = cd.isConnectingToInternet();

        cartListModelsArrayList = new ArrayList<>();

        cartListModelsArrayList = db.getCartList(shopID);

        productPriceTotaltext = (TextView) v.findViewById(R.id.totalProdcutPrice);


        Adapter Adapter = new Adapter(getActivity(),cartListModelsArrayList);

        list.setAdapter(Adapter);



        specialOrderEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {


                    try {
                        specialPriceValue = Float.parseFloat(specialOrderEdt.getText().toString());
                        getTotalPrice();
                    }catch (Exception e){
                        specialPriceValue = (float)0;
                        getTotalPrice();
                    }
                }

            }
        });
        specialOrderEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getTotalPrice();
            }

            @Override
            public void afterTextChanged(Editable s) {
//                if (!mEditing && mEditTextView.hasFocus()) {
//                    mEditing = true;
//                }

                try {
                    specialPriceValue = Float.parseFloat(specialOrderEdt.getText().toString());
                    getTotalPrice();
                }catch (Exception e){
                    specialPriceValue = (float)0;
                    getTotalPrice();
                }
            }
        });



        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
//                if(cartListModelsArrayList.size()!=0) {


                    if ((dOption2 && (Float.parseFloat(dOption2Value) > totalProdcutPriceWithSpecial))) {


                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Minimum Purchase for Delivery: $" +dOption2Value)
                                .show();


                    } else {




                        if (!dOption0) {


                            setProductArray();
                            float total;

                            try {
                                  total = db.getCartTotal(shopID);
                            }catch (Exception e){
                                total = 0;
                            }

                            sharePref.setshareprefdatastring(SharePref.TOTALPRODUCTPRICE,total + "");
                            sharePref.setshareprefdatastring(SharePref.SPECIALORDER, specialPriceValue+ "");

                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            FragmentTransaction ft = fm.beginTransaction();
                            Fragment_Cart2 llf = new Fragment_Cart2();


                            ((DrawerActivity) getActivity()).fragmentStack.lastElement().onStop();
                            ((DrawerActivity) getActivity()).fragmentStack.push(llf);

                            ft.add(R.id.frame_container, llf);
                            ft.commit();
                        } else {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText("This Store Is Not Set For Delivery")
                                    .show();
                        }

                    }
//                }else{
//                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
//                            .setTitleText("Oops...")
//                            .setContentText("Cart is empty")
//                            .show();
//                }


            }
        });

        if(cartListModelsArrayList.size()==0){
            emptyCart.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
        }








//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//             //   Log.i("si:",(DrawerActivity.class).fragmentStack.size()+"");
//                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();
//
//                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onPause();
//                ft.remove(((DrawerActivity) getActivity()).fragmentStack.pop());
//
//                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onResume();
//                ft.show(((DrawerActivity) getActivity()).fragmentStack.lastElement());
//                //  Log.i("bbb",fragmentStack.lastElement()+"");
//
//
//                ft.commit();
//            }
//        });

         setDeliveryOption();


        return v;
    }

    public void setCart(){

        cartListModelsArrayList = new ArrayList<>();

        cartListModelsArrayList = db.getCartList(shopID);


//        Adapter Adapter = new Adapter(getActivity(),cartListModelsArrayList,Fragment_Cart.this);
//
//        list.setAdapter(Adapter);

    }




    public  void getTotalPrice(){

        float total;

        try {
            total = db.getCartTotal(shopID);
        }catch (Exception e){
            total = 0;
        }
        Log.e("t",total+"");


        try {
            specialPriceValue = Float.parseFloat(specialOrderEdt.getText().toString());

        }catch (Exception e){
            specialPriceValue = (float)0;

        }

        Log.e("s",specialPriceValue+"");

        Log.e("tot",(specialPriceValue+total)+"");

        totalProdcutPriceWithSpecial = total +specialPriceValue;

        DecimalFormat df = new DecimalFormat("#.00");

        productPriceTotaltext.setText("Total Price : $"+df.format(totalProdcutPriceWithSpecial));


    }

    public void setDeliveryOption(){


        String delDataString = sharePref.getshareprefdatastring(SharePref.CURRENTSHOPNFEES);

        JSONObject storeDeliveryFee = null;
        try {
            storeDeliveryFee = new JSONObject(delDataString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject deliveryOption4 = null;
        try {
            deliveryOption4 = storeDeliveryFee.getJSONObject("dFour");


            dOption4Value = deliveryOption4.getString("value");

            trueOrfalse4 = deliveryOption4.getString("deliveryOption");

            if (trueOrfalse4.equals("0")) {


                dOption4 = false;
            } else {
                dOption4 = true;
            }



            JSONObject deliveryOption1 = storeDeliveryFee.getJSONObject("dOne");


            trueOrfalse1 = deliveryOption1.getString("deliveryOption");

            if (trueOrfalse1.equals("0")) {

                dOption1 = false;

            } else {
                dOption1 = true;
            }


            JSONObject deliveryOption2 = storeDeliveryFee.getJSONObject("dTwo");

            dOption2Value = deliveryOption2.getString("value");


            trueOrfalse2 = deliveryOption2.getString("deliveryOption");

            if (trueOrfalse2.equals("0")) {

                dOption2 = false;

            } else {
                dOption2 = true;
            }

            JSONObject deliveryOption3 = storeDeliveryFee.getJSONObject("dThree");

            dOption3Value = deliveryOption4.getString("value");

            trueOrfalse3 = deliveryOption3.getString("deliveryOption");

            if (trueOrfalse3.equals("0")) {

                dOption3 = false;

            } else {

                dOption3 = true;
            }

            JSONObject deliveryOption0 = storeDeliveryFee.getJSONObject("dZero");



            trueOrfalse0 = deliveryOption0.getString("deliveryOption");

            if (trueOrfalse0.equals("0")) {

                dOption0 = false;

            } else {

                dOption0 = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }




    }

    public void setProductArray(){

        productArray = new JSONArray();

        cartListModelsArrayList = db.getCartList(shopID);


        for(int i=0;i<cartListModelsArrayList.size();i++){


            JSONObject item = new JSONObject();
            ArrayList<JsonModelProducts> jsonProductList = new ArrayList<>();

            JsonModelProducts product = new JsonModelProducts();

            float x = Float.parseFloat(cartListModelsArrayList.get(i).getQuantity())* Float.parseFloat(cartListModelsArrayList.get(i).getProductPrice());

            product.productID=cartListModelsArrayList.get(i).getProductID();
            product.quantity=cartListModelsArrayList.get(i).getQuantity();
            product.totalPrice=x+"";

            jsonProductList.add(product);
            productArray.put(jsonProductList.get(0).getJSONObject());


        }


        sharePref.setshareprefdatastring(SharePref.ORDERDETAILS,productArray.toString());


    }




    @Override
    public void onResume() {
        ((DrawerActivity)getActivity()).hideTitle();
        ((DrawerActivity)getActivity()).hideTextButton(true);
        super.onResume();
    }


    public class Adapter extends BaseAdapter {


        ArrayList<CartListModel> cartListModelsArrayList1;
        String videoLink;

        private Context context;

        TextView title1, details;
        int success;
        String CID, vote;
        SharePref sharePref;
        String currentUserID;
        int voteNumber;
        TextView vote1;


        FontChangeCrawler fontChanger;
        boolean isInternetPresent;

        MyDatabase db;

        Fragment fragment;

        public Adapter(Context context,
                           ArrayList<CartListModel> cartListModelsArrayList2) {


            // TODO Auto-generated constructor stub
            this.context = context;

            this.cartListModelsArrayList1 = cartListModelsArrayList2;

            sharePref = new SharePref(context);

            db = new MyDatabase(context);


            // currentUserID = sharePref.getshareprefdatastring(SharePref.USERID);


        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return cartListModelsArrayList1.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View view = convertView;


            if (view == null) {

                view = LayoutInflater.from(context).inflate(
                        R.layout.cart_item, parent, false);
            }


            fontChanger = new FontChangeCrawler(context.getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");


            //    fontChanger.replaceFonts((ViewGroup)view);

            TextView itemName = (TextView) view.findViewById(R.id.itemName);


            TextView itemPrice = (TextView) view.findViewById(R.id.itemPrice);

            final TextView itemQuantity = (TextView) view.findViewById(R.id.itemQuantity);


            Button plus = (Button) view.findViewById(R.id.plus);

            Button minus = (Button) view.findViewById(R.id.minus);
            ImageButton delete = (ImageButton) view.findViewById(R.id.delete);

            Log.e("price11", cartListModelsArrayList1.get(position).getProductPrice());


            String name = cartListModelsArrayList1.get(position).getProductName();

            String Price = cartListModelsArrayList1.get(position).getProductPrice();
            final String Quanity = cartListModelsArrayList1.get(position).getQuantity();

          Price =   df.format(Float.parseFloat(Price))+"";

            itemName.setText(name);

            itemPrice.setText(Price);

            itemQuantity.setText(Quanity);
            Log.e("qt",Quanity);

            totalProdcutPrice = totalProdcutPrice + (Float.parseFloat(cartListModelsArrayList1.get(position).getProductPrice())*Float.parseFloat(cartListModelsArrayList1.get(position).getQuantity()));

            Log.e("tot",totalProdcutPrice+"");

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure?")
                            .setContentText("Remove this product from cart!")
                            .setConfirmText("Yes,delete it!")
                            .showCancelButton(true)
                            .setCancelText("Cancel")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.cancel();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {

                                    db.deleteCartItem(cartListModelsArrayList1.get(position).getProductID());

                                    cartListModelsArrayList1 = new ArrayList<>();

                                    cartListModelsArrayList1 = db.getCartList(shopID);

                                    totalProdcutPrice =0;

                                    Adapter Adapter = new Adapter(getActivity(),cartListModelsArrayList1);

                                    list.setAdapter(Adapter);


                                    if(cartListModelsArrayList1.size()==0){
                                        emptyCart.setVisibility(View.VISIBLE);
                                        list.setVisibility(View.GONE);
                                    }

                                    DecimalFormat df = new DecimalFormat("#.00");
                                  getTotalPrice();
                                    sDialog.cancel();
                                }
                            })
                            .show();

                }
            });

            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    db.checkCart(cartListModelsArrayList1.get(position).getShopID(), sharePref.getshareprefdatastring(SharePref.CURRENTSHOPNAME),
                            cartListModelsArrayList1.get(position).getProductID(), cartListModelsArrayList1.get(position).getProductName()
                            , "1", cartListModelsArrayList1.get(position).getProductPrice());

                    int x = Integer.parseInt(cartListModelsArrayList1.get(position).getQuantity()) + 1;

                    itemQuantity.setText(x+"");

                    cartListModelsArrayList1 = new ArrayList<>();

                    cartListModelsArrayList1 = db.getCartList(shopID);
                    totalProdcutPrice =0;

                    Adapter Adapter = new Adapter(getActivity(),cartListModelsArrayList1);

                    list.setAdapter(Adapter);

                    DecimalFormat df = new DecimalFormat("#.00");
                    getTotalPrice();


                }
            });

            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    int x = Integer.parseInt(cartListModelsArrayList1.get(position).getQuantity()) - 1;


                    if(x!=0){
                        db.checkCart(cartListModelsArrayList1.get(position).getShopID(), sharePref.getshareprefdatastring(SharePref.CURRENTSHOPNAME),
                                cartListModelsArrayList1.get(position).getProductID(), cartListModelsArrayList1.get(position).getProductName()
                                , "-1", cartListModelsArrayList1.get(position).getProductPrice());
                        itemQuantity.setText(x+"");
                    }

                    cartListModelsArrayList1 = new ArrayList<>();

                    cartListModelsArrayList1 = db.getCartList(shopID);

                    totalProdcutPrice =0;
                    Adapter Adapter = new Adapter(getActivity(),cartListModelsArrayList1);

                    list.setAdapter(Adapter);

                    DecimalFormat df = new DecimalFormat("#.00");
                    getTotalPrice();

                }
            });


            getTotalPrice();
            return view;
        }
    }




}