package com.bodega.customer.fragment;


import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.adapter.ProductAdapter;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.model.ProductModel;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class Fragment_search_list extends Fragment {



    // Connection detector class
    ConnectionDetector cd;

    int success;
    String message;
    JSONParser jsonParser;

    JSONObject json;

    ProductModel productModel;

    SharePref sharePref;

    String prefredArea;

    int pageNumber=1;

    ArrayList<ProductModel> productModelArrayList;

    LinearLayout btnPrev,btnNext;

    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;


    GridView list;

    EditText searchEdt;
    Button searchBtn;
    String districtId;
    boolean isInterNetpresent=false;

    Spinner categorySpinner;
    String token,searchCategory,storeID;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Get the view from fragmenttab1.xml
        View view = inflater.inflate(R.layout.fragment_search_list, container,
                false);

        searchEdt = (EditText) view.findViewById(R.id.searchEDIT);

        searchBtn = (Button) view.findViewById(R.id.search);

        list = (GridView) view.findViewById(R.id.list);

        sharePref = new SharePref(getActivity());

        storeID = sharePref.getshareprefdatastring(SharePref.CURRENTSHOPID);

        productModelArrayList = new ArrayList<>();




        sharePref = new SharePref(getActivity());


        jsonParser = new JSONParser();


        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


         cd = new ConnectionDetector(getActivity());
        isInterNetpresent = cd.isConnectingToInternet();





        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInterNetpresent) {


                    token = searchEdt.getText().toString();

                    new AsyncTaskProducts().execute();

                }

            }
        });




        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



            }
        });





return view;

    }


    class AsyncTaskProducts extends AsyncTask<String, String, String> {


        @Override
        protected void onPostExecute(String result) {
            if (success == 0) {




                try {




                    progressSweetAlertDialog.dismiss();

                    ProductAdapter productAdapter = new ProductAdapter(getActivity(),productModelArrayList);
                    list.setAdapter(productAdapter);

                    //	setData();



                }catch (Exception e){

                }

            } else
            {
                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);

                doneDialog.setTitleText(message);

                doneDialog.show();
            }


            progressSweetAlertDialog.dismiss();

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();



            productModelArrayList.clear();
            pair.add(new BasicNameValuePair("storeid", storeID));
            pair.add(new BasicNameValuePair("term", token));
            json = jsonParser.makeHttpRequest(Url.PRODUCT_SEARCH, "POST", pair);

            Log.e("reg", json + "");



            try {

                success = json.getInt("error");
                message = json.getString("message");

                if(success==0) {




                    JSONArray prouductsArray = new JSONArray();

                    prouductsArray   = json.getJSONArray("records");

                    for (int i = 0; i < prouductsArray.length(); i++) {



                        JSONObject item = prouductsArray.getJSONObject(i);
                        String catID="", title="",status="",id="",storeID="",price="";

                        id = item.getString("id");
                        title = item.getString("title");
                        status = item.getString("status");
                        storeID = item.getString("storeid");
                        price = item.getString("unitprice");
                        catID = item.getString("categoryid");



                        productModel = new ProductModel( id,  title,  status,  price,  catID,  storeID,"");


                        productModelArrayList.add(productModel);

                    }
                }




            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd","asdasd");
                progressSweetAlertDialog.show();

                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

    }













}