package com.bodega.customer.fragment;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class ProfileFragment extends Fragment {

    TextView  TopBarText, firstNameTextT, lastNameTextT, emailTextT, phoneTextT;
    EditText firstNameText, lastNameText, emailText, phoneText;
    String firstNameTextString, lastNameTextString, emailTextString, phoneTextString;
    ScrollView mainLayout;
    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;

    ImageButton nearestTxt, edittext;
    Button update;

    String firstNameString, lastNameString,emailString, addressString, phoneString, userIdString,passwordString1,passwordString2;

    LinearLayout passwordLin1,passwordLin2;
    LinearLayout shopList, inbox;

    ImageButton backBtn;


    FloatingActionButton edit;
    FloatingActionButton logout;
    FloatingActionButton changepassword;
    public boolean editable = false;
    ConnectionDetector cd;
    boolean isInternetOn=false;

    SharePref sharePref;
    JSONParser jsonParser;
    JSONObject json;
    int success;
    String message;
    ImageButton cancel;

    EditText password1,password2;

    boolean visited;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mainLayout = (ScrollView) view.findViewById(R.id.main);
        sharePref = new SharePref(getActivity());
        jsonParser = new JSONParser();

        firstNameText = (EditText) view.findViewById(R.id.profileFirstName);
        lastNameText = (EditText) view.findViewById(R.id.profileLastName);
        emailText = (EditText) view.findViewById(R.id.profileEmail);
        phoneText = (EditText) view.findViewById(R.id.profileContact);

        firstNameTextT = (TextView) view.findViewById(R.id.profileFirstNam);
        lastNameTextT = (TextView) view.findViewById(R.id.profileLastNam);
        emailTextT = (TextView) view.findViewById(R.id.profileEmailT);
        phoneTextT = (TextView) view.findViewById(R.id.profileContactT);
        cancel = (ImageButton)view.findViewById(R.id.cancel);
        edittext = (ImageButton)view.findViewById(R.id.edit);
        update = (Button)view.findViewById(R.id.update);

        password1 = (EditText) view.findViewById(R.id.password1);
        password2 = (EditText) view.findViewById(R.id.password2);

        passwordLin1 = (LinearLayout) view.findViewById(R.id.passwordLin1);

        passwordLin2 = (LinearLayout) view.findViewById(R.id.passwordLin2);

        disableAll();

     // ((DrawerActivity)getActivity()).hideCartButton(true, 1);
        cd = new ConnectionDetector(getActivity());

        isInternetOn = cd.isConnectingToInternet();

        firstNameString = sharePref.getshareprefdatastring(SharePref.FIRSTNAME);
        lastNameString = sharePref.getshareprefdatastring(SharePref.LASTNAME);
        emailString = sharePref.getshareprefdatastring(SharePref.USEREMAIL);
        phoneString = sharePref.getshareprefdatastring(SharePref.USERPHONE);
        userIdString = sharePref.getshareprefdatastring(SharePref.USERID);

        firstNameText.setText(firstNameString);
        lastNameText.setText(lastNameString);
        emailText.setText(emailString);

        if(!phoneString.isEmpty()) {
            phoneText.setText(phoneString);
        }else {
            phoneText.setText("Please update your phone number");
        }

        update.setVisibility(View.GONE);

        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
        edittext.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (!editable) {
                    enableAll();
                    cancel.setVisibility(View.VISIBLE);
                    editable = true;
                    update.setVisibility(View.VISIBLE);

                } else {
                    disableAll();
                    editable = false;
                }
            }
        });


        visited = sharePref.getshareprefdataBoolean(SharePref.FIRSTTIME);

        String method = sharePref.getshareprefdatastring(SharePref.LOGEDIN_METHOD);

        if(!visited && method.equals("facebook")){
            doneDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.NORMAL_TYPE);
            doneDialog.setTitleText("Update Contact");
            doneDialog.setContentText("Please update your contact information to receive special offers from My Bodega Online.");
            doneDialog.show();
            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sharePref.setshareprefdataBoolean(SharePref.FIRSTTIME,true);
                    doneDialog.dismiss();
                }
            });
        }


        cancel.setVisibility(View.INVISIBLE);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstNameTextString = firstNameText.getText().toString();
                lastNameTextString = lastNameText.getText().toString();
                emailTextString = emailText.getText().toString();
                phoneTextString = phoneText.getText().toString();
                emailTextString = emailText.getText().toString();
                passwordString1 = password1.getText().toString();
                passwordString2 = password2.getText().toString();
                cd = new ConnectionDetector(getActivity());

                isInternetOn = cd.isConnectingToInternet();

                if(isInternetOn) {

                    if(passwordString1.equals(passwordString2)) {

                        new AsyncTaskUpdate().execute();
                    }else{
                        doneDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE);
                        doneDialog.setTitleText("ERROR");
                        doneDialog.setContentText("Both password doesn't match");
                        doneDialog.show();
                        doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                doneDialog.dismiss();
                            }
                        });

                    }


                }else{
                    Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();


                }
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableAll();
                update.setVisibility(View.INVISIBLE);
                cancel.setVisibility(View.INVISIBLE);
            }
        });

        return view;
    }
    class AsyncTaskUpdate extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd","asdasd");
                progressSweetAlertDialog.show();
                update.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();
            pair.add(new BasicNameValuePair("userType", "0"));
            pair.add(new BasicNameValuePair("userId", userIdString));
            pair.add(new BasicNameValuePair("userFirstName", firstNameTextString));
            pair.add(new BasicNameValuePair("userLastName", lastNameTextString));
            pair.add(new BasicNameValuePair("userEmail", emailTextString));
            pair.add(new BasicNameValuePair("userContact", phoneTextString));

            pair.add(new BasicNameValuePair("newPassword", passwordString1));
            json = jsonParser.makeHttpRequest(Url.UPDATE, "POST", pair);

            Log.e("reg", pair + "");
            Log.e("reg", json + "");



            try {

                success = json.getInt("success");
                message = json.getString("message");

                Log.e("msh", message);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressSweetAlertDialog.dismiss();
            if (success == 1) {
                sharePref.setshareprefdatastring(SharePref.FIRSTNAME, firstNameTextString);
                sharePref.setshareprefdatastring(SharePref.LASTNAME, lastNameTextString);
                sharePref.setshareprefdatastring(SharePref.USERPHONE, phoneTextString);
                sharePref.setshareprefdatastring(SharePref.USEREMAIL, emailTextString);
                sharePref.setshareprefdatastring(SharePref.USERPASSWWORD, passwordString1);
                doneDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                doneDialog.setTitleText("Success");
                doneDialog.setContentText(message);
                doneDialog.show();
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        doneDialog.dismiss();
                    }
                });
                setData();
                disableAll();
                cancel.setVisibility(View.INVISIBLE);
                update.setVisibility(View.INVISIBLE);
//                Intent intent = new Intent(getActivity(), DrawerActivity.class);
//                startActivity(intent);


            } else{


                try {
                    JSONObject error = json.getJSONObject("results");

                    JSONArray jsonArray = error.getJSONArray("errors");

                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject item = jsonArray.getJSONObject(i);

                        message = item.getString("errorMessage");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final   SweetAlertDialog  errorDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE);

                errorDialog.setContentText(message);
                errorDialog.show();
                errorDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        errorDialog.dismiss();
                    }
                });
            }

            progressSweetAlertDialog.dismiss();
            update.setEnabled(true);
        }


    }
    @Override
    public void onResume() {

         ((DrawerActivity)getActivity()).hideTextButton(true);
        super.onResume();
    }


    public  void setData(){


        firstNameString = sharePref.getshareprefdatastring(SharePref.FIRSTNAME);
        lastNameString = sharePref.getshareprefdatastring(SharePref.LASTNAME);
        emailString = sharePref.getshareprefdatastring(SharePref.USEREMAIL);
        phoneString = sharePref.getshareprefdatastring(SharePref.USERPHONE);
        userIdString = sharePref.getshareprefdatastring(SharePref.USERID);

        firstNameText.setText(firstNameString);
        lastNameText.setText(lastNameString);
        emailText.setText(emailString);

        if(!phoneString.isEmpty()) {
            phoneText.setText(phoneString);
        }else {
            phoneText.setText("Please update your phone number");
        }

    }

public void disableAll(){
    firstNameText.setEnabled(false);
    lastNameText.setEnabled(false);
    emailText.setEnabled(false);
    phoneText.setEnabled(false);




        passwordLin1.setVisibility(View.GONE);

        passwordLin2.setVisibility(View.GONE);


}

    public void enableAll(){
        firstNameText.setEnabled(true);
        lastNameText.setEnabled(true);
        emailText.setEnabled(true);
        phoneText.setEnabled(true);

        String method = sharePref.getshareprefdatastring(SharePref.LOGEDIN_METHOD);

        Log.e("fb",method);

        if(!method.equals("facebook")) {
            passwordLin1.setVisibility(View.VISIBLE);

            passwordLin2.setVisibility(View.VISIBLE);
        }


        if(phoneText.getText().toString().equals("Please update your phone number")){
            phoneText.setText("");
        }
    }

//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }
}
