package com.bodega.customer.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.MyDatabase;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.SupportMapFragment;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;


public class Fragment_ProductDetails extends Fragment {


    JSONParser jsonParser;
   // Typeface myTypeface, custom;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    int success = 0;

    boolean dOption1, dOption2, dOption3, dOption4;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    String loginStatus;

    SharePref sharePref;




    SupportMapFragment mapFragment;

    FragmentManager mFragmentManager;



    ImageButton backBtn;


    TextView deliveryOption1,deliveryOption2;


    boolean isInternetOn;

    ConnectionDetector cd;

    MyDatabase db;

    CircleImageView image;

    String gcmCode;

    TextView productName,productPrice,quantity,totalPrice;

    LinearLayout productlayout;

    Button plus,minus;
    RelativeLayout addToCart;
    String shopName;

    GridView gridView;

    String shopID,productIdString,productNameString,productPriceString,productImageString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_product_detals, container,
                false);

        jsonParser = new JSONParser();
        sharePref = new SharePref(getActivity());
        loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);


        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
//
//        myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRnd-Book.otf");
//        custom = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoCondensed-Regular.ttf");

        productName = (TextView) v.findViewById(R.id.product_name);
        productPrice = (TextView) v.findViewById(R.id.product_price);

        plus = (Button) v.findViewById(R.id.btnPlus);

        minus = (Button) v.findViewById(R.id.btnMinus);

        addToCart = (RelativeLayout) v.findViewById(R.id.addToCart);


        quantity = (TextView) v.findViewById(R.id.quantity);

        totalPrice = (TextView) v.findViewById(R.id.total_price);


        gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);


       // gridView = v.findViewById(R.id.grid);


        deliveryOption1 = (TextView) v.findViewById(R.id.option1);

        deliveryOption2 = (TextView) v.findViewById(R.id.option2);


        productlayout = (LinearLayout) v.findViewById(R.id.productLayout);




        backBtn = (ImageButton) v.findViewById(R.id.backBtn);

        db = new MyDatabase(getActivity());

        cd = new ConnectionDetector(getActivity());

        isInternetOn = cd.isConnectingToInternet();

        shopID = getArguments().getString("shopId");

        shopName = getArguments().getString("shopName");
        productNameString = getArguments().getString("productName");

        Log.e("shopiD", shopID);

         productIdString =getArguments().getString("productId");

        productImageString =getArguments().getString("productImage");
        productPriceString =getArguments().getString("productPrice");



        sharePref.setshareprefdatastring(SharePref.CURRENTSHOPID,shopID);

        productPrice.setText("$" +productPriceString);
        productName.setText(productNameString);


        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
            //    db.checkCart(shopID,shopName,productIdString,productNameString,productImageString,quantity.getText().toString(),productPriceString);
                Snackbar snackbar = Snackbar
                        .make(addToCart, "Item added", Snackbar.LENGTH_LONG);

                snackbar.show();

             //   setCounter();
            }
        });


        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String temp = quantity.getText().toString();

            int currentQ=Integer.parseInt(temp) + 1;

                quantity.setText(currentQ+"");

                int total = (Integer.parseInt(temp) + 1)*Integer.parseInt(productPriceString);
                totalPrice.setText("$"+total+"");

            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String temp = quantity.getText().toString();

                quantity.setText(Integer.parseInt(temp)-1);
                int total = (Integer.parseInt(temp) - 1)*Integer.parseInt(productPriceString);
                totalPrice.setText("$"+total+"");



            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


             //   Log.i("si:",(DrawerActivity.class).fragmentStack.size()+"");
                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onPause();
                ft.remove(((DrawerActivity) getActivity()).fragmentStack.pop());

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onResume();
                ft.show(((DrawerActivity) getActivity()).fragmentStack.lastElement());
                //  Log.i("bbb",fragmentStack.lastElement()+"");


                ft.commit();
            }
        });


        return v;
    }

    public void setCounter(){

        int count = db.getCartCounter(shopID);
   ((DrawerActivity)getActivity()).hideCartButton(false,count);

    }



    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(getActivity(), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                getActivity().finish();
            }
            return false;
        }
        return true;
    }





    @Override
    public void onResume() {
        ((DrawerActivity)getActivity()).hideTextButton(true);
      //  ((DrawerActivity)getActivity()).hideTextButton(false);
        super.onResume();
    }





}
