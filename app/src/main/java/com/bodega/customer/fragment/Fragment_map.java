package com.bodega.customer.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.activity.DrawerActivity;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.databasemanager.Url;
import com.bodega.customer.model.ShopListModel;
import com.bodega.customer.utils.ConnectionDetector;
import com.bodega.customer.utils.SharePref;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Fragment_map extends Fragment implements LocationListener {

	private GoogleMap mMap;
	SupportMapFragment mapFragment;
	LocationManager locationManager;

	String provider;
	int count = 0;
	ImageButton profile;
	TextView topTxt;
	Marker m1;

	int success = 0;

	ArrayList shopIDList;

	HashMap<String ,Integer>  mapStatus;
	HashMap<String ,Integer>  hashTemp;

	RelativeLayout tutorial;
	Location location;

	JSONParser jsonParser;

	ArrayList<Marker> markers;

	JSONObject json;

	String msg, message;

	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;


	String shopContact, shopID;

	Typeface mytTypeface, customFont;

	SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

	public ArrayList<ShopListModel> shopList;
	ShopListModel shopListModel;

	Map<String, String> shopMap = new HashMap<String, String>();



	TextView titleText;

	double lat, lon;

	String firstTime;
	LatLng firstPoint = null;
	LatLng secondPoint = null;

	float distanceInMeter;

	LatLng currentLoc;
	TextView distanceInfo;
	ArrayList<LatLng> trackList;
	double tempLat, tempLon;

	String loginStatus;

	Button gotIt;
	SharePref sharePref;

	boolean isInternetOn;

	ConnectionDetector cd;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_maps, container, false);
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.

		mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

		mMap = mapFragment.getMap();

		shopIDList = new ArrayList();

		mapStatus = new HashMap<>();

		locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
		Criteria criteria = new Criteria();

		//((DrawerActivity) getActivity()). hideTextButton(true);

		mytTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");

		customFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRnd-Book.otf");

		sharePref = new SharePref(getActivity());
		provider = locationManager.getBestProvider(criteria, false);

		progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
		progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
		progressSweetAlertDialog.setTitleText("Loading");
		progressSweetAlertDialog.setCancelable(false);


		//firstTime = sharePref.getshareprefdatastring(SharePref.FIRSTTIME);

		markers = new ArrayList<>();
		shopList = new ArrayList<>();

		jsonParser = new JSONParser();
		loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);


		// profile.setVisibility(View.INVISIBLE);
		normalDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);


 cd= new ConnectionDetector(getActivity());

		isInternetOn = cd.isConnectingToInternet();

		if(isInternetOn) {
			//providerCheck();
		}else{
			Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

		}
		// ((DrawerActivity)getActivity()).hideCartButton(true,1);


		mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker marker) {
				String shopname = marker.getTitle();

				String info = marker.getSnippet();
				String shopID1 = shopMap.get(shopname);



				if(mapStatus.get(shopID1)==0){

					marker.showInfoWindow();
					setOtherMarker(shopID1);
					mapStatus.put(shopID1,1);

				}else{
					if (!shopname.equals("You are here")) {
						marker.hideInfoWindow();
						String shopName = marker.getTitle();
						String shopID = shopMap.get(shopName);
						FragmentManager fm = getActivity().getSupportFragmentManager();
						FragmentTransaction ft = fm.beginTransaction();
						Fragment_ShopDetails llf = new Fragment_ShopDetails();

						//((DrawerActivity) getActivity()).hideTextButton(false);
//
						Bundle bundle = new Bundle();
						bundle.putString("shopId", shopID);
						mapStatus.put(shopID, 0);
						llf.setArguments(bundle);
						((DrawerActivity) getActivity()).fragmentStack.push(llf);
						recheckMapHASH();
						ft.add(R.id.frame_container, llf);
						ft.commit();


					}
				}

				return false;
			}
		});



		//  LOOP(location);
//
		mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
			@Override
			public void onInfoWindowClick(Marker marker) {


				String shopname = marker.getTitle();

				String info = marker.getSnippet();


				if (!shopname.equals("You are here")) {

					String shopName = marker.getTitle();
					String shopID = shopMap.get(shopName);
					FragmentManager fm = getActivity().getSupportFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					Fragment_ShopDetails llf = new Fragment_ShopDetails();
					recheckMapHASH();
				//	((DrawerActivity) getActivity()).hideTextButton(false);
					marker.hideInfoWindow();
					Bundle bundle = new Bundle();
					bundle.putString("shopId", shopID);
					mapStatus.put(shopID,0);
					llf.setArguments(bundle);
					((DrawerActivity) getActivity()). fragmentStack.push(llf);
					marker.hideInfoWindow();
					ft.add(R.id.frame_container, llf);
					ft.commit();


				}
			}
		});

//		profile.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				loginStatus = sharePref.getshareprefdatastring(SharePref.LOGEDIN);
//
//				if(loginStatus.equals("yes")){
//					Intent intent = new Intent(getActivity(), DrawerActivity.class);
//
//
//					startActivity(intent);
//
//					getActivity().finish();
//				}else{
//
//					Intent intent = new Intent(getActivity(), LoginActivity.class);
//
//
//					startActivity(intent);
//
//
//				}
//
//
//			}
//		});


		return rootView;
	}

	public void recheckMapHASH(){

		mapStatus = hashTemp;

	}

	public void setOtherMarker(String shopID){

		for(int i=0;i<mapStatus.size();i++){

			String temp = shopIDList.get(i).toString();
			if(!temp.equals(shopID)){
				mapStatus.put(temp,0);
			}
		}

	}


	@Override
	public void onResume() {

 	((DrawerActivity) getActivity()). hideTextButton(true);


		super.onResume();
	}

//	public void providerCheck(){
//
//		if (provider != null && !provider.equals("")) {
//
//			// Get the location from the given provider
//		//	if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//				// TODO: Consider calling
//
//				return;
//		//	}
//		//	location = locationManager.getLastKnownLocation(provider);
//
//			//locationManager.requestLocationUpdates(provider, 20000, 1, this);
//
//			//isGPSEnabled = locationManager
//			//		.isProviderEnabled(LocationManager.GPS_PROVIDER);
//
//			// getting network status
//			//isNetworkEnabled = locationManager
//			//		.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//			//mMap.setMyLocationEnabled(true);
//
//			if (location != null) {
//
//			//	onLocationChanged(location);
//
//
//				//setUpMap();
//
//			}else  if(!isGPSEnabled){
//				errorDialog();
//			//	Log.e("op1","1");
//			}else  if(!isNetworkEnabled){
//				errorDialog();
//
//			//	Log.e("op2", "2");
//			}
//			else {
//				errorDialog();
//			//	//Log.e("op3", "3");
//			}
//			// Toast.makeText(getApplicationContext(), "Location can't be retrieved, check your gps", Toast.LENGTH_SHORT).show();
//
//		} else {
//			Toast.makeText(getActivity(), "No Location Provider Found", Toast.LENGTH_SHORT).show();
//		}
//	}


	public void errorDialog(){

		new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
				.setTitleText("Oops...")
				.setContentText("Location can't be retrieved, check your gps")
				.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sweetAlertDialog) {

//                                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
//
//
//                                startActivity(intent);
						getActivity().finish();

						sweetAlertDialog.dismiss();
					}
				}).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
			@Override
			public void onClick(SweetAlertDialog sweetAlertDialog) {


				//providerCheck();
				sweetAlertDialog.dismiss();

			}
		}).setCancelText("Cancel").setConfirmText("Restart this page")
				.show();
	}

	public void setUpMap() {


		progressSweetAlertDialog.show();

		mMap.setMyLocationEnabled(true);

		MarkerOptions markerOptions = new MarkerOptions();





		// markerOptions.title("Ahmedabad Cordinat Found here");

		// Marker m = mMap.addMarker(markerOptions);

		mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
			@Override
			public View getInfoWindow(Marker marker) {
				View myContentView = getActivity().getLayoutInflater().inflate(
						R.layout.custom_marker, null);
				TextView tvTitle = ((TextView) myContentView
						.findViewById(R.id.snippet));
				tvTitle.setText(marker.getTitle());
				TextView tvSnippet = ((TextView) myContentView
						.findViewById(R.id.title));
				tvSnippet.setText(marker.getSnippet());
				return myContentView;
			}

			@Override
			public View getInfoContents(Marker marker) {

			return null;
			}
		});

		setLocation(tempLat, tempLon);

		new AsyncTaskEdit().execute();

	}


	public void LOOP(Location location) {

     for (int i = 0; i < 20000; i++) {

			if (location != null) {

				tempLon = location.getLongitude();
				tempLat = location.getLatitude();

				setUpMap();
				break;
			}

		}

	}



	public void getShopDetails(String shopName, String shopInfo) {


		for (int i = 0; i < shopList.size(); i++) {


			String shopName1 = shopList.get(i).getShopName();
			String shopInfo1 = shopList.get(i).getShopLocation();
			shopID = shopList.get(i).getShopID();

			shopContact = shopList.get(i).getShopContact();


			if (shopName1.equals(shopName) && shopInfo1.equals(shopInfo)) {

				sharePref.setshareprefdatastring(SharePref.SELECTED_ID, shopID);

				break;
			}

		}


	}

	@Override
	public void onLocationChanged(Location location) {

		//   String myLocation = "Latitude = " + location.getLatitude() + " Longitude = " + location.getLongitude();

		//   LatLng(location.getLatitude(),location.getLongitude());
		currentLoc = new LatLng(location.getLatitude(), location.getLongitude());


		int temp = (int) tempLat;

		String temp1 = tempLon + "";

		if (temp == 0) {
			tempLat = location.getLatitude();
			tempLon = location.getLongitude();


		}



	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onProviderDisabled(String provider) {

	}


	public void setLocation(double lat, double lon) {

		LatLng loc = new LatLng(lat, lon);


		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 13.0f));

	}




	class AsyncTaskEdit extends AsyncTask<String, String, String> {


		@Override
		protected void onPostExecute(String result) {
			if (success == 1) {


				try {


					setShopLocations();


					 progressSweetAlertDialog.dismiss();


				} catch (Exception e) {

				}

			} else {
                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                doneDialog.setContentText(message);
                doneDialog.show();
			}


			progressSweetAlertDialog.dismiss();

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> pair = new ArrayList<NameValuePair>();


			pair.add(new BasicNameValuePair("myLatitude", tempLat + ""));

			pair.add(new BasicNameValuePair("myLongitude", tempLon + ""));


			 Log.e("url", Url.NEARSETSHOP);

			json = null;

			json = jsonParser.makeHttpRequest(Url.NEARSETSHOP, "POST", pair);

			 Log.e("edit", json + "");


			try {

				success = json.getInt("success");


				message = json.getString("message");


			JSONObject	results = json.getJSONObject("results");
				if (success == 1) {


					JSONArray jsonArray = results.getJSONArray("shopList");


					for (int i = 0; i < jsonArray.length(); i++) {


						JSONObject object = jsonArray.getJSONObject(i);

						String shopID = object.getString("storeId");
						String shopName = object.getString("storeName");

						String shopLocation = object.getString("storeStreet");
						String shopLatitude = object.getString("storeLatitude");
						String shopLongitude = object.getString("storeLongitude");
						if (!shopLatitude.equals("") || !shopLongitude.equals("")) {

							//Log.e("asoda", Double.parseDouble(shopLatitude) + "");
							double tempLat = 0, tempLon = 0;
							try {
								tempLat = Double.parseDouble(shopLatitude);


								tempLon = Double.parseDouble(shopLongitude);
							} catch (Exception e) {

							}

							shopListModel = new ShopListModel(shopID, shopName, shopContact, shopLocation, tempLat, tempLon);


							mapStatus.put(shopID,0);
							shopList.add(shopListModel);
						}
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPreExecute() {

			try {

				//Log.e("asd", "asdasd");
				// progressSweetAlertDialog.show();

				//   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
			} catch (IndexOutOfBoundsException e) {
				Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT).show();
			}

		}

	}


	public void setShopLocations() {


		for (int i = 0; i < shopList.size(); i++) {


			String temp = "m" + i + 1;

			Marker marker = null;

			String shopID = shopList.get(i).getShopID();

			String shopName = shopList.get(i).getShopName();

			String shopAddress = shopList.get(i).getShopLocation();

			double shopLat = shopList.get(i).getShopLatitude();

			double shopLon = shopList.get(i).getShopLongitude();


			shopIDList.add(shopID);
			shopMap.put(shopName,shopID);



			LatLng loc = new LatLng(shopLat, shopLon);

			//Log.e("loc",loc+"");

			if(shopLat!=0 || shopLon!=0) {

				//  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 16.0f));
				marker = mMap.addMarker(new MarkerOptions().position(new LatLng(shopLat, shopLon)).title(shopName).snippet(shopAddress));
				marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_shop));
				marker.showInfoWindow();

				markers.add(marker);
			}
		}

		hashTemp = mapStatus;


	}


//	@Override
//	public void onActivityCreated(Bundle savedInstanceState)
//	{
//		super.onActivityCreated(savedInstanceState);
//
//		FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//		fontChanger.replaceFonts((ViewGroup) this.getView());
//	}
}