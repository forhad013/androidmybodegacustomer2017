package com.bodega.customer.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.customer.R;
import com.bodega.customer.databasemanager.JSONParser;
import com.bodega.customer.utils.ConnectionDetector;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class Fragment_home extends Fragment {

	Calendar myCalendar;

	TextView to, from, paid_t, sale_t, balance_t;

	private DatePicker datePicker;
	private Calendar calendar;
	private TextView dateView;
	private int year, month, day;
	ListView list;
	Button post;
	String day1, month1;
	ArrayList<String> date, trasectionid, balance, payment, subtotal;
	Boolean isInternetPresent = false;
	String total_paid, total_sale, total_receieved;

	// Connection detector class
	ConnectionDetector cd;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml
		View view = inflater.inflate(R.layout.fragment_home, container,
				false);

		// getActionBar().setDisplayHomeAsUpEnabled(true);
		// getActionBar().setHomeButtonEnabled(true);

		cd = new ConnectionDetector(getActivity());

		isInternetPresent = cd.isConnectingToInternet();

		// calendar = Calendar.getInstance();

		// ReportDrawerActivity report = new ReportDrawerActivity();

		// dateFormat = "MM";



	

		if (isInternetPresent) {



		} else {
			Toast.makeText(getActivity(), "No internet connection available",
					Toast.LENGTH_LONG).show();
		}

		return view;
	}



	class AsyncTaskRunnerDetails extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(getActivity());
		int success = 5;
		JSONParser jParser = new JSONParser();

		Boolean availableProduct = false;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			{

				date.clear();

				trasectionid.clear();
				payment.clear();

				subtotal.clear();
				balance.clear();

				try {

					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

					// nameValuePairs.add(new BasicNameValuePair("fromdate",
					// from
					// .getText().toString()));

					// nameValuePairs.add(new BasicNameValuePair("todate", to
					// .getText().toString()));

					JSONObject json = jParser
							.makeHttpRequest(
									"http://altertechit.com/stock_android/due_payment.php",
									"POST", nameValuePairs);

					success = json.getInt("success");

					if (success == 1) {

						JSONArray product = json.getJSONArray("report");

						for (int i = 0; i < product.length(); i++) {
							JSONObject item = product.getJSONObject(i);

							date.add(item.getString("due"));

							payment.add(item.getString("payment"));
							trasectionid.add(item.getString("transactionid"));
							subtotal.add(item.getString("subtotal"));
							balance.add(item.getString("balance"));
						}

						availableProduct = true;

					}

				} catch (Exception e) {

					Log.d("Problemasdddddddddddddd", e + "");
				}
				return null;
			}

		}

		@Override
		protected void onPostExecute(String string) {

			progressDialog.dismiss();
			if (availableProduct = true) {



				// /calculayion();
				// Toast.makeText(getApplicationContext(),
				// date.get(0),
				// Toast.LENGTH_LONG).show();

				// ArrayAdapter<String> adapter = new
				// ArrayAdapter<String>(SalesReport.this,
				// android.R.layout.simple_list_item_1, android.R.id.text1,
				// date);
			}

			else {
				Toast.makeText(getActivity(),
						"Something wrong....Try again later", Toast.LENGTH_LONG)
						.show();

			}

		}

		@Override
		protected void onPreExecute() {

			progressDialog.setMessage("Please wait. Loading..");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

	}


//	@Override
//	public void onActivityCreated(Bundle savedInstanceState)
//	{
//		super.onActivityCreated(savedInstanceState);
//
//		FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//		fontChanger.replaceFonts((ViewGroup) this.getView());
//	}
}